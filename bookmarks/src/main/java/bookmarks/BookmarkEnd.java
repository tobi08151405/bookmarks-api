package bookmarks;

import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTMarkupRange;

/**
 * Properties of a bookmarkEnd element.
 * 
 * @author Markus Pscheidt
 *
 */
public class BookmarkEnd {

	private Integer id;
	private CTMarkupRange ctMarkup;
	private XWPFParagraph paragraph;
	private Integer index;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public CTMarkupRange getCtMarkup() {
		return ctMarkup;
	}

	public void setCtMarkup(CTMarkupRange ctMarkup) {
		this.ctMarkup = ctMarkup;
	}

	public XWPFParagraph getParagraph() {
		return paragraph;
	}

	public void setParagraph(XWPFParagraph paragraph) {
		this.paragraph = paragraph;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

}
