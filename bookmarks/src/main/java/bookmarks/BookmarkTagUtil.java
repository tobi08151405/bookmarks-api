package bookmarks;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBookmark;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTMarkup;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTMarkupRange;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

/**
 * Find bookmark start tags, end tags, nested bookmarks, overlapping.
 * 
 * @author Markus Pscheidt
 *
 */
public final class BookmarkTagUtil {

	private static Logger log = LoggerFactory.getLogger(BookmarkTagUtil.class);

	/**
	 * Get list of nodes of overlapping bookmarks.
	 */
	public static OverlappingBookmarks findOverlappingBookmarks(Bookmark b) {
		OverlappingBookmarks overlappingBookmarks = new OverlappingBookmarks();

		Set<Integer> bookmarkIdToggler = new HashSet<>();
		Map<Integer, XmlObject> bookmarkIdToNodeMapFirstP = new LinkedHashMap<>();
		Map<Integer, XmlObject> bookmarkIdToNodeMapOthers = new LinkedHashMap<>();

		BigInteger id = b.getCTBookmark().getId();
		int bookmarkId = id.intValue();
		XmlCursor bStartCursor = b.getCTBookmark().newCursor();
		
		XWPFParagraph firstParagraph = b.getParagraph();
		XWPFDocument document = firstParagraph.getDocument();
		CTMarkup bookmarkEnd = findBookmarkEndUsingPath(document, id);
		if (bookmarkEnd == null) {
			log.warn("Bookmark '{}' has no end tag. Aborting");
			return null;
		}
		overlappingBookmarks.setBookmarkEnd(bookmarkEnd);
		XmlCursor bEndCursor = bookmarkEnd.newCursor();
		
		boolean firstP = true;
		XmlCursor cursor = firstParagraph.getCTP().newCursor();
		while (cursor.isLeftOf(bEndCursor)) {
			XmlObject myParaCursorNode = cursor.getObject();
			if (myParaCursorNode instanceof CTP) {
				CTP ctp = (CTP) myParaCursorNode;
				
				// einmal alle BookmarkStart-Tags
				for (CTBookmark ctb : ctp.getBookmarkStartList()) {
					int ctbId = ctb.getId().intValue();
					if (ctbId == bookmarkId) {
						continue;
					}
					
					XmlCursor ctbCursor = ctb.newCursor();
					// noch links vom Anfang? -> noch nicht relevant
					if (ctbCursor.isLeftOf(bStartCursor)) {
						ctbCursor.dispose();
						continue;
					}
					
					toggle(bookmarkIdToggler, ctbId);
					if (firstP) {
						bookmarkIdToNodeMapFirstP.put(ctbId, ctb);
					} else {
						bookmarkIdToNodeMapOthers.put(ctbId, ctb);
					}
					ctbCursor.dispose();
				}

				// dann alle BookmarkEnd-Tags
				for (CTMarkupRange ctb : ctp.getBookmarkEndList()) {
					int ctbId = ctb.getId().intValue();
					if (ctbId == bookmarkId) {
						continue;
					}
					
					XmlCursor ctbCursor = ctb.newCursor();
					// bereits rechts vom Ende? -> nicht mehr relevants
					if (ctbCursor.isRightOf(bEndCursor)) {
						ctbCursor.dispose();
						continue;
					}

					toggle(bookmarkIdToggler, ctbId);
					if (firstP) {
						bookmarkIdToNodeMapFirstP.put(ctbId, ctb);
					} else {
						bookmarkIdToNodeMapOthers.put(ctbId, ctb);
					}
					ctbCursor.dispose();
				}
			}

			cursor.toNextSibling();
			firstP = false;
		}
		cursor.dispose();

		bStartCursor.dispose();
		bEndCursor.dispose();

		bookmarkIdToNodeMapFirstP.keySet().retainAll(bookmarkIdToggler);
		bookmarkIdToNodeMapOthers.keySet().retainAll(bookmarkIdToggler);

		// Value from linkedHashMap are ordered in insertion order, therefore makes sense to put in a list to keep the order
		overlappingBookmarks.setOnFirstParagraph(new ArrayList<>(bookmarkIdToNodeMapFirstP.values()));
		overlappingBookmarks.setOnOtherParagraphs(new ArrayList<>(bookmarkIdToNodeMapOthers.values()));

		return overlappingBookmarks;
	}

	private static void toggle(Set<Integer> bookmarkIds, int ctbId) {
		if (bookmarkIds.contains(ctbId)) {
			bookmarkIds.remove(ctbId);
		} else {
			bookmarkIds.add(ctbId);
		}
	}

	/**
	 * Search for the bookmark with the given id within the given paragraph
	 * 
	 * @param ctp        paragraph
	 * @param bookmarkId bookmark id
	 */
	public static CTMarkup findBookmarkEndUsingPath(CTP ctp, BigInteger bookmarkId) {
		return findBookmarkEndUsingPathUnder(ctp, bookmarkId);
	}

	/**
	 * Search the entire document for the bookmarkEnd element with the given id.
	 * 
	 * <p>
	 * This find bookmarkEnds also when they are not located within a paragraph, but next to a paragraph.
	 * 
	 * @param para
	 * @param bookmarkId
	 * @return
	 */
	public static CTMarkup findBookmarkEndUsingPath(XWPFDocument document, BigInteger bookmarkId) {
		
		return findBookmarkEndUsingPathUnder(document.getDocument().getBody(), bookmarkId);
////		para.getCTP().newCursor();
//		CTBody body = para.getDocument().getDocument().getBody();
//		
//		String declareNameSpaces = "declare namespace w='http://schemas.openxmlformats.org/wordprocessingml/2006/main'";
//		XmlObject[] bookmarkEndObjects = body.selectPath(declareNameSpaces + ".//w:bookmarkEnd");
//
//		for (XmlObject bookmarkEndObject : bookmarkEndObjects) {
//			BigInteger wId = BookmarkTagUtil.getWId(bookmarkEndObject);
//			if (wId != null && wId.intValue() == bookmarkId) {
//				// There is no dedicated CTBookmarkEnd class, all we get is a CTMarkup
//				log.info("{}", bookmarkEndObject);
//				return (CTMarkup) bookmarkEndObject;
//			}
//		}
//		return null;
	}

	private static CTMarkup findBookmarkEndUsingPathUnder(XmlObject xmlObj, BigInteger bookmarkId) {
		String declareNameSpaces = "declare namespace w='http://schemas.openxmlformats.org/wordprocessingml/2006/main'";
		XmlObject[] bookmarkEndObjects = xmlObj.selectPath(declareNameSpaces + ".//w:bookmarkEnd");

		for (XmlObject bookmarkEndObject : bookmarkEndObjects) {
			BigInteger wId = BookmarkTagUtil.getWId(bookmarkEndObject);
			if (wId != null && wId.equals(bookmarkId)) {
				// There is no dedicated CTBookmarkEnd class, all we get is a CTMarkup
				return (CTMarkup) bookmarkEndObject;
			}
		}
		return null;
	}

	public static void deleteBookmarkEnd(XWPFDocument document, BigInteger bookmarkId) {
		
		// First, search all the paragraphs
		BookmarkEnd bookmarkEnd = findBookmarkEnd(document, bookmarkId.intValue());
		if (bookmarkEnd != null) {
			// In this case remove it cleanly so that internal data structures are maintained
			CTP bookmarkEndCtp = bookmarkEnd.getParagraph().getCTP();
			bookmarkEndCtp.removeBookmarkEnd(bookmarkEnd.getIndex());
			return;
		}
		
		// If bookmarkEnd was not within a paragraph, search entire body recursively
		// - this finds everything that is located anywhere in the body
		// (not in header and footer, though - this would simply require separate searches there)
		
		CTMarkup bookmarkEndMarkup = findBookmarkEndUsingPath(document, bookmarkId);
		if (bookmarkEndMarkup != null) {
			Node bookmarkEndNode = bookmarkEndMarkup.getDomNode();
			bookmarkEndNode.getParentNode().removeChild(bookmarkEndNode);
			return;
		}

		log.warn("No bookmarkEnd element found in document body (headers and footers not yet implemented). Cannot remove bookmarkEnd element");
	}

	public static BigInteger getWId(XmlObject xmlObject) {
		XmlObject wIdAttribute = xmlObject
				.selectAttribute("http://schemas.openxmlformats.org/wordprocessingml/2006/main", "id");
		if (wIdAttribute != null) {
			return new BigInteger(wIdAttribute.newCursor().getTextValue());
		}
		return null;
	}

	public static BookmarkEnd findBookmarkEnd(XWPFDocument document, int bookmarkId) {
		return findBookmarkEnd(document.getParagraphs(), bookmarkId);
	}

	private static BookmarkEnd findBookmarkEnd(List<XWPFParagraph> paraList, int bookmarkId) {

		for (XWPFParagraph para : paraList) {
			List<CTMarkupRange> bookmarkEndList = para.getCTP().getBookmarkEndList();
			BookmarkEnd bookmarkEnd = findBookmarkEndInBookmarkEndList(bookmarkEndList, bookmarkId);
			if (bookmarkEnd != null) {
				bookmarkEnd.setParagraph(para);
				return bookmarkEnd;
			}
		}
		return null;
	}

	private static BookmarkEnd findBookmarkEndInBookmarkEndList(List<CTMarkupRange> bookmarkEndList, int bookmarkId) {
		Iterator<CTMarkupRange> bookmarkIter;
		CTMarkupRange markupRange;
		int i = 0;
		bookmarkIter = bookmarkEndList.iterator();
		while (bookmarkIter.hasNext()) {
			markupRange = bookmarkIter.next();
			if (bookmarkId == markupRange.getId().intValue()) {
				BookmarkEnd be = new BookmarkEnd();
				be.setId(bookmarkId);
				be.setCtMarkup(markupRange);
				be.setIndex(i);
				return be;
			}
			i++;
		}
		return null;
	}


	
}
