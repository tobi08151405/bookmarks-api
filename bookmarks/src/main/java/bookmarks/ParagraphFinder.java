package bookmarks;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFFooter;
import org.apache.poi.xwpf.usermodel.XWPFHeader;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;

/**
 * Hilfsmethoden zum Finden von Paragraphen.
 * 
 * @author Markus Pscheidt
 *
 */
public final class ParagraphFinder {

	private ParagraphFinder() {
		// not to be instantiated
	}

	/**
	 * Paragraphen koennen sich in verschiedenen Orten befinden: Document, Header,
	 * Footer, Tabellen
	 * 
	 * @param p CTP, fuer das ein Paragraph gefunden werden soll.
	 * @return ein {@link XWPFParagraph}, das dem gegebenen {@link CTP} entspricht. null, falls kein Treffer.
	 */
	public static XWPFParagraph findParagraph(XWPFDocument doc, CTP p) {

		XWPFParagraph paragraph = doc.getParagraph(p);
		if (paragraph != null) {
			return paragraph;
		}
		
		for (XWPFHeader header : doc.getHeaderList()) {
			paragraph = header.getParagraph(p);
			if (paragraph != null) {
				return paragraph;
			}
		}

		for (XWPFFooter footer : doc.getFooterList()) {
			paragraph = footer.getParagraph(p);
			if (paragraph != null) {
				return paragraph;
			}
		}

		for (XWPFTableCell cell : TableUtil.allTableCells(doc)) {
			paragraph = cell.getParagraph(p);
			if (paragraph != null) {
				return paragraph;
			}
		}
		
		return null;
	}

}
