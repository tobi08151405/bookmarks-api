package bookmarks;

import org.apache.poi.xwpf.usermodel.XWPFDocument;

/**
 * Image utilities related to Apache POI.
 * 
 * @author Markus Pscheidt
 *
 */
public final class ImageUtil {

	public static int getImageFormat(String imgFileName) {
		
		if (imgFileName == null) {
			return 0;
		}
		
		String lowerCase = imgFileName.toLowerCase();
		int i = lowerCase.lastIndexOf('.');
		if (i == -1) {
			return 0;
		}

		String suffix = lowerCase.substring(i);
		
		switch (suffix) {
		case ".emf":
			return XWPFDocument.PICTURE_TYPE_EMF;
		case ".wmf":
			return XWPFDocument.PICTURE_TYPE_WMF;
		case ".pict":
			return XWPFDocument.PICTURE_TYPE_PICT;
		case ".jpeg":
		case ".jpg":
			return XWPFDocument.PICTURE_TYPE_JPEG;
		case ".png":
			return XWPFDocument.PICTURE_TYPE_PNG;
		case ".dib":
			return XWPFDocument.PICTURE_TYPE_DIB;
		case ".gif":
			return XWPFDocument.PICTURE_TYPE_GIF;
		case ".tiff":
			return XWPFDocument.PICTURE_TYPE_TIFF;
		case ".eps":
			return XWPFDocument.PICTURE_TYPE_EPS;
		case ".bmp":
			return XWPFDocument.PICTURE_TYPE_BMP;
		case ".wpg":
			return XWPFDocument.PICTURE_TYPE_WPG;
		default:
			return 0;
		}
//		  int format;
//		  if (lowerCase.endsWith(".emf"))
//		   format = XWPFDocument.PICTURE_TYPE_EMF;
//		  else if (imgFileName.endsWith(".wmf"))
//		   format = XWPFDocument.PICTURE_TYPE_WMF;
//		  else if (imgFileName.endsWith(".pict"))
//		   format = XWPFDocument.PICTURE_TYPE_PICT;
//		  else if (imgFileName.endsWith(".jpeg") || imgFileName.endsWith(".jpg"))
//		   format = XWPFDocument.PICTURE_TYPE_JPEG;
//		  else if (imgFileName.endsWith(".png"))
//		   format = XWPFDocument.PICTURE_TYPE_PNG;
//		  else if (imgFileName.endsWith(".dib"))
//		   format = XWPFDocument.PICTURE_TYPE_DIB;
//		  else if (imgFileName.endsWith(".gif"))
//		   format = XWPFDocument.PICTURE_TYPE_GIF;
//		  else if (imgFileName.endsWith(".tiff"))
//		   format = XWPFDocument.PICTURE_TYPE_TIFF;
//		  else if (imgFileName.endsWith(".eps"))
//		   format = XWPFDocument.PICTURE_TYPE_EPS;
//		  else if (imgFileName.endsWith(".bmp"))
//		   format = XWPFDocument.PICTURE_TYPE_BMP;
//		  else if (imgFileName.endsWith(".wpg"))
//		   format = XWPFDocument.PICTURE_TYPE_WPG;
//		  else {
//		   return 0;
//		  }
//		  return format;
		 }
}
