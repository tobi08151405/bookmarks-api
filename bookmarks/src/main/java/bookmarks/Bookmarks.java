package bookmarks;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFFooter;
import org.apache.poi.xwpf.usermodel.XWPFHeader;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBookmark;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 *
 * @author Mark Beardsley
 */
public class Bookmarks {

	private static Logger log = LoggerFactory.getLogger(Bookmarks.class);

	private String name;
	private XWPFDocument document;
	private HashMap<String, Bookmark> _bookmarks = null;

	/**
	 * Create an instance of the XWPFBookmakrs class using the following parameters.
	 *
	 * @param document An instance of the XWPFDocument class encapsulating
	 *                 information about a Word OOXML document.
	 * @param name     document (file) name, used for debug purposes only
	 */
	public Bookmarks(XWPFDocument document, String name) {
		
		this.document = document;
		this.name = name;
		this._bookmarks = new HashMap<String, Bookmark>();

		init();
	}

	private void init() {
		// Firstly, deal with any paragraphs in the body of the document.
		this.procParaList(document.getParagraphs());

		// and in header and footer
		for (XWPFHeader header : document.getHeaderList()) {
			this.procParaList(header.getParagraphs());
		}
		for (XWPFFooter footer : document.getFooterList()) {
			this.procParaList(footer.getParagraphs());
		}
		
		// Try to find bookmarks that are outside of a paragraph
		// see https://stackoverflow.com/q/64143049/606662
		this.procBodyList(document);

		// Then check to see if there are any bookmarks in table cells. To do this
		// it is necessary to get at the list of paragraphs 'stored' within the
		// individual table cell, hence this code which get the tables from the
		// document, the rows from each table, the cells from each row and the
		// paragraphs from each cell.
		for (XWPFTableRow row : TableUtil.allTableRows(document)) {
			for (XWPFTableCell cell : row.getTableCells()) {
				this.procParaList(cell, row);
			}
		}

		log.debug("Bookmarks found in {}: {}", this.name, this._bookmarks.keySet());
	}

	/**
	 * Get a reference to a specific bookmark.
	 *
	 * @param bookmarkName An instance of the String class that encapsulates the
	 *                     name of the bookmark.
	 * @return An instance of the Bookmark class that will encapsulate a reference
	 *         to a named bookmark if it exists within the document, null otherwise.
	 */
	public Bookmark getBookmark(String bookmarkName) {
		Bookmark bookmark = null;
		if (this._bookmarks.containsKey(bookmarkName)) {
			bookmark = this._bookmarks.get(bookmarkName);
		}
		return (bookmark);
	}

	/**
	 * Get all of the bookmarks in the document.
	 *
	 * @return An instance of a class that implements the Collections interface and
	 *         which makes it possible to access all of the bookmarks within the
	 *         document.
	 */
	public Collection<Bookmark> getBookmarkList() {
		return (this._bookmarks.values());
	}

	/**
	 * Gat an instance of the Iterator class that encapsulates the names of all of
	 * the bookmarks.
	 *
	 * @return An instance of the Iterator class that encapsulates references to one
	 *         or more String(s). Each String will itself encapsulate the name of a
	 *         bookmark.
	 */
	public Iterator<String> getNameIterator() {
		return (this._bookmarks.keySet().iterator());
	}

	/**
	 * Get the names of all of the bookmarks in the document.
	 *
	 * @return An instance of a class that implements the Set interface and which
	 *         contains the names of the bookmarks in this document. Note that this
	 *         assumes it is not possible to have more that one bookmark with the
	 *         same name in the same document.
	 */
	public Set<String> getBookmarkNameSet() {
		return (this._bookmarks.keySet());
	}

	/**
	 * Bookmark komplett entfernen, d.h. Inhalt und Bookmark-Start- und
	 * -End-Elemente.
	 * 
	 * <p>
	 * Da sich Bookmark-Start- und -End-Elemente verschieben k�nnen, wird am Ende
	 * die interne Datenstruktur neu aufgebaut. die Verschiebungen ergeben sich
	 * daraus, dass sich Bookmarks in .docx-Dateien manchmal �berschneiden, d.h.
	 * bevor ein Bookmark endet, beginnt bereits das n�chste. In solchen F�llen ist
	 * es n�tig, die Bookmark-Elemente, die sich innerhalb des Bereichs eines
	 * anderen Bookmarks befinden, nicht zu l�schen, sondern zu verschieben.
	 */
	public void deleteBookmark(String bookmark) {
		if (!this._bookmarks.containsKey(bookmark)) {
			log.warn("Bookmark '{}' does not exist, hence cannot be deleted", bookmark);
			return;
		}

		Bookmark b = getBookmark(bookmark);
		if (b.isCell()) {
			log.warn("Bookmark '{}' is part of a table cell and thus cannot be deleted", bookmark);
			return;
		}

		b.deleteBookmark();

		// Da sich einige Bookmarks verschoben haben k�nnten, Datenstruktur neu aufbauen
		init();

//		this._bookmarks.remove(bookmark);
	}
	
	private void procParaList(XWPFTableCell tableCell, XWPFTableRow tableRow) {
		List<XWPFParagraph> paraList = tableCell.getParagraphs();
		Iterator<XWPFParagraph> paraIter = null;
		XWPFParagraph para = null;
		List<CTBookmark> bookmarkList = null;
		Iterator<CTBookmark> bookmarkIter = null;
		CTBookmark bookmark = null;
		NamedNodeMap attributes = null;
		Node colFirstNode = null;
		Node colLastNode = null;
		int firstColIndex = 0;
		int lastColIndex = 0;

		// Step through the list of pargraphs and check whether or not each
		// one contains a bookmark. If it does, create an instance of the
		// Bookmark class encapsulating the bookmark/para pairing and
		// write away to the list
		paraIter = paraList.iterator();
		while (paraIter.hasNext()) {
			para = paraIter.next();
			bookmarkList = para.getCTP().getBookmarkStartList();
			bookmarkIter = bookmarkList.iterator();
			while (bookmarkIter.hasNext()) {
				bookmark = bookmarkIter.next();
				// With a bookmark in hand, test to see if the bookmarkStart tag
				// has w:colFirst or w:colLast attributes. If it does, we are
				// dealing with a bookmarked table cell. This will need to be
				// handled differently - I think by an different concrete class
				// that implements the Bookmark interface!!
				attributes = bookmark.getDomNode().getAttributes();
				if (attributes != null) {
					// Get the colFirst and colLast attributes. If both - for
					// now - are found, then we are dealing with a bookmarked
					// cell.
					colFirstNode = attributes.getNamedItem("w:colFirst");
					colLastNode = attributes.getNamedItem("w:colLast");
					if (colFirstNode != null && colLastNode != null) {
						// Get the index of the cell (or cells later) from them.
						// First convefrt the String values both return to primitive
						// int value. TO DO, what happens if there is a
						// NumberFormatException.
						firstColIndex = Integer.parseInt(colFirstNode.getNodeValue());
						lastColIndex = Integer.parseInt(colLastNode.getNodeValue());
						// if the indices are equal, then we are dealing with a#
						// cell and can create the bookmark for it.
						if (firstColIndex == lastColIndex) {
							addBookmark(para, bookmark, tableRow.getCell(firstColIndex));
						} else {
							log.error(
									"This bookmark " + bookmark.getName() + " identifies a number of cells in the "
											+ "table. That condition is not handled yet.");
						}
					} else {
						// 2020-11-26 V. 1.11 Alle Bookmarks, die in Zellen definiert werden, ben�tigen diese Info �ber ihre Zelle
						//   insbesondere relevant beim Kopieren von Paragraphen.
//						addBookmark(para, bookmark);
						addBookmark(para, bookmark, tableCell);
					}
				} else {
					// 2020-11-26 V. 1.11 Alle Bookmarks, die in Zellen definiert werden, ben�tigen diese Info �ber ihre Zelle
					//   insbesondere relevant beim Kopieren von Paragraphen.
//					addBookmark(para, bookmark);
					addBookmark(para, bookmark, tableCell);
				}
			}
		}
	}

	private void addBookmark(XWPFParagraph para, CTBookmark bookmark, XWPFTableCell tableCell) {
		this._bookmarks.put(bookmark.getName(),
				new Bookmark(bookmark, para, tableCell));
	}

	private void procParaList(List<XWPFParagraph> paraList) {
//		Iterator<XWPFParagraph> paraIter = null;
//		XWPFParagraph para = null;
//		List<CTBookmark> bookmarkList = null;
//		Iterator<CTBookmark> bookmarkIter = null;
//		CTBookmark bookmark = null;
		// Step through the list of paragraphs and check whether or not each
		// one contains a bookmark. If it does, create an instance of the
		// Bookmark class encapsulating the bookmark/para pairing and
		// write away to the list
//		paraIter = paraList.iterator();
//		while (paraIter.hasNext()) {
//			para = paraIter.next();
		for (XWPFParagraph para : paraList) {
			List<CTBookmark> bookmarkList = para.getCTP().getBookmarkStartList();
			procBookmarkList(para, bookmarkList);
		}
	}

	private void procBookmarkList(XWPFParagraph para, List<CTBookmark> bookmarkList) {
		Iterator<CTBookmark> bookmarkIter;
		CTBookmark bookmark;
		bookmarkIter = bookmarkList.iterator();
		while (bookmarkIter.hasNext()) {
			bookmark = bookmarkIter.next();
//				log.debug("Adding bookmark {}", bookmark.getName());
			addBookmark(para, bookmark);
		}
	}

	private void addBookmark(XWPFParagraph para, CTBookmark bookmark) {
		this._bookmarks.put(bookmark.getName(), new Bookmark(bookmark, para));
	}

	private void procBodyList(XWPFDocument document) {
		for (CTBookmark bookmark : document.getDocument().getBody().getBookmarkStartList()) {
			// search for the next paragraph, because a paragraph is needed to copy content from
			XmlCursor cursor = bookmark.newCursor();
			while (cursor.toNextSibling()) {
				XmlObject object = cursor.getObject();
				if (object instanceof CTP) {
					XWPFParagraph para = document.getParagraph((CTP)cursor.getObject());
					addBookmark(para, bookmark);
					break;
				}
			}
		}
	}
}
