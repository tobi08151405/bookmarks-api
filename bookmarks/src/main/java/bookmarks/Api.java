package bookmarks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import bookmarks.api.BookmarkApplier;

/**
 * See {@link BookmarkApplier} for entry point.
 * 
 * @author Markus Pscheidt
 *
 */
@SpringBootApplication
public class Api {

	public static void main(String[] args) {
		SpringApplication.run(Api.class, args);
	}

}
