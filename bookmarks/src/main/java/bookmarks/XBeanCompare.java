package bookmarks;


import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.impl.common.QNameHelper;
import org.apache.xmlbeans.impl.common.XmlWhitespace;

/**
 * This originates from an author called Dmitri.Coleba, see attached source here:
 * http://mail-archives.apache.org/mod_mbox/xml-xmlbeans-user/200310.mbox/%3cOF7B942026.9900E6EE-ONCA256DC8.00064356@tmca.com.au%3e
 * 
 * <p>
 * Deep comparison of two xml objects.
 * 
 * @author Markus Pscheidt
 *
 */
public class XBeanCompare {

	public static class Diagnostic {
		private StringBuffer message = new StringBuffer();

		void add(String s) {
			message.append(s).append("\n");
		}

		public String toString() {
			return message.toString();
		}
	}

	/**
	 * Provides a utility to compare the xml inside the two strings.
	 * 
	 * @return true if the xml inside the two strings is the same
	 */
	public static boolean lenientlyCompareTwoXmlStrings(XmlObject actualObj, XmlObject expectObj, Diagnostic diag) {
		boolean match = true;

		XmlCursor cur1 = actualObj.newCursor();
		XmlCursor cur2 = expectObj.newCursor();

		cur1.toFirstChild();
		cur2.toFirstChild();

		while (cur1.currentTokenType() != XmlCursor.TokenType.STARTDOC) {
			if (!compareNamesAndAttributes(cur1, cur2, diag)) {
				match = false;
			}

			boolean hasChildren1 = cur1.toFirstChild();
			boolean hasChildren2 = cur2.toFirstChild();
			if (hasChildren1 != hasChildren2) {
				diag.add("Topology differs: one document has " + "children where the other does not ("
						+ QNameHelper.pretty(cur1.getName()) + ", " + QNameHelper.pretty(cur2.getName()) + ")."); // TODO:
																													// where?
				match = false;
				if (hasChildren1) {
					cur1.toParent();
					hasChildren1 = false;
				}
				if (hasChildren2) {
					cur2.toParent();
					hasChildren2 = false;
				}
			} else if (hasChildren1 == false) {
				if (!wsCollapseEqual(cur1.getTextValue(), cur2.getTextValue())) {
					diag.add("Value '" + cur1.getTextValue() + "' differs from value '" + cur2.getTextValue() + "'.");
					match = false;
				}
			}

			if (hasChildren1)
				continue;

			for (;;) {
				boolean hasSibling1 = cur1.toNextSibling();
				boolean hasSibling2 = cur2.toNextSibling();

				if (hasSibling1 != hasSibling2) {
					diag.add("Topology differs: one document has " + "siblings where the other does not."); // TODO:
																											// where?
					hasSibling1 = false;
					hasSibling2 = false;
				}

				if (hasSibling1)
					break;

				cur1.toParent();
				cur2.toParent();

				if (cur1.currentTokenType() == XmlCursor.TokenType.STARTDOC)
					break;
			}
		}
		return match;
	}

	private static boolean compareNamesAndAttributes(XmlCursor cur1, XmlCursor cur2, Diagnostic diag) {
		if (!cur1.getName().equals(cur2.getName())) {
			diag.add("Element names '" + QNameHelper.pretty(cur1.getName()) + "' and '"
					+ QNameHelper.pretty(cur2.getName()) + "' do not match.");
			return false;
		}

		String elemName = QNameHelper.pretty(cur1.getName());

		boolean more = cur1.toFirstAttribute();
		if (more) {
			for (; more; more = cur1.toNextAttribute()) {
				String text1 = cur1.getTextValue();
				String text2 = cur2.getAttributeText(cur1.getName());
				if (text2 == null) {
					diag.add("Attribute '" + QNameHelper.pretty(cur1.getName()) + "' " + " of element '" + elemName
							+ "' not present.");
					return false;
				}

				if (!wsCollapseEqual(text1, text2)) {
					diag.add("Attribute values for '" + QNameHelper.pretty(cur1.getName()) + "' " + " of element '"
							+ elemName + "' don't match.");
					return false;
				}
			}
			cur1.toParent();
		}

		more = cur2.toFirstAttribute();
		if (more) {
			for (; more; more = cur2.toNextAttribute()) {
				String text1 = cur1.getAttributeText(cur2.getName());
				if (text1 == null) {
					diag.add("Attribute '" + QNameHelper.pretty(cur2.getName()) + "' " + " of element '" + elemName
							+ "' not present.");
					return false;
				}
			}
			cur2.toParent();
		}

		return true;
	}

	public static boolean wsCollapseEqual(String s1, String s2) {
		String s1c = XmlWhitespace.collapse(s1);
		String s2c = XmlWhitespace.collapse(s2);
		return (s1c.equals(s2c));
	}

//    public static void main(String[] args)
//        throws java.io.IOException
//    {
//        if (args.length < 2) {
//            System.err.println("Usage: XBeanCompare file1.xml file2.xml");
//            System.exit(1);
//        }
//
//        File file1 = new File(args[0]);
//        File file2 = new File(args[1]);
//
//        XmlObject xobj1 = XmlObject.Factory.parse(file1);
//        XmlObject xobj2 = XmlObject.Factory.parse(file2);
//
//        Diagnostic diag = new Diagnostic();
//        if (lenientlyCompareTwoXmlStrings(xobj1, xobj2, diag)) {
//            System.out.println("equal.");
//        }
//        else {
//            System.out.println(diag);
//        }
//    }
}

