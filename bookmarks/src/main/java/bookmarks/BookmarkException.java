package bookmarks;

/**
 * Indicate a problem during bookmark operations.
 * 
 * @author Markus Pscheidt
 *
 */
public class BookmarkException extends Exception {

	private static final long serialVersionUID = 1L;

	public BookmarkException(String message) {
		super(message);
	}

	public BookmarkException(String message, Throwable cause) {
		super(message, cause);
	}

	public BookmarkException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
