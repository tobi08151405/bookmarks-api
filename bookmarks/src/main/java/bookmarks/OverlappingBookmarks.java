package bookmarks;

import java.util.List;

import org.apache.xmlbeans.XmlObject;

public class OverlappingBookmarks {

	private List<XmlObject> onFirstParagraph;
	private List<XmlObject> onOtherParagraphs;
	private XmlObject bookmarkEnd;

	public List<XmlObject> getOnFirstParagraph() {
		return onFirstParagraph;
	}

	public void setOnFirstParagraph(List<XmlObject> onFirstParagraph) {
		this.onFirstParagraph = onFirstParagraph;
	}

	public List<XmlObject> getOnOtherParagraphs() {
		return onOtherParagraphs;
	}

	public void setOnOtherParagraphs(List<XmlObject> onOtherParagraphs) {
		this.onOtherParagraphs = onOtherParagraphs;
	}

	public XmlObject getBookmarkEnd() {
		return bookmarkEnd;
	}

	public void setBookmarkEnd(XmlObject bookmarkEnd) {
		this.bookmarkEnd = bookmarkEnd;
	}

	@Override
	public String toString() {
		return "OverlappingBookmarks [onFirstParagraph=" + onFirstParagraph + ", onOtherParagraphs=" + onOtherParagraphs
				+ ", bookmarkEnd=" + bookmarkEnd + "]";
	}

}
