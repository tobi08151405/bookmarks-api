package bookmarks.api;

/**
 * 
 * @author Markus Pscheidt
 *
 */
public class CopyFromTo {

	@Override
	public String toString() {
		return "CopyFromTo [fromBookmark=" + fromBookmark + ", toBookmark=" + toBookmark + "]";
	}

	private String fromBookmark;
	private String toBookmark;

	public String getFromBookmark() {
		return fromBookmark;
	}

	public void setFromBookmark(String fromBookmark) {
		this.fromBookmark = fromBookmark;
	}

	public String getToBookmark() {
		return toBookmark;
	}

	public void setToBookmark(String toBookmark) {
		this.toBookmark = toBookmark;
	}

}
