package bookmarks.api;

import org.springframework.stereotype.Service;

import bookmarks.Bookmarks;

/**
 * 
 * @author Markus Pscheidt
 *
 */
@Service
public class DeleteBookmarks {

	public void deleteBookmark(Bookmarks bookmarks, String bookmark) {

		bookmarks.deleteBookmark(bookmark);
	}

}
