package bookmarks.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author Markus Pscheidt
 *
 */
public class BookmarkData {

	private String sourceFile;
	private String targetFile;
	private Map<String, String> texts = new HashMap<>();
	private Map<String, ImageData> images = new HashMap<>();
	private Map<String, TableData> tables = new HashMap<>();
	private List<CopyData> copy = new ArrayList<>();
	private List<String> delete = new ArrayList<>();

	@Override
	public String toString() {
		return "BookmarkData [sourceFile=" + sourceFile + ", targetFile=" + targetFile + ", texts=" + texts
				+ ", images=" + images + ", tables=" + tables + ", copy=" + copy + ", delete=" + delete + "]";
	}

	public Map<String, String> getTexts() {
		return texts;
	}

	public void setTexts(Map<String, String> texts) {
		this.texts = texts;
	}

	public Map<String, ImageData> getImages() {
		return images;
	}

	public void setImages(Map<String, ImageData> images) {
		this.images = images;
	}

	public String getSourceFile() {
		return sourceFile;
	}

	public void setSourceFile(String sourceFile) {
		this.sourceFile = sourceFile;
	}

	public String getTargetFile() {
		return targetFile;
	}

	public void setTargetFile(String targetFile) {
		this.targetFile = targetFile;
	}

	public Map<String, TableData> getTables() {
		return tables;
	}

	public void setTables(Map<String, TableData> tables) {
		this.tables = tables;
	}

	public List<CopyData> getCopy() {
		return copy;
	}

	public void setCopy(List<CopyData> copy) {
		this.copy = copy;
	}

	public List<String> getDelete() {
		return delete;
	}

	public void setDelete(List<String> delete) {
		this.delete = delete;
	}

}
