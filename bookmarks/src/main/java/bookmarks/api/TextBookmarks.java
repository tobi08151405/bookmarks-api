package bookmarks.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ResponseStatusException;

import bookmarks.Bookmark;
import bookmarks.Bookmarks;

/**
 * 
 * @author Markus Pscheidt
 *
 */
@Service
public class TextBookmarks {
	
	private Logger log = LoggerFactory.getLogger(getClass());

	public void setText(Bookmarks bookmarks, String bookmark, String text) {

		Bookmark b = bookmarks.getBookmark(bookmark);
		if (b == null) {
			log.warn("Bookmark '{}' is not defined in document. Skipping.", bookmark);
			return;
		}

		if (!StringUtils.hasText(text)) {
			b.clearBookmark();
			return;
		}

		// Replace the bookmark content with text
		try {
			b.insertTextAtBookmark(text, Bookmark.REPLACE);
		} catch (RuntimeException e) {
			String reason = "Error on setting bookmark '" + bookmark + "'";
			log.error(reason, e);
			throw new ResponseStatusException(HttpStatus.NOT_IMPLEMENTED, reason + ": " + e.getMessage(), e);
		}

	}
}
