package bookmarks.api;

import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlObject;
import org.w3c.dom.Node;

/**
 * DomNodes im Xml-Dokument verschieben.
 * 
 * @author Markus Pscheidt
 *
 */
public final class MoveContentUtil {

	private MoveContentUtil() {
	}

	public static void moveXInFrontOfY(Node parent, Node x, Node y) {
		parent.insertBefore(x, y);
	}

	/**
	 * x wird hinter y platziert. Es wird y.getParent() ermittelt.
	 * 
	 * <p>
	 * Je nachdem, ob y einen "nextSibling()" hat oder nicht, wird entweder
	 * insertBefore() oder appendChild() verwendet, um x direkt nach y einzufügen.
	 */
	public static void moveXAfterY(XmlObject x, XmlObject y) {

		XmlCursor endCursor = y.newCursor();
		Node yNode = y.getDomNode();
		if (endCursor.toNextSibling()) {
			XmlObject nextSibling = endCursor.getObject();
			yNode.getParentNode().insertBefore(x.getDomNode(), nextSibling.getDomNode());
		} else {
			yNode.getParentNode().appendChild(x.getDomNode());
		}
		endCursor.dispose();

	}
}
