package bookmarks.api;

import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import bookmarks.Bookmark;
import bookmarks.Bookmarks;

/**
 * 
 * @author Markus Pscheidt
 *
 */
@Service
public class ImageBookmarks {

	private Logger log = LoggerFactory.getLogger(getClass());

	public void fillImageBookmark(Bookmarks bookmarks, String bookmark, ImageData imageData) {

		log.info("Filling bookmark {} with {} in document ", bookmark, imageData);
//		BookmarksNavigator bookmarkNavigator = new BookmarksNavigator(doc);
//		bookmarkNavigator.moveToBookmark(bookmark);
		Bookmark b = bookmarks.getBookmark(bookmark);
		if (b == null) {
			log.warn("Bookmark '{}' is not defined in document. Skipping.", bookmark);
			return;
		}

		if (imageData == null || !StringUtils.hasText(imageData.getFilename())) {
			b.clearBookmark();
			return;
		}

		try {
			b.insertImageAtBookmark(imageData.getFilename(), imageData.getWidth(), imageData.getHeight());
		} catch (InvalidFormatException | IOException e) {
			log.error("cannot set image bookmark '" + bookmark + "'", e);
		}

		// Delete the bookmark content
//		bookmarkNavigator.deleteBookmarkContent(false);

		// Create a Paragraph object
//		Paragraph para = new Paragraph(doc);

		// Append a picture to the paragraph
//		String picPath = imageData.getFilename();
//        String picPath = "C:\\Users\\Administrator\\Desktop\\portrait.jpeg";  
//		InputStream inputStream = new FileInputStream(picPath);
//		DocPicture picture = para.appendPicture(inputStream);
//		DocPicture picture;
//		if (picPath != null) {
//			picture = para.appendPicture(picPath);
//		} else {
//			picture = para.appendPicture(imageData.getSource());
//		}

		// Set the height and width of the picture
//		picture.setHeight(120f);
//		picture.setWidth(90f);
//		picture.setHeight(4.06f);
//		picture.setWidth(5.75f);
//		picture.setHeight(4);  // Faktor: 61:4 = 15,25
//		picture.setWidth(16);  // Faktor: 238:16 = 14,875
//        int width = 238;
//		int width = imageData.getWidth();
//		int height = imageData.getHeight();
//		picture.setWidth(ImageSizeUtil.width(width));
//        picture.setHeight(ImageSizeUtil.height(height));
		
		// das kommt auch ganz gut hin, aber nur ohne Kommazahlen
//		picture.setWidth(178);
//		picture.setHeight(45);
//		picture.setWidthScale(10);
//		picture.setHeightScale(10);

//		float w = 238.0f;
//        picture.setWidth(w);
//        picture.setHeight(61f);
//        picture.setWidthScale(10);
//        picture.setHeightScale(10);

		// Set the horizontal and vertical alignment
//		picture.setHorizontalAlignment(ShapeHorizontalAlignment.Center);
//		picture.setVerticalAlignment(ShapeVerticalAlignment.Center);

		// Set text wrapping style
//		picture.setTextWrappingStyle(TextWrappingStyle.In_Front_Of_Text);

		// Insert the paragraph to the bookmark
//		bookmarkNavigator.insertParagraph(para);
	}

}
