package bookmarks.api;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Map;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import bookmarks.Bookmarks;

/**
 * Apply the given bookmark data: first, open word file. Second, set text, image
 * and table bookmarks. Third, save Word file.
 * 
 * @author Markus Pscheidt
 *
 */
@RestController
@RequestMapping
public class BookmarkApplier {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private CopyBookmarks copyParagraphs;
	
	@Autowired
	private DeleteBookmarks deleteBookmarks;

	@Autowired
	private ImageBookmarks imageBookmarks;

	@Autowired
	private TableBookmarks tableBookmarks;

	@Autowired
	private TextBookmarks textBookmarks;

	@GetMapping("alive")
	public boolean alive() {
		return true;
	}

	// Aufruf z.B. aus documentrevise.ctrl.js:424 - docxmarks() und PlaceImages()
	@PostMapping("apply")
	public void apply(@RequestBody BookmarkData data) {

		log.info("Apply {}", data);

		String sourceFile = data.getSourceFile();
		if (!StringUtils.hasText(sourceFile)) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No source file given");
		}
		String targetFile = data.getTargetFile();
		if (!StringUtils.hasText(targetFile)) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No target file given");
		}

		// Load the template document
//		Document doc = loadDoc(sourceFile);
		XWPFDocument document = DocumentFileUtil.loadDoc(sourceFile);
		Bookmarks bookmarks = new Bookmarks(document, sourceFile);

		if (data.getTexts() != null) {
			for (Map.Entry<String, String> entry : data.getTexts().entrySet()) {
				textBookmarks.setText(bookmarks, entry.getKey(), entry.getValue());
			}
		}

		if (data.getImages() != null) {
			for (Map.Entry<String, ImageData> imageEntry : data.getImages().entrySet()) {
				String bookmark = imageEntry.getKey();
				ImageData imageData = imageEntry.getValue();
				this.imageBookmarks.fillImageBookmark(bookmarks, bookmark, imageData);
			}
		}
		
		if (data.getTables() != null) {
			for (Map.Entry<String, TableData> tableEntry : data.getTables().entrySet()) {
				String bookmark = tableEntry.getKey();
				TableData tableData = tableEntry.getValue();
				this.tableBookmarks.fillTableBookmark(bookmarks, bookmark, tableData);
			}
		}
		
		if (data.getCopy() != null) {
			for (CopyData cpd : data.getCopy()) {
				copyParagraphs.copyParagraphs(bookmarks, cpd);
			}
		}

		if (data.getDelete() != null) {
			for (String bookmark : data.getDelete()) {
				deleteBookmarks.deleteBookmark(bookmarks, bookmark);
			}
		}

		// ask user to refresh fields upon opening the document
		document.enforceUpdateFields();

		// write file
		try(FileOutputStream out = new FileOutputStream(new File(targetFile))) {
			document.write(out);
		} catch (Exception e) {
			String reason = "Error on saving document " + targetFile;
			log.error(reason, e);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, reason + ": " + e.getMessage(), e);
		}
	}

}
