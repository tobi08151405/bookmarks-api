package bookmarks.api;

import java.util.Arrays;

/**
 * 
 * @author Markus Pscheidt
 *
 */
public class TableData {

	private String[][] data;
	
	public TableData() {
	}

	public TableData(String[][] data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "TableData [data=" + Arrays.deepToString(data) + "]";
	}

	public String[][] getData() {
		return data;
	}

	public void setData(String[][] data) {
		this.data = data;
	}

}
