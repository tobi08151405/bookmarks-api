package bookmarks.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import bookmarks.Bookmark;
import bookmarks.BookmarkException;
import bookmarks.Bookmarks;

/**
 * 
 * @author Markus Pscheidt
 *
 */
@Service
public class TableBookmarks {

	private Logger log = LoggerFactory.getLogger(getClass());

	public void fillTableBookmark(Bookmarks bookmarks, String bookmark, TableData tableData) {
		
		Bookmark b = bookmarks.getBookmark(bookmark);
		if (b == null) {
			log.warn("Bookmark '{}' is not defined in document. Skipping.", bookmark);
			return;
		}

		try {
			b.insertTableAtBookmark(tableData);
		} catch (BookmarkException e) {
			log.error("cannot set table bookmark '" + bookmark + "'", e);
		}
		
	}
	
}
