package bookmarks.api;

import java.util.ArrayList;
import java.util.List;

/**
 * Define which bookmark content of which source .docx file shall be copied to
 * which target bookmark in the {@link BookmarkData#getSourceFile()}.
 * 
 * @author Markus Pscheidt
 *
 */
public class CopyData {

	private String fromFile;
	private List<CopyFromTo> contents = new ArrayList<>();
	private List<CopyFromTo> oleObjects = new ArrayList<>();
	
	@Override
	public String toString() {
		return "CopyData [fromFile=" + fromFile + ", contents=" + contents + ", oleObjects=" + oleObjects + "]";
	}

	public String getFromFile() {
		return fromFile;
	}

	public void setFromFile(String fromFile) {
		this.fromFile = fromFile;
	}

	public List<CopyFromTo> getContents() {
		return contents;
	}

	public void setContents(List<CopyFromTo> contents) {
		this.contents = contents;
	}

	public List<CopyFromTo> getOleObjects() {
		return oleObjects;
	}

	public void setOleObjects(List<CopyFromTo> oleObjects) {
		this.oleObjects = oleObjects;
	}


}
