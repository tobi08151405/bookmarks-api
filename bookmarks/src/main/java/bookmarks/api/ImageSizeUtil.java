package bookmarks.api;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ImageSizeUtil {

	public static float width(int width) {
        BigDecimal faktorW = new BigDecimal("13.38");
        BigDecimal w0 = new BigDecimal(width);
		BigDecimal w1 = w0.divide(faktorW, 0, RoundingMode.HALF_UP);
		return w1.floatValue();
	}

	public static float height(int height) {
        BigDecimal faktorH = new BigDecimal("13.64");
        BigDecimal h0 = new BigDecimal(height);
        BigDecimal h1 = h0.divide(faktorH, 0, RoundingMode.HALF_UP);
        return h1.floatValue();
	}

}
