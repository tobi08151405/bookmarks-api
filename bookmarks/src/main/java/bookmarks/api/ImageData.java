package bookmarks.api;

import java.util.Arrays;

/**
 * 
 * @author Markus Pscheidt
 *
 */
public class ImageData {

	private String filename;
	private byte[] source;
	private Integer width;
	private Integer height;

	@Override
	public String toString() {
		return "ImageData [filename=" + filename + ", source=" + Arrays.toString(source) + ", width=" + width
				+ ", height=" + height + "]";
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public byte[] getSource() {
		return source;
	}

	public void setSource(byte[] source) {
		this.source = source;
	}

}
