package bookmarks.api;

import java.io.FileInputStream;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * Access documents. 
 * 
 * @author Markus Pscheidt
 *
 */
public final class DocumentFileUtil {

	private DocumentFileUtil() {
	}

	public static XWPFDocument loadDoc(String filename) {
		try(FileInputStream fis = new FileInputStream(filename)) {
			return new XWPFDocument(fis);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "File cannot be parsed: " + filename, e);
		}
	}

}
