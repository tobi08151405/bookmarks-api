package bookmarks.api;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import bookmarks.Bookmark;
import bookmarks.Bookmarks;

/**
 * 
 * @author Markus Pscheidt
 *
 */
@Service
public class CopyBookmarks {

	private Logger log = LoggerFactory.getLogger(getClass());

	public void copyParagraphs(Bookmarks toBookmarks, CopyData cpd) {
		String fromFile = cpd.getFromFile();
		XWPFDocument fromDoc = DocumentFileUtil.loadDoc(fromFile);
		Bookmarks fromBookmarks = new Bookmarks(fromDoc, fromFile);

		for (CopyFromTo content : cpd.getContents()) {
			copyParagraphsContent(toBookmarks, fromBookmarks, content);
		}

		for (CopyFromTo oleObject : cpd.getOleObjects()) {
			copyOleObject(toBookmarks, fromBookmarks, oleObject);
		}
	}

	private void copyParagraphsContent(Bookmarks toBookmarks, Bookmarks fromBookmarks, CopyFromTo content) {
		String fromBookmarkName = content.getFromBookmark();
		Bookmark fromBookmark = fromBookmarks.getBookmark(fromBookmarkName);
		if (fromBookmark == null) {
			log.warn("Bookmark '{}' is not defined in fromFile. Skipping.", fromBookmarkName);
			return;
		}

		String toBookmarkName = content.getToBookmark();
		Bookmark toBookmark = toBookmarks.getBookmark(toBookmarkName);
		if (toBookmark == null) {
			log.warn("Bookmark '{}' is not defined in targetFile. Skipping.", toBookmarkName);
			return;
		}

		toBookmark.copyAllParagraphsFrom(fromBookmark);
	}

	private void copyOleObject(Bookmarks toBookmarks, Bookmarks fromBookmarks, CopyFromTo oleObject) {
		String fromBookmarkName = oleObject.getFromBookmark();
		Bookmark fromBookmark = fromBookmarks.getBookmark(fromBookmarkName);
		if (fromBookmark == null) {
			log.warn("Bookmark '{}' is not defined in document fromFile. Skipping.", fromBookmarkName);
			return;
		}

		String toBookmarkName = oleObject.getToBookmark();
		Bookmark toBookmark = toBookmarks.getBookmark(toBookmarkName);
		if (toBookmark == null) {
			log.warn("Bookmark '{}' is not defined in targetFile. Skipping.", toBookmarkName);
			return;
		}

		toBookmark.copyOleObjectFrom(fromBookmark);
	}

}
