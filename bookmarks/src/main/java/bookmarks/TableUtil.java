package bookmarks;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;

/**
 * Tools fuers Arbeiten mit Tabellen, Rows, Cells.
 * 
 * @author Markus Pscheidt
 *
 */
public final class TableUtil {

	private TableUtil() {
		// not to be instantiated
	}

	/**
	 * @return Alle Rows einer Tabelle
	 */
	public static List<XWPFTableRow> allTableRows(XWPFDocument doc) {
		List<XWPFTableRow> rows = new ArrayList<>();
		
		List<XWPFTable> tableList = doc.getTables();
		Iterator<XWPFTable> tableIter = tableList.iterator();
		
		while (tableIter.hasNext()) {
			XWPFTable table = tableIter.next();
			rows.addAll(table.getRows());
		}
		
		return rows;
	}

	public static List<XWPFTableCell> allTableCells(XWPFDocument doc) {
		List<XWPFTableCell> cells = new ArrayList<>();
		
		for (XWPFTableRow row : allTableRows(doc)) {
			cells.addAll(row.getTableCells());
		}

		return cells;
	}
}
