package bookmarks;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import javax.imageio.ImageIO;

import org.apache.commons.compress.utils.IOUtils;
import org.apache.poi.ooxml.POIXMLDocumentPart;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.Document;
import org.apache.poi.xwpf.usermodel.IBodyElement;
import org.apache.poi.xwpf.usermodel.XWPFAbstractNum;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFNumbering;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFPicture;
import org.apache.poi.xwpf.usermodel.XWPFPictureData;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFStyle;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.main.CTBlipFillProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTNonVisualDrawingProps;
import org.openxmlformats.schemas.drawingml.x2006.main.CTNonVisualPictureProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTShapeProperties;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTAbstractNum;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBookmark;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTDecimalNumber;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTInd;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTLvl;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTMarkup;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTMarkupRange;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPBdr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTParaRPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTR;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTbl;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTcPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTText;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import bookmarks.XBeanCompare.Diagnostic;
import bookmarks.api.MoveContentUtil;
import bookmarks.api.TableData;

/**
 *
 * @author Mark Beardsley
 */
public class Bookmark {

	private static Logger log = LoggerFactory.getLogger(Bookmark.class);

	public static final int INSERT_AFTER = 0;
	public static final int INSERT_BEFORE = 1;
	public static final int REPLACE = 2;

	private static final String RUN_NODE_NAME = "w:r";
	private static final String TEXT_NODE_NAME = "w:t";
	private static final String BOOKMARK_START_TAG = "bookmarkStart";
	public static final String BOOKMARK_END_TAG = "bookmarkEnd";
	public static final String BOOKMARK_ID_ATTR_NAME = "w:id";
	private static final String STYLE_NODE_NAME = "w:rPr";

	private CTBookmark _ctBookmark;
	private XWPFParagraph _para;
	private XWPFTableCell _tableCell;
	private String _bookmarkName;
	private BigInteger _bookmarkId;
	private boolean _isCell;


	private Map<BigInteger, BigInteger> numIdMap = new HashMap<>();

	/**
	 * Create an instance of the Bookmark class using the following parameters.
	 *
	 * @param ctBookmark An instance of the CTBookmark class that encapsulates
	 *                   information about a bookmark within a Word document.
	 * @param para       An instance of the XWPFParagraph class that encapsulates
	 *                   information about a paragraph of text within a Word
	 *                   document. The reference passed to this parameter will be
	 *                   for the paragraph that contains the bookmark.
	 */
	protected Bookmark(CTBookmark ctBookmark, XWPFParagraph para) {
		this._ctBookmark = ctBookmark;
		this._para = para;
		this._bookmarkName = ctBookmark.getName();
		this._bookmarkId = ctBookmark.getId();
		this._tableCell = null;
		this._isCell = false;
//        System.out.println("Normale Textmarke: " + ctBookmark.getName());
	}

	protected Bookmark(CTBookmark ctBookmark, XWPFParagraph para, XWPFTableCell tableCell) {
		this(ctBookmark, para);
		this._tableCell = tableCell;
		this._isCell = true;
//        System.out.println("Zellen-Textmarke: " + ctBookmark.getName());
	}

	protected Bookmark(CTBookmark ctBookmark, XWPFParagraph para, List<XWPFTableCell> tableCellList) {
		// Adding this constructor in, just in case it is possible to
		// bookmark two or more cells in a table.
	}

	/**
	 * Get the name of the bookmark.
	 *
	 * @return An instance of the String class that encapsulates the name of the
	 *         bookmark as seen in the Insert->Bookmark dialog box.
	 */
	public String getBookmarkName() {
		return (this._bookmarkName);
	}

	public BigInteger getBookmarkId() {
		return _bookmarkId;
	}

	public XWPFParagraph getParagraph() {
		return _para;
	}
	
	public boolean isCell() {
		return this._isCell;
	}
	
	public CTBookmark getCTBookmark() {
		return this._ctBookmark;
	}

	@Override
	public String toString() {
		return "Bookmark [_bookmarkId=" + _bookmarkId + ", _bookmarkName=" + _bookmarkName + ", _isCell=" + _isCell
				+ "]";
	}

	/**
	 * Insert text into the Word document in the location indicated by this
	 * bookmark.
	 *
	 * @param bookmarkValue An instance of the String class that encapsulates the
	 *                      text to insert into the document.
	 * @param where         A primitive int whose value indicates where the text
	 *                      ought to be inserted. There are three options controlled
	 *                      by constants; insert the text immediately in front of
	 *                      the bookmark (Bookmark.INSERT_BEFORE), insert text
	 *                      immediately after the bookmark (Bookmark.INSERT_AFTER)
	 *                      and replace any and all text that appears between the
	 *                      bookmark's square brackets (Bookmark.REPLACE).
	 */
	public void insertTextAtBookmark(String bookmarkValue, int where) {
		XWPFRun run = null;
		// Are we dealing with a bookmarked table cell?
//		if (this._isCell) {
//			this.handleTextInBookmarkedCells(bookmarkValue);
//		} else {
			// We are not dealing with a table cell so, create the run for the
			// paragraph then check how it ought to be handled.
			run = this._para.createRun();

			setText(bookmarkValue, run);

			switch (where) {
			case Bookmark.INSERT_AFTER:
				this.insertAfterBookmark(run);
				break;
			case Bookmark.INSERT_BEFORE:
				this.insertBeforeBookmark(run);
				break;
			case Bookmark.REPLACE:
				this.replaceBookmark(run);
				break;
			}
//		}
	}

	/**
	 * Set text at run. Supports line breaks.
	 * 
	 * @param text may contain line breaks ({@code \r, \n})
	 * @param run  text is inserted at this run
	 */
	private void setText(String text, XWPFRun run) {
		if (text == null) {
			return;
		}
		
		// take into account CR and LF and convert them into breaks
		String[] rows = text.split("\\r?\\n");
		for (int i = 0; i < rows.length; i++) {
			String row = rows[i];
			run.setText(row);
			if (i < rows.length - 1) {
				run.addBreak();
			}
		}
	}

	/**
	 * Same as {@link #setText(String, XWPFRun)}, but starting text replacement at
	 * give position within the run's text array. Set text at run. Supports line
	 * breaks and starts at given run position.
	 * 
	 * @param text     may contain line breaks ({@code \r, \n})
	 * @param run      text is inserted at this run
	 * @param startPos run text array position. Setting this to 0 will overwrite
	 *                 existing text at position 0.
	 */
	private void setText(String text, XWPFRun run, int startPos) {
		// take into account CR and LF and convert them into breaks
		String[] rows = text.split("\\r?\\n");
		for (int i = 0; i < rows.length; i++) {
			String row = rows[i];
			run.setText(row, i + startPos);
			if (i < rows.length - 1) {
				run.addBreak();
			}
		}
	}

	/**
	 * Inserts some text into a Word document in a position that is immediately
	 * after a named bookmark.
	 *
	 * Bookmarks can take two forms, they can either simply mark a location within a
	 * document or they can do this but contain some text. The difference is obvious
	 * from looking at some XML markup. The simple placeholder bookmark will look
	 * like this;
	 *
	 * <pre>
	 * {@code
	 * <w:bookmarkStart w:name="AllAlone" w:id="0"/><w:bookmarkEnd w:id="0"/>
	 * }
	 * </pre>
	 *
	 * Simply a pair of tags where one tag has the name bookmarkStart, the other the
	 * name bookmarkEnd and both share matching id attributes. In this case, the
	 * text will simply be inserted into the document at a point immediately after
	 * the bookmarkEnd tag. No styling will be applied to the text, it will simply
	 * inherit the documents defaults.
	 *
	 * The more complex case looks like this;
	 *
	 * <pre>
	 * {@code
	 * <w:bookmarkStart w:name="InStyledText" w:id="3"/>
	 *   <w:r w:rsidRPr="00DA438C">
	 *     <w:rPr>
	 *       <w:rFonts w:hAnsi="Engravers MT" w:ascii="Engravers MT" w:cs="Arimo"/>
	 *       <w:color w:val="FF0000"/>
	 *     </w:rPr>
	 *     <w:t>text</w:t>
	 *   </w:r>
	 * <w:bookmarkEnd w:id="3"/>
	 * }
	 * </pre>
	 *
	 * Here, the user has selected the word 'text' and chosen to insert a bookmark
	 * into the document at that point. So, the bookmark tags 'contain' a character
	 * run that is styled. Inserting any text after this bookmark, it is important
	 * to ensure that the styling is preserved and copied over to the newly inserted
	 * text.
	 *
	 * The approach taken to dealing with both cases is similar but slightly
	 * different. In both cases, the code simply steps along the document nodes
	 * until it finds the bookmarkEnd tag whose ID matches that of the bookmarkStart
	 * tag. Then, it will look to see if there is one further node following the
	 * bookmarkEnd tag. If there is, it will insert the text into the paragraph
	 * immediately in front of this node. If, on the other hand, there are no more
	 * nodes following the bookmarkEnd tag, then the new run will simply be
	 * positioned at the end of the paragraph.
	 *
	 * Styles are dealt with by 'looking' for a 'w:rPr' element whilst iterating
	 * through the nodes. If one is found, its details will be captured and applied
	 * to the run before the run is inserted into the paragraph. If there are
	 * multiple runs between the bookmarkStart and bookmarkEnd tags and these have
	 * different styles applied to them, then the style applied to the last run
	 * before the bookmarkEnd tag - if any - will be cloned and applied to the newly
	 * inserted text.
	 *
	 * @param run An instance of the XWPFRun class that encapsulates the text that
	 *            is to be inserted into the document following the bookmark.
	 */
	private void insertAfterBookmark(XWPFRun run) {
		Node nextNode = null;
		Node insertBeforeNode = null;
		Node styleNode = null;
		int bookmarkStartID = 0;
		int bookmarkEndID = -1;

		// Capture the id of the bookmarkStart tag. The code will step through
		// the document nodes 'contained' within the start and end tags that have
		// matching id numbers.
		bookmarkStartID = this._ctBookmark.getId().intValue();

		// Get the node for the bookmark start tag and then enter a loop that
		// will step from one node to the next until the bookmarkEnd tag with
		// a matching id is fouind.
		nextNode = this._ctBookmark.getDomNode();
		while (bookmarkStartID != bookmarkEndID) {

			// Get the next node along and check to see if it is a bookmarkEnd
			// tag. If it is, get its id so that the containing while loop can
			// be terminated once the correct end tag is found. Note that the
			// id will be obtained as a String and must be converted into an
			// integer. This has been coded to fail safely so that if an error
			// is encuntered converting the id to an int value, the while loop
			// will still terminate.
			nextNode = nextNode.getNextSibling();
			if (nextNode.getNodeName().contains(Bookmark.BOOKMARK_END_TAG)) {
				try {
					bookmarkEndID = Integer.parseInt(
							nextNode.getAttributes().getNamedItem(Bookmark.BOOKMARK_ID_ATTR_NAME).getNodeValue());
				} catch (NumberFormatException nfe) {
					bookmarkEndID = bookmarkStartID;
				}
			} // If we are not dealing with a bookmarkEnd node, are we dealing
				// with a run node that MAY contains styling information. If so,
				// then get that style information from the run.
			else {
				if (nextNode.getNodeName().equals(Bookmark.RUN_NODE_NAME)) {
					styleNode = this.getStyleNode(nextNode);
				}
			}
		}

		// After the while loop completes, it should have located the correct
		// bookmarkEnd tag but we cannot perform an insert after only an insert
		// before operation and must, therefore, get the next node.
		insertBeforeNode = nextNode.getNextSibling();

		// Style the newly inserted text. Note that the code copies or clones
		// the style it found in another run, failure to do this would remove the
		// style from one node and apply it to another.
		if (styleNode != null) {
			run.getCTR().getDomNode().insertBefore(styleNode.cloneNode(true),
					run.getCTR().getDomNode().getFirstChild());
		}

		// Finally, check to see if there was a node after the bookmarkEnd
		// tag. If there was, then this code will insert the run in front of
		// that tag. If there was no node following the bookmarkEnd tag then the
		// run will be inserted at the end of the paragraph and this was taken
		// care of at the point of creation.
		if (insertBeforeNode != null) {
			this._para.getCTP().getDomNode().insertBefore(run.getCTR().getDomNode(), insertBeforeNode);
		}
	}

	/**
	 * Inserts some text into a Word document immediately in front of the location
	 * of a bookmark.
	 *
	 * This case is slightly more straightforward than inserting after the bookmark.
	 * For example, it is possible only to insert a new node in front of an existing
	 * node. When inserting after the bookmark, then end node had to be located
	 * whereas, in this case, the node is already known, it is the CTBookmark
	 * itself. The only information that must be discovered is whether there is a
	 * run immediately in front of the boookmarkStart tag and whether that run is
	 * styled. If there is and if it is, then this style must be cloned and applied
	 * the text which will be inserted into the paragraph.
	 *
	 * @param run An instance of the XWPFRun class that encapsulates the text that
	 *            is to be inserted into the document following the bookmark.
	 */
	private void insertBeforeBookmark(XWPFRun run) {
		Node insertBeforeNode = null;
		Node childNode = null;
		Node styleNode = null;

		// Get the dom node from the bookmarkStart tag and look for another
		// node immediately preceding it.
		insertBeforeNode = this._ctBookmark.getDomNode();
		childNode = insertBeforeNode.getPreviousSibling();

		// If a node is found, try to get the styling from it.
		if (childNode != null) {
			styleNode = this.getStyleNode(childNode);

			// If that previous node was styled, then apply this style to the
			// text which will be inserted.
			if (styleNode != null) {
				run.getCTR().getDomNode().insertBefore(styleNode.cloneNode(true),
						run.getCTR().getDomNode().getFirstChild());
			}
		}

		// Insert the text into the paragraph immediately in front of the
		// bookmarkStart tag.
		this._para.getCTP().getDomNode().insertBefore(run.getCTR().getDomNode(), insertBeforeNode);
	}

	/**
	 * Replace the text - if any - contained between the bookmarkStart and it's
	 * matching bookmarkEnd tag with the text specified. The technique used will
	 * resemble that employed when inserting text after the bookmark. In short, the
	 * code will iterate along the nodes until it encounters a matching bookmarkEnd
	 * tag. Each node encountered will be deleted unless it is the final node before
	 * the bookmarkEnd tag is encountered and it is a character run. If this is the
	 * case, then it can simply be updated to contain the text the users wishes to
	 * see inserted into the document. If the last node is not a character run, then
	 * it will be deleted, a new run will be created and inserted into the paragraph
	 * between the bookmarkStart and bookmarkEnd tags.
	 *
	 * @param run An instance of the XWPFRun class that encapsulates the text that
	 *            is to be inserted into the document following the bookmark.
	 */
	private void replaceBookmark(XWPFRun run) {

		Stack<Node> nodeStack = new Stack<Node>();
		int bookmarkStartID = this._ctBookmark.getId().intValue();
		Node nextNode = this._ctBookmark.getDomNode();
		nodeStack.push(nextNode);

		int bookmarkEndID = -1;
		// Loop through the nodes looking for a matching bookmarkEnd tag
		while (bookmarkStartID != bookmarkEndID) {
			nextNode = nextNode.getNextSibling();
			if (nextNode == null) {
				throw new RuntimeException("Bookmarks spanning several paragraphs not supported: " + getBookmarkName());
			}
			nodeStack.push(nextNode);

			// If an end tag is found, does it match the start tag? If so, end
			// the while loop.
			if (nextNode.getNodeName().contains(Bookmark.BOOKMARK_END_TAG)) {
				try {
					bookmarkEndID = Integer.parseInt(
							nextNode.getAttributes().getNamedItem(Bookmark.BOOKMARK_ID_ATTR_NAME).getNodeValue());
				} catch (NumberFormatException nfe) {
					bookmarkEndID = bookmarkStartID;
				}
//			} else if (nextNode.getNodeName().equals(Bookmark.RUN_NODE_NAME)) {
//				// remember the last <w:r> element
//				lastRunNode = nextNode;
			}
		}

		// If the stack of nodes found between the bookmark tags is not empty
		// then they have to be removed.
		if (!nodeStack.isEmpty()) {
//			if (lastRunNode != null) {
//
//				// Get the run's style - if any - and apply to the run that will be replacing
//				// it.
//				if ((lastRunNode.getNodeName().equals(Bookmark.RUN_NODE_NAME))) {
			Node styleNode = this.getStyleNodeOfBookmark();
//					styleNode = this.getStyleNode(lastRunNode);
			if (styleNode != null) {
				run.getCTR().getDomNode().insertBefore(styleNode.cloneNode(true),
						run.getCTR().getDomNode().getFirstChild());
			}
//				}
//			}

			// Delete any and all node that were found in between the start and
			// end tags. This is slightly safer that trying to delete the nodes
			// as they are found while stepping through them in the loop above.

			// If we are peeking, then this line can be commented out.
			// this._para.getCTP().getDomNode().removeChild(lastRunNode);
			this.deleteChildNodes(nodeStack);
		}

		// Place the text into position, between the bookmark tags.
		this._para.getCTP().getDomNode().insertBefore(run.getCTR().getDomNode(), nextNode);
	}

	/**
	 * When replacing the bookmark's text, it is necessary to delete any nodes that
	 * are found between matching start and end tags. Complications occur here
	 * because it is possible to have bookmarks nested within bookmarks to almost
	 * any level and it is important to not remove any inner or nested bookmarks
	 * when replacing the contents of an outer or containing bookmark. This code
	 * successfully handles the simplest occurrence - where one bookmark completely
	 * contains another - but not more complex cases where one bookmark overlaps
	 * another in the markup. That is still to do.
	 *
	 * @param nodeStack An instance of the Stack class that encapsulates references
	 *                  to any and all nodes found between the opening and closing
	 *                  tags of a bookmark.
	 */
	private void deleteChildNodes(Stack<Node> nodeStack) {
		Node toDelete = null;
		int bookmarkStartID = 0;
		int bookmarkEndID = 0;
		boolean inNestedBookmark = false;

		// The first element in the list will be a bookmarkStart tag and that
		// must not be deleted.
		for (int i = 1; i < nodeStack.size(); i++) {

			// Get an element. If it is another bookmarkStart tag then
			// again, we do not want to delete it, it's matching end tag
			// or any nodes that fall inbetween.
			toDelete = nodeStack.elementAt(i);
			if (toDelete.getNodeName().contains(Bookmark.BOOKMARK_START_TAG)) {
				bookmarkStartID = Integer
						.parseInt(toDelete.getAttributes().getNamedItem(Bookmark.BOOKMARK_ID_ATTR_NAME).getNodeValue());
				inNestedBookmark = true;
			} else if (toDelete.getNodeName().contains(Bookmark.BOOKMARK_END_TAG)) {
				bookmarkEndID = Integer
						.parseInt(toDelete.getAttributes().getNamedItem(Bookmark.BOOKMARK_ID_ATTR_NAME).getNodeValue());
				if (bookmarkEndID == bookmarkStartID) {
					inNestedBookmark = false;
				}
			} else {
				if (!inNestedBookmark) {
					this._para.getCTP().getDomNode().removeChild(toDelete);
				}
			}
		}
	}

	/**
	 * Delete all siblings after the bookmark start node.
	 * @return true if, within the paragraph, the bookmark end node was found whose id corresponds to this bookmark's id.
	 */
	private boolean deleteSiblingNodesAfterBookmarkStart() {
		Node nextNode = null;
		int bookmarkStartID = 0;
		int bookmarkEndID = -1;

		Stack<Node> nodeStack = new Stack<Node>();
		BigInteger idObj = this._ctBookmark.getId();
		bookmarkStartID = idObj.intValue();
		nextNode = this._ctBookmark.getDomNode();
		nodeStack.push(nextNode);

		// Loop through the nodes looking for a matching bookmarkEnd tag
		while (bookmarkStartID != bookmarkEndID) {
			nextNode = nextNode.getNextSibling();
			if (nextNode == null) {
				log.trace("Bookmark {}: Last sibling node found before finding bookmarkEnd tag", this._bookmarkName);
				break;
			} else {
				nodeStack.push(nextNode);
			}

			// If an end tag is found, does it match the start tag? If so, end
			// the while loop.
			if (nextNode.getNodeName().contains(Bookmark.BOOKMARK_END_TAG)) {
				try {
					bookmarkEndID = Integer.parseInt(
							nextNode.getAttributes().getNamedItem(Bookmark.BOOKMARK_ID_ATTR_NAME).getNodeValue());
				} catch (NumberFormatException nfe) {
//					bookmarkEndID = bookmarkStartID;
					log.warn("Bookmark {}: Something is wrong", _bookmarkName, nfe);
					break;
				}
//			} else if (nextNode.getNodeName().equals(Bookmark.RUN_NODE_NAME)) {
//				// remember the last <w:r> element
//				lastRunNode = nextNode;
			}
		}

		// Delete any and all node that were found in between the start and
		// end tags. This is slightly safer that trying to delete the nodes
		// as they are found while stepping through them in the loop above.

		// If we are peeking, then this line can be commented out.
		// this._para.getCTP().getDomNode().removeChild(lastRunNode);
		this.deleteChildNodes(nodeStack);
		
		return bookmarkStartID == bookmarkEndID;
	}

	
	private void deleteSiblingsBeforeBookmarkEnd(CTMarkup ctBookmarkEnd) {
		
//		Stack<Node> nodeStack = new Stack<Node>();

		// find the last run before the bookmarkEnd (it could of course be none)
//		CTR lastCtrBeforeBookmarkEnd = null;
		XmlCursor cursor = ctBookmarkEnd.newCursor();
		while (cursor.toPrevSibling()) {
			XmlObject o = cursor.getObject();
//			nodeStack.add(0, o);

			Node domNode = o.getDomNode();
			ctBookmarkEnd.getDomNode().getParentNode().removeChild(domNode);
			
//			if (o instanceof CTR) {
//				lastCtrBeforeBookmarkEnd = (CTR) o;
//				break;
//			}
		}
		cursor.dispose();

//		nodeStack.add(0, null);	// erstes wird ignoriert in deleteChildNodes()
//		this.deleteChildNodes(nodeStack);
		
		// if a run was found before the bookmarkEnd, add all runs up to that one to the filtered list
//		List<XWPFRun> filteredRuns = new ArrayList<>();
//		if (lastCtrBeforeBookmarkEnd != null) {
////			XWPFRun xwpfRun = toParagraph.getRun(lastCtrBeforeBookmarkEnd);
//			
//			for (XWPFRun run : fromPara.getRuns()) {
//				filteredRuns.add(run);
//				// bis zum letzten vor dem bookmarkEnd alle hinzufuegen
//				if (run.getCTR() == lastCtrBeforeBookmarkEnd) {
//					break;
//				}
//			}
//		}
//
//		return filteredRuns;
	}

	/**
	 * Recover styling information - if any - from another document node. Note that
	 * it is only possible to accomplish this if the node is a run (w:r) and this
	 * could be tested for in the code that calls this method. However, a check is
	 * made in the calling code as to whether a style has been found and only if a
	 * style is found is it applied. This method always returns null if it does not
	 * find a style making that checking process easier.
	 *
	 * @param parentNode An instance of the Node class that encapsulates a reference
	 *                   to a document node.
	 * @return An instance of the Node class that encapsulates the styling
	 *         information applied to a character run. Note that if no styling
	 *         information is found in the run OR if the node passed as an argument
	 *         to the parentNode parameter is NOT a run, then a null value will be
	 *         returned.
	 */
	private Node getStyleNode(Node parentNode) {
		Node childNode = null;
		Node styleNode = null;
		if (parentNode != null) {

			// If the node represents a run and it has child nodes then
			// it can be processed further. Note, whilst testing the code, it
			// was observed that although it is possible to get a list of a nodes
			// children, even when a node did have children, trying to obtain this
			// list would often return a null value. This is the reason why the
			// technique of stepping from one node to the next is used here.
			if (parentNode.getNodeName().equalsIgnoreCase(Bookmark.RUN_NODE_NAME) && parentNode.hasChildNodes()) {

				// Get the first node and catch it's reference for return if
				// the first child node is a style node (w:rPr).
				childNode = parentNode.getFirstChild();
				if (childNode.getNodeName().equals("w:rPr")) {
					styleNode = childNode;
				} else {
					// If the first node was not a style node and there are other
					// child nodes remaining to be checked, then step through
					// the remaining child nodes until either a style node is
					// found or until all child nodes have been processed.
					while ((childNode = childNode.getNextSibling()) != null) {
						if (childNode.getNodeName().equals(Bookmark.STYLE_NODE_NAME)) {
							styleNode = childNode;
							// Note setting to null here if a style node is
							// found in order order to terminate any further
							// checking
							childNode = null;
						}
					}
				}
			}
		}
		return (styleNode);
	}

	/**
	 * Get the text - if any - encapsulated by this bookmark. The creator of a Word
	 * document can chose to select one or more items of text and then insert a
	 * bookmark at that location. The highlighted text will appear between the
	 * square brackets that denote the location of a bookmark in the document's text
	 * and they will be returned by a call to this method.
	 *
	 * @return An instance of the String class encapsulating any text that appeared
	 *         between the opening and closing square bracket associated with this
	 *         bookmark.
	 * @throws XmlException Thrown if a problem is encountered parsing the XML
	 *                      markup recovered from the document in order to construct
	 *                      a CTText instance which may required to obtain the
	 *                      bookmarks text.
	 */
	public String getBookmarkText() throws XmlException {
		StringBuilder builder = null;
		// Are we dealing with a bookmarked table cell? If so, the entire
		// contents of the cell - if anything - must be recovered and returned.
		if (this._tableCell != null) {
			builder = new StringBuilder(this._tableCell.getText());
		} else {
			builder = this.getTextFromBookmark();
		}
		return (builder == null ? null : builder.toString());
	}

	/**
	 * There are two types of bookmarks. One is a simple placeholder whilst the
	 * second is still a placeholder but it 'contains' some text. In the second
	 * instance, the creator of the document has selected some text and then chosen
	 * to insert a bookmark there and the difference if obvious when looking at the
	 * XML markup.
	 *
	 * The simple case;
	 *
	 * <pre>
	 * {@code
	 * <w:bookmarkStart w:name="AllAlone" w:id="0"/><w:bookmarkEnd w:id="0"/>
	 * }
	 * </pre>
	 *
	 * The more complex case;
	 *
	 * <pre>
	 * {@code
	 * <w:bookmarkStart w:name="InStyledText" w:id="3"/>
	 *   <w:r w:rsidRPr="00DA438C">
	 *     <w:rPr>
	 *       <w:rFonts w:hAnsi="Engravers MT" w:ascii="Engravers MT" w:cs="Arimo"/>
	 *       <w:color w:val="FF0000"/>
	 *     </w:rPr>
	 *     <w:t>text</w:t>
	 *   </w:r>
	 * <w:bookmarkEnd w:id="3"/>
	 * }
	 * </pre>
	 *
	 * This method assumes that the user wishes to recover the content from any
	 * character run that appears in the markup between a matching pair of
	 * bookmarkStart and bookmarkEnd tags; thus, using the example above again, this
	 * method would return the String 'text' to the user. It is possible however for
	 * a bookmark to contain more than one run and for a bookmark to contain other
	 * bookmarks. In both of these cases, this code will return the text contained
	 * within any and all runs that appear in the XML markup between matching
	 * bookmarkStart and bookmarkEnd tags. The term 'matching bookmarkStart and
	 * bookmarkEndtags' here means tags whose id attributes have matching value.
	 *
	 * @return An instance of the StringBuilder class encapsulating the text
	 *         recovered from any character run elements found between the
	 *         bookmark's start and end tags. If no text is found then a null value
	 *         will be returned.
	 * @throws XmlException Thrown if a problem is encountered parsing the XML
	 *                      markup recovered from the document in order to construct
	 *                      a CTText instance which may be required to obtain the
	 *                      bookmarks text.
	 */
	private StringBuilder getTextFromBookmark() throws XmlException {
		int startBookmarkID = 0;
		int endBookmarkID = -1;
		Node nextNode = null;
		Node childNode = null;
		CTText text = null;
		StringBuilder builder = null;
		String rawXML = null;

		// Get the ID of the bookmark from it's start tag, the DOM node from the
		// bookmark (to make looping easier) and initialise the StringBuilder.
		startBookmarkID = this._ctBookmark.getId().intValue();
		nextNode = this._ctBookmark.getDomNode();
		builder = new StringBuilder();

		// Loop through the nodes held between the bookmark's start and end
		// tags.
		while (startBookmarkID != endBookmarkID) {

			// Get the next node and, if it is a bookmarkEnd tag, get it's ID
			// as matching ids will terminate the while loop..
			nextNode = nextNode.getNextSibling();
			if (nextNode.getNodeName().contains(Bookmark.BOOKMARK_END_TAG)) {

				// Get the ID attribute from the node. It is a String that must
				// be converted into an int. An exception could be thrown and so
				// the catch clause will ensure the loop ends neatly even if the
				// value might be incorrect. Must inform the user.
				try {
					endBookmarkID = Integer.parseInt(
							nextNode.getAttributes().getNamedItem(Bookmark.BOOKMARK_ID_ATTR_NAME).getNodeValue());
				} catch (NumberFormatException nfe) {
					endBookmarkID = startBookmarkID;
				}
			} else {
				// This is not a bookmarkEnd node and can processed it for any
				// text it may contain. Note the check for both type - it must
				// be a run - and contain children. Interestingly, it seems as
				// though the node may contain children and yet the call to
				// nextNode.getChildNodes() will still return an empty list,
				// hence the need to step through the child nodes.
				if (nextNode.getNodeName().equals(Bookmark.RUN_NODE_NAME) && nextNode.hasChildNodes()) {
					// Get the text from the child nodes.
					builder.append(this.getTextFromChildNodes(nextNode));
				}
			}
		}
		return (builder);
	}

	/**
	 * Iterates through all and any children of the Node whose reference will be
	 * passed as an argument to the node parameter, and recover the contents of any
	 * text nodes. Testing revealed that a node can be called a text node and yet
	 * report it's type as being something different, an element node for example.
	 * Calling the getNodeValue() method on a text node will return the text the
	 * node encapsulates but doing the same on an element node will not. In fact,
	 * the call will simply return a null value. As a result, this method will test
	 * the nodes name to catch all text nodes - those whose name is to 'w:t' and
	 * then it's type. If the type is reported to be a text node, it is a trivial
	 * task to get at it's contents. However, if the type is not reported as a text
	 * type, then it is necessary to parse the raw XML markup for the node to
	 * recover it's value.
	 *
	 * @param node An instance of the Node class that encapsulates a reference to a
	 *             node recovered from the document being processed. It should be
	 *             passed a reference to a character run - 'w:r' - node.
	 * @return An instance of the String class that encapsulates the text recovered
	 *         from the nodes children, if they are text nodes.
	 * @throws XmlException Thrown if a problem is encountered parsing the XML
	 *                      markup recovered from the document in order to construct
	 *                      the CTText instance which may be required to obtain the
	 *                      bookmarks text.
	 */
	private String getTextFromChildNodes(Node node) throws XmlException {
		NodeList childNodes = null;
		Node childNode = null;
		CTText text = null;
		StringBuilder builder = new StringBuilder();
		int numChildNodes = 0;

		// Get a list of chid nodes from the node passed to the method and
		// find out how many children there are in the list.
		childNodes = node.getChildNodes();
		numChildNodes = childNodes.getLength();

		// Iterate through the children one at a time - it is possible for a
		// run to ciontain zero, one or more text nodes - and recover the text
		// from an text type child nodes.
		for (int i = 0; i < numChildNodes; i++) {

			// Get a node and check it's name. If this is 'w:t' then process as
			// text type node.
			childNode = childNodes.item(i);

			if (childNode.getNodeName().equals(Bookmark.TEXT_NODE_NAME)) {

				// If the node reports it's type as txet, then simply call the
				// getNodeValue() method to get at it's text.
				if (childNode.getNodeType() == Node.TEXT_NODE) {
					builder.append(childNode.getNodeValue());
				} else {
					// Correct the type by parsing the node's XML markup and
					// creating a CTText object. Call the getStringValue()
					// method on that to get the text.
					text = CTText.Factory.parse(childNode);
					builder.append(text.getStringValue());
				}
			}
		}
		return (builder.toString());
	}

//	private void handleTextInBookmarkedCells(String bookmarkValue) {
//		XWPFParagraph para = replaceBookmarkedCellWithEmptyParagraph();
//		para.createRun().setText(bookmarkValue);
//	}
//
//	private void handleImageInBookmarkedCells(String imageFilename, int width, int height)
//			throws InvalidFormatException, FileNotFoundException, IOException {
//		XWPFParagraph para = replaceBookmarkedCellWithEmptyParagraph();
//		XWPFRun run = para.createRun();
//		addImageToRun(imageFilename, width, height, run);
//	}
//
//	private XWPFParagraph replaceBookmarkedCellWithEmptyParagraph() {
//		List<XWPFParagraph> paraList = null;
//		List<XWPFRun> runs = null;
//		XWPFParagraph para = null;
//		XWPFRun readRun = null;
//		// Get a list of paragraphs from the table cell and remove any and all.
//		paraList = this._tableCell.getParagraphs();
//		for (int i = 0; i < paraList.size(); i++) {
//			this._tableCell.removeParagraph(i);
//		}
//		para = this._tableCell.addParagraph();
//		return para;
//	}

	// MPS: self added methods

	/**
	 * Taken and adapted from replaceBookmark().
	 * 
	 * <p>
	 * Here, style is being lost.
	 */
	public void clearContent() {
		int bookmarkEndID = -1;

		Stack<Node> nodeStack = new Stack<Node>();
		int bookmarkStartID = this._ctBookmark.getId().intValue();
		Node nextNode = this._ctBookmark.getDomNode();
		nodeStack.push(nextNode);

		// Loop through the nodes looking for a matching bookmarkEnd tag
		while (bookmarkStartID != bookmarkEndID) {
			nextNode = nextNode.getNextSibling();
			nodeStack.push(nextNode);

			// If an end tag is found, does it match the start tag? If so, end
			// the while loop.
			if (nextNode.getNodeName().contains(Bookmark.BOOKMARK_END_TAG)) {
				try {
					bookmarkEndID = Integer.parseInt(
							nextNode.getAttributes().getNamedItem(Bookmark.BOOKMARK_ID_ATTR_NAME).getNodeValue());
				} catch (NumberFormatException nfe) {
					bookmarkEndID = bookmarkStartID;
				}
			}
			// else {
			// Place a reference to the node on the nodeStack
			// nodeStack.push(nextNode);
			// }
		}

		// If the stack of nodes found between the bookmark tags is not empty
		// then they have to be removed.
		if (!nodeStack.isEmpty()) {

			// Check the node at the top of the stack. If it is a run, get it's
			// style - if any - and apply to the run that will be replacing it.
			// lastRunNode = nodeStack.pop();
//			Node lastRunNode = nodeStack.peek();
//			if ((lastRunNode.getNodeName().equals(Bookmark.RUN_NODE_NAME))) {
//				Node styleNode = this.getStyleNode(lastRunNode);
//				if (styleNode != null) {
//					run.getCTR().getDomNode().insertBefore(styleNode.cloneNode(true),
//							run.getCTR().getDomNode().getFirstChild());
//				}
//			}

			// Delete any and all node that were found in between the start and
			// end tags. This is slightly safer that trying to delete the nodes
			// as they are found while stepping through them in the loop above.

			// If we are peeking, then this line can be commented out.
			// this._para.getCTP().getDomNode().removeChild(lastRunNode);
			this.deleteChildNodes(nodeStack);
		}
	}

	public XWPFRun createRun() {
		return this._para.createRun();
	}

	public void addRun(XWPFRun run) {

		// child is supposed to be bookmark start
		Node child = this._ctBookmark.getDomNode();
		Node sibling = child.getNextSibling();

		// Place the text into position, between the bookmark tags.
		this._para.getCTP().getDomNode().insertBefore(run.getCTR().getDomNode(), sibling);
	}

	public void clearBookmark() {
		// funktioniert ev. nicht fuer Bookmarks in Zellen
		XWPFRun run = this._para.createRun();
		this.replaceBookmark(run);
	}

	public void insertImageAtBookmark(String imageFilename) throws InvalidFormatException, IOException {
		insertImageAtBookmark(imageFilename, null, null);
	}

	public void insertImageAtBookmark(String imageFilename, Integer width, Integer height)
			throws InvalidFormatException, IOException {

		if (width == null || height == null) {
			File imageFile = new File(imageFilename);
			BufferedImage bimg1 = ImageIO.read(imageFile);
			width = bimg1.getWidth();
			height = bimg1.getHeight();
		}
		log.info("Bookmark {}: setting image {} with width = {}, height = {}", _bookmarkName, imageFilename, width, height);

		XWPFRun run = null;
		// Are we dealing with a bookmarked table cell?
		// 2021-01-04 Bookmarks in Zellen werden gleich behandelt wie andere Bookmarks 
//		if (this._isCell) {
//			this.handleImageInBookmarkedCells(imageFilename, width, height);
//		} else {
			// We are not dealing with a table cell so, create the run for the
			// paragraph then check how it ought to be handled.
			run = this._para.createRun();
			addImageToRun(imageFilename, width, height, run);

			this.replaceBookmark(run);
//		}
	}

	private void addImageToRun(String imageFilename, int width, int height, XWPFRun run)
			throws InvalidFormatException, IOException, FileNotFoundException {
		try (InputStream is = new FileInputStream(imageFilename)) {
			int type = ImageUtil.getImageFormat(imageFilename);
			run.addPicture(is, type, imageFilename, Units.pixelToEMU(width), Units.pixelToEMU(height));
		}
	}

	public void insertTableAtBookmark(TableData tableData) throws BookmarkException {

		if (!_isCell) {
			log.warn("Bookmark '{}' is no table cell. Aborting.", _bookmarkName);
			return;
		}

		XWPFTableRow topRow = _tableCell.getTableRow();
		XWPFTable table = topRow.getTable();
		int height = topRow.getHeight();

		int rowNum = getTableRowOfBookmark(topRow);
		log.debug("Bookmark {}: Insert of table data starts in row {}", _bookmarkName, rowNum);

		Node styleNode = this.getStyleNodeOfBookmarkInTableCell();
		cleanTableCells(table, rowNum);

		String[][] data = tableData.getData();
		int lastRowWithData = lastRowWithData(data);
		for (int i = 0; i <= lastRowWithData; i++) {
			XWPFTableRow tableRow = table.getRow(rowNum + i);
			if (tableRow == null) {
				tableRow = table.createRow();
			}
			
			tableRow.setHeight(height);
			for (int j = 0; j < data[i].length; j++) {
				String[] rowData = data[i];
				XWPFTableCell cell = tableRow.getCell(j);
				if (cell == null) {
					cell = tableRow.createCell();
				}
				// copy table cell properties <w:tcPr> for all cells except top row
				if (i != 0) {
					XWPFTableCell topCell = topRow.getCell(j);
					copyCellStyle(topCell, cell);
				}
				
				XWPFParagraph par = cell.getParagraphs().isEmpty() ? cell.addParagraph() : cell.getParagraphArray(0);

				if (i != 0) {
					// For all rows except first row, copy paragraph style within each cell from the first cell in the same column
					XWPFParagraph tableCellPara = getTableCellParagraph(topRow, j);
					copyParagraphProperties(tableCellPara, par);
				}

				XWPFRun run = par.createRun();
				setText(rowData[j], run);

				if (styleNode != null) {
					setStyleNode(run, styleNode);
				}
			}
		}
	}

	/**
	 * Copy cell style ({@code <w:tcPr>} elements) from source to target cell.
	 * Target cell style is replaced by source cell style.
	 * 
	 * <p>
	 * Example {@code <w:tcPr>} element:
	 * 
	 * <pre>
	 * {@code
	 * <w:tcW w:w="794" w:type="dxa"/>
	 * <w:tcBorders>
	 *   <w:bottom w:val="dotted" w:sz="4" w:space="0" w:color="808080" w:themeColor="background1" w:themeShade="80"/>
	 * </w:tcBorders>
	 * <w:vAlign w:val="center"/>
	 * }
	 * </pre>
	 * 
	 * {@code <w:vAlign w:val="center"/>} denotes the vertical alignment of the cell.
	 */
	private void copyCellStyle(XWPFTableCell sourceCell, XWPFTableCell targetCell) {
		CTTcPr cellStyle = sourceCell.getCTTc().getTcPr();
		if (cellStyle != null) {
			targetCell.getCTTc().setTcPr(cellStyle);
		}
	}

	private XWPFParagraph getTableCellParagraph(XWPFTableRow row, int i) {
		XWPFTableCell cell = row.getCell(i);
		List<XWPFParagraph> cellParagraphs = cell.getParagraphs();
		if (cellParagraphs.isEmpty()) {
			return null; 
		}
		
		return cellParagraphs.get(0);
	}

	private void setStyleNode(XWPFRun run, Node styleNode) {
		if (styleNode == null) {
			return;
		}
		run.getCTR().getDomNode().insertBefore(styleNode.cloneNode(true), run.getCTR().getDomNode().getFirstChild());
	}

	/**
	 * Get last row that has at least one column value that is not null
	 * 
	 * @return row index
	 */
	private int lastRowWithData(String[][] data) {
		for (int r = data.length - 1; r >= 0; r--) {
			String[] rowData = data[r];
			for (int c = 0; c < rowData.length; c++) {
				if (rowData[c] != null) {
					return r;
				}
			}
		}
		return 0;
	}

	private int getParagraphIndex(List<IBodyElement> bodyElementsRef, XWPFParagraph paragraph) {
		int index = -1;
		for (IBodyElement elem : bodyElementsRef) {
			if (elem instanceof XWPFParagraph) {
				index++;
			}
			if (elem == paragraph) {
				return index;
			}
		}
		return -1;
	}

	/**
	 * It's a dirty hack but I cannot find proper way to remove elements from cell's
	 * body elements
	 * 
	 * @param cell
	 * @return
	 */
	private List<IBodyElement> getBodyElementsRef(XWPFTableCell cell) {
		try {
			Field beField = cell.getClass().getDeclaredField("bodyElements");
			beField.setAccessible(true);
			return (List<IBodyElement>) beField.get(cell);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	private int getTableRowOfBookmark(XWPFTableRow row) {
		XWPFTable table = row.getTable();

		// determine the row
		for (int r = 0; r < table.getRows().size(); r++) {
			if (table.getRows().get(r) == row) {
				return r;
			}
		}

		// this should never be reached
		return 0;
	}

	private int getNCols(String[][] data) {
		int nCols = 0;
		for (int i = 0; i < data.length; i++) {
			String[] colsPerRow = data[i];
			if (colsPerRow.length > nCols) {
				nCols = colsPerRow.length;
			}
		}
		return nCols;
	}

	private Node getStyleNodeOfBookmark() {
		Node nextNode = null;
		Node lastRunNode = null;
//		Stack<Node> nodeStack = null;
		int bookmarkStartID = 0;
		int bookmarkEndID = -1;

//		nodeStack = new Stack<Node>();
		bookmarkStartID = this._ctBookmark.getId().intValue();
		nextNode = this._ctBookmark.getDomNode();
//		nodeStack.push(nextNode);

		// Loop through the nodes looking for a matching bookmarkEnd tag
		while (bookmarkStartID != bookmarkEndID) {
			nextNode = nextNode.getNextSibling();
			if (nextNode == null) {
				throw new RuntimeException("Bookmarks spanning several paragraphs not supported: " + getBookmarkName());
			}
//			nodeStack.push(nextNode);

			// If an end tag is found, does it match the start tag? If so, end
			// the while loop.
			if (nextNode.getNodeName().contains(Bookmark.BOOKMARK_END_TAG)) {
				try {
					bookmarkEndID = Integer.parseInt(
							nextNode.getAttributes().getNamedItem(Bookmark.BOOKMARK_ID_ATTR_NAME).getNodeValue());
				} catch (NumberFormatException nfe) {
					bookmarkEndID = bookmarkStartID;
				}
			} else if (nextNode.getNodeName().equals(Bookmark.RUN_NODE_NAME)) {
				// remember the last <w:r> element
				lastRunNode = nextNode;
			}
		}

		// If the stack of nodes found between the bookmark tags is not empty
		// then they have to be removed.
		if (lastRunNode != null) {
			return this.getStyleNode(lastRunNode);
		}
		return null;
	}

	/**
	 * Style is found inside the paragraph properties of a table cell paragraph (see {@code w:rPr>}:
	 * 
	 * <pre>
	 * {@code
	 * <w:p>
	 *   <w:pPr>
	 *     <w:rPr>
	 *       <w:rFonts w:cs="Arial"/>
	 *       <w:sz w:val="20"/>
	 *       <w:lang w:val="en-GB"/>
	 *     </w:rPr>
	 *   </w:pPr>
	 * </w:p>
	 * }
	 * </pre>
	 * 
	 * @return The {@code <w:rPr>} node
	 */
	private Node getStyleNodeOfBookmarkInTableCell() {

		if (_tableCell.getParagraphs() == null || _tableCell.getParagraphs().isEmpty()) {
			return null;
		}
		XWPFParagraph para = _tableCell.getParagraphs().get(0);
		NodeList pChildren = para.getCTP().getDomNode().getChildNodes();
		for (int pChildNo = 0; pChildNo < pChildren.getLength(); pChildNo++) {
			Node pChild = pChildren.item(pChildNo);
			if (pChild.getNodeName().equals("w:pPr")) {
				NodeList pprChildren = pChild.getChildNodes();
				for (int pprChildNo = 0; pprChildNo < pprChildren.getLength(); pprChildNo++) {
					Node pprChild = pprChildren.item(pprChildNo);
					if (pprChild.getNodeName().equals(STYLE_NODE_NAME)) {
						return pprChild;
					}
				}
			}
		}
		return null;
	}

	private void cleanTableCells(XWPFTable table, int startRowNumber) {

		for (int i = startRowNumber; i < table.getNumberOfRows(); i++) {
			XWPFTableRow tableRow = table.getRow(i);
			for (XWPFTableCell cell : tableRow.getTableCells()) {
				for (XWPFParagraph paragraph : cell.getParagraphs()) {
					// must not remove paragraphs, so that bookmark isn't destroyed
					// remove all runs - we expect that there is no other data than text
					for (int runNo = paragraph.getRuns().size() - 1; runNo >= 0; runNo--) {
						paragraph.removeRun(runNo);
					}
				}
			}
		}
	}

	public void insertBulletsAtBookmark(List<String> texts) {
		XWPFParagraph paragraph = _para;

		boolean first = true;

		// All bullet points shall have the same style, otherwise it might look awkward.
		// The bullet point style itself is set in the CTPPr,
		// the text style is set inside the run.
		CTR cTR = null; // for a deep copy of the run's low level object
		CTPPr ppr = (CTPPr) paragraph.getCTP().getPPr().copy();

		for (String text : texts) {

			if (first) {

				XWPFRun run = getOrCreateRun(paragraph);

				/*
				 * Take a deep copy of the run's low level object. This includes both w:rPr and
				 * w:t <w:rPr> <w:rFonts w:cs="Arial"/> <w:color w:val="FF0000"/> <w:sz
				 * w:val="20"/> <w:lang w:val="en-GB"/> </w:rPr> <w:t>TA</w:t>
				 */
				cTR = (CTR) run.getCTR().copy();
//				run.setText(text, 0);
				setText(text, run, 0);

				first = false;

			} else {

				XmlCursor cursor = paragraph.getCTP().newCursor();
				cursor.toNextSibling();
				XmlObject nextNode = cursor.getObject();

				if (nextNode instanceof CTP) {

					// If another bullet point already exists, simply overwrite its text
					CTP ctp = (CTP) nextNode;
					paragraph = paragraph.getDocument().getParagraph(ctp);
					setParagraphProperties(paragraph, ppr);
					XWPFRun run = getOrCreateRun(paragraph);
					setCTR(run, cTR);
//					run.setText(text, 0);
					setText(text, run, 0);

				} else {

					// create new paragraph and copy style
					paragraph = paragraph.getDocument().insertNewParagraph(cursor);
					setParagraphProperties(paragraph, ppr);

					XWPFRun run = paragraph.createRun();
					setCTR(run, cTR);
//					run.setText(text, 0);
					setText(text, run, 0);
				}
				cursor.dispose();
			}
		}

//		deleteRemainingNumberedParagraphs(paragraph);
		deleteRunsOfRemainingParagraphs(paragraph);
	}

	private void setCTR(XWPFRun run, CTR cTR) {
		if (cTR != null) {
			run.getCTR().set(cTR); // set template paragraph's run formatting
		}
	}

	private void setParagraphProperties(XWPFParagraph paragraph, CTPPr ppr) {
		if (ppr != null) {
			if (paragraph.getCTP().getPPr() == null) {
				paragraph.getCTP().addNewPPr();
			}
			paragraph.getCTP().getPPr().set(ppr);
		}
	}

	private void setParagraphRpr(XWPFParagraph paragraph, CTParaRPr pRpr) {
		if (pRpr != null) {
//			ctp.getPPr().getRPr()
			CTPPr pPr = paragraph.getCTP().getPPr();
			if (pPr == null) {
				pPr = paragraph.getCTP().addNewPPr();
			}
			CTParaRPr rPr = pPr.getRPr();
			if (rPr == null) {
				rPr = pPr.addNewRPr();
			}
			rPr.set(pRpr);
		}
	}

	private void deleteRunsOfRemainingParagraphs(XWPFParagraph paragraph) {
		XWPFDocument document = paragraph.getDocument();

		XmlCursor cursor = paragraph.getCTP().newCursor();

		while (cursor.toNextSibling()) {
			XmlObject nextNode = cursor.getObject();
			boolean clearing = false;
			if (nextNode instanceof CTP) {
				XWPFParagraph toClear = document.getParagraph((CTP) nextNode);
				clearing = toClear.getNumID() != null;
				if (clearing) {
					for (int i = 0; i < toClear.getRuns().size(); i++) {
						toClear.removeRun(i);
					}
				}
			}
			if (!clearing) {
				break;
			}
		}
		cursor.dispose();
	}

	// Problem with deleting numbered paragraphs is that the bookmark may get
	// deleted as well
	private void deleteRemainingNumberedParagraphs(XWPFParagraph paragraph) {
		XWPFDocument document = paragraph.getDocument();

		XmlCursor cursor = paragraph.getCTP().newCursor();

		// only once go to next sibling, after deletion cursor is automatically at the
		// correct position
		cursor.toNextSibling();
		while (true) {
			XmlObject nextNode = cursor.getObject();
			boolean deleting = false;
			if (nextNode instanceof CTP) {
				XWPFParagraph toDelete = document.getParagraph((CTP) nextNode);
				deleting = toDelete.getNumID() != null;
				if (deleting) {
					document.removeBodyElement(document.getPosOfParagraph(toDelete));
				}
			}
			if (!deleting) {
				break;
			}
		}
		cursor.dispose();
	}

	private XWPFRun getOrCreateRun(XWPFParagraph paragraph) {
		if (paragraph.getRuns() == null || paragraph.getRuns().isEmpty()) {
			return paragraph.createRun();
		}

		return paragraph.getRuns().get(0);
	}

	private void removeAllRuns(XWPFParagraph paragraph) {
		for (int i = paragraph.getRuns().size() - 1; i >= 0; i--) {
			paragraph.removeRun(i);
		}
	}

	/**
	 * Copies the embedded Excel workbook content. Problem is that the preview image
	 * (an .emf file) does not get refreshed. Refreshing has to be done manually by
	 * the user by double clicking on the file in the Word document. Then a new EMF
	 * preview image gets created.
	 * 
	 * <p>
	 * In both the source and the target bookmarks there needs to be an Excel
	 * object, because data is copied from the source Excel to the target Excel.
	 */
	public void copyOleObjectFrom(Bookmark other) {
		XWPFParagraph targetPara = this._para;
		XWPFParagraph sourcePara = other._para;

		// findOleObject durchsucht alles zwischen Bookmark-Start und -Ende, bis ein <w:object> gefunden wird,
		//  statt nur die Runs des dem Bookmark zugeordneten Paragraphen zu durchsuchen,
		// da der Paragraph des Bookmark-Start ev. vor dem Paragraph mit dem Ole-Objekt ist.
		PackagePart sourceOLEObject = findOleObject(other);
		PackagePart targetOLEObject = findOleObject(this);

		if (sourceOLEObject == null || targetOLEObject == null) {
			log.warn("Bookmark {}: sourceOLEObject ({}) or targetOLEObject ({}) not found. Skipping.",
					this._bookmarkName, sourceOLEObject, targetOLEObject);
			return;
		}

		try {

			InputStream is = sourceOLEObject.getInputStream();
			OutputStream os = targetOLEObject.getOutputStream();
			IOUtils.copy(is, os);
			os.close();
			is.close();

			log.info("Bookmark {}: Copy OLE object, size = {} bytes", _bookmarkName, targetOLEObject.getSize());

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Copy the (.emf) preview of the Excel sheet and insert as image.
	 * 
	 * <p>
	 * Source bookmark must contain the Excel object. Target bookmark is as simple
	 * as when inserting an image.
	 * 
	 * TODO read width/height from <v:shape>
	 * 
	 * <pre>
	 * {@code 
	 * <v:shape id="_x0000_i1031" type="#_x0000_t75" alt="" style=
	"width:441.75pt;height:167.25pt" o:ole="">
	 *   <v:imagedata r:id="rId14" o:title=""/>
	 * </v:shape>
	 * }
	 * </pre>
	 */
	public void copyOleImageFrom(Bookmark other) {
		XWPFParagraph targetPara = this._para;
		XWPFParagraph sourcePara = other._para;

		PackagePart sourceOLEObject = findOleImage(sourcePara.getRuns());

		if (sourceOLEObject == null) {
			log.warn("Bookmark {}: sourceOLEObject ({}) not found. Skipping.", this._bookmarkName, sourceOLEObject);
			return;
		}

		String filename = sourceOLEObject.getPartName().getURI().toString();
		int width = Units.toEMU(441.75);
		int height = Units.toEMU(167.25);

		try (InputStream is = sourceOLEObject.getInputStream()) {

			XWPFRun run = targetPara.createRun();
			run.addPicture(is, XWPFDocument.PICTURE_TYPE_EMF, filename, width, height);
			this.replaceBookmark(run);

		} catch (IOException | InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Test if an OLE Objekt (Excel spreadsheet) exists.
	 * @return
	 */
	public boolean existsOleObject() {
//		PackagePart oleObject = this.findOleObject(this._para.getRuns());
		PackagePart oleObject = this.findOleObject(this);
		return oleObject != null;
	}

	private PackagePart findOleObject(List<XWPFRun> runs) {
		for (XWPFRun r : runs) {
			PackagePart oleObject = findOleObject(r);
			if (oleObject != null) {
				return oleObject;
			}
		}
		return null;
	}
	
	private PackagePart findOleObject(Bookmark b) {

		// search in the bookmarks's paragraph
		PackagePart oleObject = this.findOleObject(b.getParagraph().getRuns());
		if (oleObject != null) {
			return oleObject;
		}
		
		if (BookmarkTagUtil.findBookmarkEndUsingPath(b.getParagraph().getCTP(), b.getBookmarkId()) != null) {
			// If the bookmark's paragraph contains the bookmark end tag, stop the search
			return null;
		}
		
		// search further paragraphs for the ole object until bookmark end tag found
		XmlCursor cursor = b.getParagraph().getCTP().newCursor();
		XWPFDocument document = b.getParagraph().getDocument();
		while (cursor.toNextSibling()) {

			XmlObject elem = cursor.getObject();
			if (elem instanceof CTP) {
				// CTR: run
				CTP ctp = (CTP) elem;
				oleObject = this.findOleObject(document.getParagraph(ctp).getRuns());
				if (oleObject != null) {
					return oleObject;
				}
				if (BookmarkTagUtil.findBookmarkEndUsingPath(ctp, b.getBookmarkId()) != null) {
					// If this paragraph contains the bookmark end tag, stop the search
					break;
				}
			} else if (isBookmarkEnd(elem, b.getBookmarkId().intValue())) {
				break;
			}
		}
		cursor.dispose();
		
		// Need to start search at the level at which the ole object can be found
//		XmlCursor cursor = b.getParagraph().getCTP().newCursor();
//		while (cursor.toNextSibling()) {
//
//			XmlObject elem = cursor.getObject();
//
//			if (elem instanceof CTR) {
//				// CTR: run
//				CTR ctr = (CTR) elem;
//				PackagePart packagePart = findOleObject(document, ctr);
//				if (packagePart != null) {
//					return packagePart;
//				}
//			} else {
//				// Stop searching if reached end of bookmark scope
//				Node domNode = elem.getDomNode();
//				if (domNode.getNodeName().contains(Bookmark.BOOKMARK_END_TAG)) {
//					int bookmarkEndID = Integer.parseInt(
//							domNode.getAttributes().getNamedItem(Bookmark.BOOKMARK_ID_ATTR_NAME).getNodeValue());
//					if (bookmarkEndID == b.getBookmarkId().intValue()) {
//						break;
//					}
//				}
//			}
//		}
		
		return null;
	}

	private boolean isBookmarkEnd(XmlObject xmlObject, int bookmarkId) {

		Node domNode = xmlObject.getDomNode();
		if (domNode.getNodeName().contains(Bookmark.BOOKMARK_END_TAG)) {
			int bookmarkEndID = Integer.parseInt(
					domNode.getAttributes().getNamedItem(Bookmark.BOOKMARK_ID_ATTR_NAME).getNodeValue());
			if (bookmarkEndID == bookmarkId) {
				return true;
			}
		}

		return false;
	}

	private PackagePart findOleImage(List<XWPFRun> runs) {
		for (XWPFRun r : runs) {
			PackagePart oleObject = findImageData(r);
			if (oleObject != null) {
				return oleObject;
			}
		}
		return null;
	}

	private void cloneRunWithImage(XWPFRun targetRun, XWPFRun sourceRun) {
		CTRPr rPr = targetRun.getCTR().isSetRPr() ? targetRun.getCTR().getRPr() : targetRun.getCTR().addNewRPr();
		rPr.set(sourceRun.getCTR().getRPr());
//	    clone.setText(source.getText(0));
//	    targetRun.getCTR().set(sourceRun.getCTR().copy());

		for (XWPFPicture pic : sourceRun.getEmbeddedPictures()) {
			byte[] img = pic.getPictureData().getData();

			long cx = pic.getCTPicture().getSpPr().getXfrm().getExt().getCx();
			long cy = pic.getCTPicture().getSpPr().getXfrm().getExt().getCy();

			try {
				// Working addPicture Code below...
				XWPFParagraph dstPr = (XWPFParagraph) targetRun.getParent();
//                String blipId = dstPr.getDocument().addPictureData(new ByteArrayInputStream(img),
//                        Document.PICTURE_TYPE_EMF);

				XWPFDocument destDoc = dstPr.getDocument();
				destDoc.addPictureData(img, Document.PICTURE_TYPE_EMF);

			} catch (InvalidFormatException e1) {
				e1.printStackTrace();
			}
		}

	}

	private PackagePart findImageData(XWPFRun run) {
		CTR ctr = run.getCTR();
		String declareNameSpaces = "declare namespace v='urn:schemas-microsoft-com:vml'";
		XmlObject[] imageDataObjects = ctr.selectPath(declareNameSpaces + ".//v:imagedata");

		for (XmlObject oleObject : imageDataObjects) {
			XmlObject rIdAttribute = oleObject
					.selectAttribute("http://schemas.openxmlformats.org/officeDocument/2006/relationships", "id");
			if (rIdAttribute != null) {
				String rId = rIdAttribute.newCursor().getTextValue();
				return findImageObject(run.getDocument(), rId);
			}
		}
		return null;
	}

	private PackagePart findImageObject(XWPFDocument document, String rId) {
		POIXMLDocumentPart documentPart = document.getRelationById(rId);
		PackagePart packagePart = documentPart.getPackagePart();
		return packagePart;
	}

	private PackagePart findOleObject(XWPFRun run) {
		CTR ctr = run.getCTR();
		return findOleObject(run.getDocument(), ctr);
	}

	private PackagePart findOleObject(XWPFDocument document, CTR ctr) {
		String declareNameSpaces = "declare namespace o='urn:schemas-microsoft-com:office:office'";
		XmlObject[] oleObjects = ctr.selectPath(declareNameSpaces + ".//o:OLEObject");

		for (XmlObject oleObject : oleObjects) {
			XmlObject rIdAttribute = oleObject
					.selectAttribute("http://schemas.openxmlformats.org/officeDocument/2006/relationships", "id");
			if (rIdAttribute != null) {
				String rId = rIdAttribute.newCursor().getTextValue();
				return findOleObject(document, rId);
			}
		}
		return null;
	}

	private PackagePart findOleObject(XWPFDocument document, String rId) {
		POIXMLDocumentPart documentPart = document.getRelationById(rId);
		PackagePart packagePart = documentPart.getPackagePart();
		String contentType = packagePart.getContentType();
		if ("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet".equals(contentType)) {
			return packagePart;
		} // else if ...
		else {
			log.error("Bookmark {}: unknown workbook type: {}", _bookmarkName, contentType);
		}
		return null;
	}

	/**
	 * If bookmarkStart and bookmarkEnd are part of the same paragraph, it is not
	 * possible to insert additional paragraphs between bookmarkStart and
	 * bookmarkEnd, because one paragraph cannot be put inside another paragraph.
	 * 
	 * <p>
	 * One way is to define bookmarks in a way that bookmarkStart and bookmarkEnd do
	 * not belong to the same paragraph.
	 * 
	 * <p>
	 * The other way - which is done here - is to move the bookmarkEnd to after the
	 * paragraph that contains it.
	 */
	private Node assertBookmarkEndIsNotInSameParagraphAsBookmarkStart() {
		Node nextNode = null;
		int bookmarkStartID = 0;
		int bookmarkEndID = -1;

		BigInteger idObj = this._ctBookmark.getId();
		bookmarkStartID = idObj.intValue();
		nextNode = this._ctBookmark.getDomNode();

		// Loop through the nodes looking for a matching bookmarkEnd tag
		while (bookmarkStartID != bookmarkEndID) {
			nextNode = nextNode.getNextSibling();
			if (nextNode == null) {
				// OK, bookmarkEnd is not within this paragraph, we are fine
				return null;
			}

			// If an end tag is found, does it match the start tag? If so, end
			// the while loop.
			if (nextNode.getNodeName().contains(Bookmark.BOOKMARK_END_TAG)) {
				try {
					bookmarkEndID = Integer.parseInt(
							nextNode.getAttributes().getNamedItem(Bookmark.BOOKMARK_ID_ATTR_NAME).getNodeValue());
				} catch (NumberFormatException nfe) {
					log.warn("Bookmark {}: Something is wrong", _bookmarkName, nfe);
					return null;
				}
			}
		}
		
		// Der Node, der das Bookmark-End enth�lt: 
		// Dieser wird aus der aktuellen Position entfernt und sp�ter am Ende des letzten eingef�gten Bookmarks eingef�gt werden
		return nextNode; //.getParentNode().removeChild(nextNode);

		
//		Node ctpNode = _para.getCTP().getDomNode();
//		Node insertBeforeNode = ctpNode.getNextSibling();
//		if (insertBeforeNode != null) {
//			// Move the bookmark-end to after the paragraph
//			Node removedChild = nextNode.getParentNode().removeChild(nextNode); // TODO entfernen ist vermutlich gar nicht noetig, sondern passiert ohnehin, siehe JavaDoc von insertBefore
//			this._para.getDocument().getDocument().getBody().getDomNode().insertBefore(removedChild, insertBeforeNode);
//		}
	}
	
	/**
	 * Situation is the following:
	 * 
	 * <pre>
	 * {@code
	 * <w:p>
	 *  <w:bookmarkStart/>
	 *  <w:r></w:r>
	 *  <w:r></w:r>    // any number of runs
	 * </w:p>
	 * 
	 * <w:p>...</w:p>
	 * <w:p>...</w:p>  // any number of paragraphs
	 * 
	 * <w:bookmarkEnd/>
	 * } </pre
	 * 
	 * The bookmark starts within a paragraph, whereas the bookmark ends after (or
	 * within) a paragraph.
	 * 
	 * <p>
	 * The strategy is:
	 * <ul>
	 * <li>First paragraph (the one that includes bookmarkStart): Copy all runs
	 * starting with the run following bookmarkStart until end of paragraph or until
	 * bookmarkEnd is encountered.
	 * <li>All parapraphs between first and last: copy entire paragraph
	 * <li>Last paragraph (only if bookmarkEnd is within paragraph): Copy all runs
	 * up to bookmarkEnd
	 * <li>If bookmarkEnd is sibling of last paragraph, then it must be recognized
	 * (to avoid copying everything until end of document)
	 * </ul>
	 * <p>
	 * take all the runs starting after the w:bookmarkStart.
	 * 
	 * @param fromBookmark the bookmark in the source document
	 */
	public void copyAllParagraphsFrom(Bookmark fromBookmark) {
		
//		if (fromBookmark._isCell) {
//			log.warn("Bookmark '{}' (in fromFile) is a table cell: cannot copy table contents. Skipping", this._bookmarkName);
//			return;
//		}
//		if (this._isCell) {
//			log.warn("Bookmark '{}' (in targetFile) is a table cell: cannot copy table contents. Skipping", this._bookmarkName);
//			return;
//		}

		XWPFParagraph fromPara = fromBookmark.getParagraph();
		XWPFDocument fromDoc = fromPara.getDocument();

		XWPFParagraph myParagraph = _para;
		XWPFDocument myDocument = _para.getDocument();

		copyParagraphStyleAndNumbering(fromPara, myParagraph);

		clearMyBookmarContent(myParagraph);

		// All bullet points shall have the same style, otherwise it might look awkward.
		// The bullet point style itself is set in the CTPPr,
		// the text style is set inside the run.
//		CTR cTR = null; // for a deep copy of the run's low level object
//		CTPPr ppr = (CTPPr) paragraph.getCTP().getPPr().copy();

//		List<XWPFRun> runs = fromPara.getRuns();
//		copyRunsInto(runs, myParagraph);
		// Copy all runs starting after bookmarkStart until before bookmarkEnd or end of paragraph
		copyAllRunsFromBookmarkStart(fromBookmark);
		// If bookmarkEnd is part of the first paragraph (in source doc), then we're done
		// (This is the case when copying simple texts that do not span multiple paragraphs
		if (BookmarkTagUtil.findBookmarkEndUsingPath(fromBookmark.getParagraph().getCTP(), fromBookmark.getBookmarkId()) != null) {
			return;
		}

		// At this point it is clear that the source bookmark spans multiple paragraphs,
		// therefore ensure that our bookmark also spans more than a single paragraph
		Node bookmarkEndNodeToMove = assertBookmarkEndIsNotInSameParagraphAsBookmarkStart();

		// Copy each paragraph in full or the paragraph runs inside the bookmark
		// boundaries
		while (true) {

			XmlCursor cursor = fromPara.getCTP().newCursor();
			try {
				cursor.toNextSibling();
				XmlObject otherParaNode = cursor.getObject();

				if (otherParaNode instanceof CTP) {

					CTP fromCTP = (CTP) otherParaNode;

//dummyGetParagraph(fromDoc, fromCTP);

//					fromPara = fromDoc.getParagraph(fromCTP);
					fromPara = ParagraphFinder.findParagraph(fromDoc, fromCTP);
	//				XWPFParagraph srcPr = srcDoc.getParagraph(srcCTP);
					// create a paragraph on my side
					XmlCursor myParaCursor = myParagraph.getCTP().newCursor();
					if (myParaCursor.toNextSibling()) {
						// als n�chsten Node einf�gen
						if (_isCell) {
							myParagraph = _tableCell.insertNewParagraph(myParaCursor);
						} else {
							myParagraph = myDocument.insertNewParagraph(myParaCursor);
						}
					} else {
						// als letzten Node einf�gen
						if (_isCell) {
							myParagraph = _tableCell.addParagraph();
						} // TODO else falls es im Dokument an letzter Stelle eingef�gt werden soll
					}
					myParaCursor.dispose();

					copyParagraphStyleAndNumbering(fromPara, myParagraph);

					// is there a bookmarkEnd within this paragraph?
					CTMarkup ctBookmarkEnd = BookmarkTagUtil.findBookmarkEndUsingPath(fromCTP, fromBookmark.getBookmarkId());
					if (ctBookmarkEnd == null) {
						copyRunsInto(fromPara.getRuns(), myParagraph);
					} else {
						List<XWPFRun> runsToCopy = filterRunsBeforeBookmarkEnd(fromPara, ctBookmarkEnd);
						copyRunsInto(runsToCopy, myParagraph);
						break;
					}
				} else if (isBookmarkEnd(otherParaNode, fromBookmark.getBookmarkId().intValue())) {
					log.trace("Bookmark {}: BookmarkEnd found following a paragraph node. Finished with copyAllParagraphs operation.", _bookmarkName);
					break;
				} else {
					// Falls nicht CTP, koennte dies durchaus das bookmarkEnd-Element sein
					log.debug("Bookmark {}: Copy all paragraphs: discovered unknown element {}, therefore breaking", _bookmarkName, otherParaNode);
					break;
				}
			}
			finally {
				cursor.dispose();
			}
		}

		if (bookmarkEndNodeToMove != null) {
			myParagraph.getCTP().getDomNode().appendChild(bookmarkEndNodeToMove);
//			Node insertBeforeNode = myParagraph.getCTP().getDomNode().getNextSibling();
//			myParagraph.getDocument().getDocument().getBody().getDomNode().insertBefore(bookmarkEndNodeToMove, insertBeforeNode);
		}
	}

	/**
	 * Take style and numbering from given source paragraph and apply it to given target paragraph.
	 */
	private void copyParagraphStyleAndNumbering(XWPFParagraph fromPara, XWPFParagraph toPara) {

		XWPFDocument fromDoc = fromPara.getDocument();
		XWPFDocument toDocument = toPara.getDocument();

		String styleID = fromPara.getStyleID();
		XWPFStyle style = fromDoc.getStyles().getStyle(styleID);
		copyStyle(fromDoc, toDocument, style);

		if (styleID != null) {
			toPara.setStyle(styleID);
		}

		CTP fromCTP = fromPara.getCTP();
		CTParaRPr pRpr = fromCTP.getPPr().getRPr() == null ? null
				: (CTParaRPr) fromCTP.getPPr().getRPr().copy();

		// copy properties
		setParagraphRpr(toPara, pRpr);

		// copy entire paragraph
//					XmlObject pCopy = ctp.copy();
//					myParagraph.getCTP().set(pCopy);

		// <w:Ppr> kopieren
		copyParagraphProperties(fromPara, toPara);

		// copy numbering details
		BigInteger numID = fromPara.getNumID();
		if (numID != null) {
//						System.out.println("para is numbered: " + numID + ": "+ otherPara.getParagraphText());

			XWPFNumbering numbering = fromDoc.getNumbering();
			BigInteger abstractNumID = numbering.getAbstractNumID(numID);
			XWPFAbstractNum abstractNum = numbering.getAbstractNum(abstractNumID);
			
			// The entire bullet list should use the same concrete numbering id.
			// Therefore for all paragraphs with the unique numID in the source document,
			// also in the target document use a unique numID. (Only create one numID per bullet list.)
			BigInteger newNumId = numIdMap.get(numID);
			if (newNumId == null) {
				newNumId = getNumberingForListStyle(fromDoc, abstractNum, toDocument);
				numIdMap.put(numID, newNumId);
			}
			toPara.setNumID(newNumId);
//						STNumberFormat.Enum numFmt = STNumberFormat.BULLET;
			BigInteger llvl = fromPara.getCTP().getPPr().getNumPr().getIlvl().getVal();
			toPara.getCTP().getPPr().getNumPr().addNewIlvl().setVal(llvl == null ? BigInteger.valueOf(0) : llvl);
		}
	}

    
	/**
	 * Copy paragraph properties, that is <w:Ppr>. Each property (such as alignment) is set one by one.
	 * 
	 * <p>
	 * Some elements (such as <w:ind> and <w:jc>) are first removed and then added if properties are copied from source to target.
	 * The reason is that it can have bad effects if attributes are present in the target that are not overwritten by the source.
	 * For example, if {@code <w:ind w:left="709"/>} exists in the target, but is not overwritten, then the result may not be as expected,
	 * since the "natural indentation" that the source "defines" by omitting the <w:ind> may not correspond to the indentation set in target (709).
	 * To avoid such discrepancies, such elements are removed completely, in order to produce the exact same constellation given by the source.  
	 */
	private void copyParagraphProperties(XWPFParagraph sourcePara, XWPFParagraph targetPara) {
		if (sourcePara == null) {
			return;
		}
		
		CTPPr pr = sourcePara.getCTP().getPPr();
		CTPPr prTarget = targetPara.getCTP().getPPr();

		// --- <w:ind> - Indentation

		CTInd ind = pr == null ? null : pr.getInd();
		CTInd indTarget = prTarget == null ? null : prTarget.getInd();

		// Zuerst Element entfernen und dann bei Bedarf hinzufuegen
		if (indTarget != null) {
			prTarget.getDomNode().removeChild(indTarget.getDomNode());
		}

		// Vermeiden, dass Tags im Ziel erzeugt werden, die in der Quelle gar nicht vorhanden sind
		if (ind != null) {
			if (ind.isSetLeft()) {
				targetPara.setIndentationLeft(sourcePara.getIndentationLeft());
			}
			if (ind.isSetFirstLine()) {
				targetPara.setIndentationFirstLine(sourcePara.getIndentationFirstLine());
			}
			if (ind.isSetRight()) {
				targetPara.setIndentationRight(sourcePara.getIndentationRight());
			}
			if (ind.isSetHanging()) {
				targetPara.setIndentationHanging(sourcePara.getIndentationHanging());
			}
		}
		
		// --- <w:jc> - Alignment
		CTJc jc = pr == null ? null : pr.getJc();
		CTJc jcTarget = prTarget == null ? null : prTarget.getJc();
		if (jcTarget != null) {
			prTarget.getDomNode().removeChild(jcTarget.getDomNode());
		}

		if (jc != null) {
			targetPara.setAlignment(sourcePara.getAlignment());
		}
		
		// ---w:pBdr/> - Border
		CTPBdr bdr = pr == null ? null : pr.getPBdr();
		CTPBdr bdrTarget = prTarget == null ? null : prTarget.getPBdr();
		
		if (bdrTarget != null) {
			prTarget.getDomNode().removeChild(bdrTarget.getDomNode());
		}

		if (bdr != null) {
			if (bdr.isSetBetween()) {
				targetPara.setBorderBetween(sourcePara.getBorderBetween());
			}
			if (bdr.isSetBottom()) {
				targetPara.setBorderBottom(sourcePara.getBorderBottom());
			}
			if (bdr.isSetLeft()) {
				targetPara.setBorderLeft(sourcePara.getBorderLeft());
			}
			if (bdr.isSetRight()) {
				targetPara.setBorderRight(sourcePara.getBorderRight());
			}
			if (bdr.isSetTop()) {
				targetPara.setBorderTop(sourcePara.getBorderTop());
			}
		}

		if (pr.isSetKeepNext()) {
			targetPara.setKeepNext(sourcePara.isKeepNext());
		}
		if (pr.isSetPageBreakBefore()) {
			targetPara.setPageBreak(sourcePara.isPageBreak());
		}
		if (pr.isSetTextAlignment()) {
			targetPara.setVerticalAlignment(sourcePara.getVerticalAlignment());
		}
	}

	/**
	 * 1 point are 20 twips :)
	 * 
	 * @param pixel pixel
	 * @return converted to twips
	 */
	private int pixelToTwips(int pixel) {
		return (int) (Units.pixelToPoints(pixel) * 20);
	}

	/**
	 * Get a numbering id for the given abstract numbering object. First a check is
	 * made if the abstractNum exists in the numbering scheme of the given document.
	 * 
	 * @param abstractNum this abstract numbering may come from a different document
	 *                    and will be searched and, if not found, created in the
	 *                    given document.
	 * @return a concrete numbering id that can be used for a particular bullet list
	 */
	private BigInteger getNumberingForListStyle(XWPFDocument fromDoc, XWPFAbstractNum abstractNum, XWPFDocument toDoc) {
		BigInteger abstractNumId = getOrCreateListStyle(fromDoc, abstractNum, toDoc);
    	return toDoc.getNumbering().addNum(abstractNumId);

	}
	
	/**
	 * Search for an abstract numbering scheme in the list of the given document's
	 * numbering schemes.
	 * 
	 * <p>
	 * The abstract numbering is the "template", which allows concrete numbering ids
	 * that are being used in {@code <w:numPr><w:numId w:val="41"/></numPr>}
	 * elements. The {@code w:val="41"} is the concrete numbering, which is
	 * connected to the abstract numbering id.
	 * 
	 * <p>
	 * For example {@code <w:num w:numId="41"><w:abstractNumId w:val="11"/></w:num>}
	 * in numbering.xml (after unzipping the docx file) connects the numbering id 41
	 * to the abstract numbering id 11.
	 * 
	 * <p>
	 * The abstract numbering is also defined in numbering.xml in the form of
	 * {@code <w:abstractNum w:abstractNumId="11" ...}.
	 * 
	 * @param fromDoc         document in which to search for
	 * @param abstractNum the abstract numbering to search for
	 * @return the abstract numbering id that can be used to create a new "concrete"
	 *         numbering id.
	 */
	// from https://stackoverflow.com/a/24217336/606662
	private BigInteger getOrCreateListStyle(XWPFDocument fromDoc, XWPFAbstractNum abstractNum, XWPFDocument toDoc) {
//		String style = "<xml-fragment xmlns:wpc=\"http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas\" xmlns:mc=\"http://schemas.openxmlformats.org/markup-compatibility/2006\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\" xmlns:m=\"http://schemas.openxmlformats.org/officeDocument/2006/math\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:wp14=\"http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing\" xmlns:wp=\"http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing\" xmlns:w10=\"urn:schemas-microsoft-com:office:word\" xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" xmlns:w14=\"http://schemas.microsoft.com/office/word/2010/wordml\" xmlns:wpg=\"http://schemas.microsoft.com/office/word/2010/wordprocessingGroup\" xmlns:wpi=\"http://schemas.microsoft.com/office/word/2010/wordprocessingInk\" xmlns:wne=\"http://schemas.microsoft.com/office/word/2006/wordml\" xmlns:wps=\"http://schemas.microsoft.com/office/word/2010/wordprocessingShape\" mc:Ignorable=\"w14 wp14\"><w:nsid w:val=\"1656060D\" /><w:multiLevelType w:val=\"hybridMultilevel\" /><w:tmpl w:val=\"99FCFC1A\" /><w:lvl w:ilvl=\"0\" w:tplc=\"0409000F\"><w:start w:val=\"1\" /><w:numFmt w:val=\"decimal\" /><w:lvlText w:val=\"%1.\" /><w:lvlJc w:val=\"left\" /><w:pPr><w:ind w:left=\"720\" w:hanging=\"360\" /></w:pPr></w:lvl><w:lvl w:ilvl=\"1\" w:tplc=\"04090019\" w:tentative=\"1\"><w:start w:val=\"1\" /><w:numFmt w:val=\"lowerLetter\" /><w:lvlText w:val=\"%2.\" /><w:lvlJc w:val=\"left\" /><w:pPr><w:ind w:left=\"1440\" w:hanging=\"360\" /></w:pPr></w:lvl><w:lvl w:ilvl=\"2\" w:tplc=\"0409001B\" w:tentative=\"1\"><w:start w:val=\"1\" /><w:numFmt w:val=\"lowerRoman\" /><w:lvlText w:val=\"%3.\" /><w:lvlJc w:val=\"right\" /><w:pPr><w:ind w:left=\"2160\" w:hanging=\"180\" /></w:pPr></w:lvl><w:lvl w:ilvl=\"3\" w:tplc=\"0409000F\" w:tentative=\"1\"><w:start w:val=\"1\" /><w:numFmt w:val=\"decimal\" /><w:lvlText w:val=\"%4.\" /><w:lvlJc w:val=\"left\" /><w:pPr><w:ind w:left=\"2880\" w:hanging=\"360\" /></w:pPr></w:lvl><w:lvl w:ilvl=\"4\" w:tplc=\"04090019\" w:tentative=\"1\"><w:start w:val=\"1\" /><w:numFmt w:val=\"lowerLetter\" /><w:lvlText w:val=\"%5.\" /><w:lvlJc w:val=\"left\" /><w:pPr><w:ind w:left=\"3600\" w:hanging=\"360\" /></w:pPr></w:lvl><w:lvl w:ilvl=\"5\" w:tplc=\"0409001B\" w:tentative=\"1\"><w:start w:val=\"1\" /><w:numFmt w:val=\"lowerRoman\" /><w:lvlText w:val=\"%6.\" /><w:lvlJc w:val=\"right\" /><w:pPr><w:ind w:left=\"4320\" w:hanging=\"180\" /></w:pPr></w:lvl><w:lvl w:ilvl=\"6\" w:tplc=\"0409000F\" w:tentative=\"1\"><w:start w:val=\"1\" /><w:numFmt w:val=\"decimal\" /><w:lvlText w:val=\"%7.\" /><w:lvlJc w:val=\"left\" /><w:pPr><w:ind w:left=\"5040\" w:hanging=\"360\" /></w:pPr></w:lvl><w:lvl w:ilvl=\"7\" w:tplc=\"04090019\" w:tentative=\"1\"><w:start w:val=\"1\" /><w:numFmt w:val=\"lowerLetter\" /><w:lvlText w:val=\"%8.\" /><w:lvlJc w:val=\"left\" /><w:pPr><w:ind w:left=\"5760\" w:hanging=\"360\" /></w:pPr></w:lvl><w:lvl w:ilvl=\"8\" w:tplc=\"0409001B\" w:tentative=\"1\"><w:start w:val=\"1\" /><w:numFmt w:val=\"lowerRoman\" /><w:lvlText w:val=\"%9.\" /><w:lvlJc w:val=\"right\" /><w:pPr><w:ind w:left=\"6480\" w:hanging=\"180\" /></w:pPr></w:lvl></xml-fragment>";
		try {
			XWPFNumbering numbering = toDoc.getNumbering();
			if (numbering == null) {
				numbering = toDoc.createNumbering();
			}

			// comparing in numbering. doesn't work, returns wrong abstractNum
			// because not yet implemented, see XmlComplexContentImpl line 82:
			//  "BUGBUG: by-value structure comparison undone"
			// Therefore own implementation of getIdOfAbstractNum()
	        BigInteger idOfAbstractNum = getIdOfAbstractNum(numbering, abstractNum);
	        if (idOfAbstractNum != null) {
	        	return idOfAbstractNum;
	        }

//	        // generate numbering style from XML
//	        CTAbstractNum abstractNum = CTAbstractNum.Factory.parse(style);
//	        XWPFAbstractNum abstractNum = new XWPFAbstractNum(abstractNum, numbering);

			// Find available id in document, that is, an abstractNumId that is currently not in use
			BigInteger abstractNumId = BigInteger.valueOf(0);
			boolean found = true;
			while (found) {
				Object o = numbering.getAbstractNum(abstractNumId);
				found = (o != null);
				if (found)
					abstractNumId = abstractNumId.add(BigInteger.ONE);
			}
			// assign id
			abstractNum.getAbstractNum().setAbstractNumId(abstractNumId);
			
			// deal with pics that are used as bullets
			copyBulletPics(fromDoc, abstractNum);
			
			// add to numbering, should get back same id
			abstractNumId = numbering.addAbstractNum(abstractNum);
			return abstractNumId;
			// add to num list, result is numid
//			return doc.getNumbering().addNum(id);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private void copyBulletPics(XWPFDocument fromDoc, XWPFAbstractNum abstractNum) {
		for (CTLvl lvl : abstractNum.getAbstractNum().getLvlList()) {
			CTDecimalNumber picBulletId = lvl.getLvlPicBulletId();
			if (picBulletId != null) {
				BigInteger val = picBulletId.getVal();
				log.warn("Bookmark {}: Pictures are not supported as bullet points. Removing picBulletId '{}' from AbstractNum element", _bookmarkName, val);
				lvl.getDomNode().removeChild(picBulletId.getDomNode());
			}
		}
	}
	
	/**
	 * Search for an abstractNum in the given numbering scheme.
	 * 
	 * <p>
	 * The difficulty is the deep comparison of the entire xml objects. See: XBeanCompare 
	 * 
	 * @return null if nothing found.
	 */
	private BigInteger getIdOfAbstractNum(XWPFNumbering numbering, XWPFAbstractNum abstractNum) {
		CTAbstractNum copy = (CTAbstractNum) abstractNum.getCTAbstractNum().copy();
		XWPFAbstractNum newAbstractNum = new XWPFAbstractNum(copy, numbering);
  
		BigInteger id = BigInteger.valueOf(0);
		while (true) {
			newAbstractNum.getCTAbstractNum().setAbstractNumId(id);
			XWPFAbstractNum o = numbering.getAbstractNum(id);
			if (o == null) {
				return null;
			}

	        Diagnostic diag = new Diagnostic();
	        if (XBeanCompare.lenientlyCompareTwoXmlStrings(newAbstractNum.getCTAbstractNum(), o.getCTAbstractNum(), diag)) {
	        	return o.getCTAbstractNum().getAbstractNumId();
	        }
	        
	        id = id.add(BigInteger.ONE);
		}
	}

	// Copy Styles of Table and Paragraph.
	private static void copyStyle(XWPFDocument srcDoc, XWPFDocument destDoc, XWPFStyle style) {
		if (destDoc == null || style == null)
			return;

		if (destDoc.getStyles() == null) {
			destDoc.createStyles();
		}

		List<XWPFStyle> usedStyleList = srcDoc.getStyles().getUsedStyleList(style);
		for (XWPFStyle xwpfStyle : usedStyleList) {
			destDoc.getStyles().addStyle(xwpfStyle);
		}
	}

	/**
	 * First, clear all nodes following the bookmark start node in the current paragraph.
	 * Then, clear all paragraphs until the bookmark end node is found.
	 * @param firstParagraph
	 */
	private void clearMyBookmarContent(XWPFParagraph firstParagraph) {

		// step one: clear runs in paragraph that contains bookmarkStart
		if (deleteSiblingNodesAfterBookmarkStart()) {
			// if the bookmark end tag was found, then do not carry on and delete further paragraphs
			return;
		}

		// step two: remove paragraphs and tables that do neither contain bookmarkStart
		// nor bookmarkEnd
		List<XWPFParagraph> paragraphsToRemove = new ArrayList<>();
		List<XWPFTable> tablesToRemove = new ArrayList<>();

		XmlCursor cursor = firstParagraph.getCTP().newCursor();
		XWPFDocument document = firstParagraph.getDocument();
		while (cursor.toNextSibling()) {
			XmlObject myParaCursorNode = cursor.getObject();
			if (myParaCursorNode instanceof CTMarkupRange && this._bookmarkId.equals(BookmarkTagUtil.getWId(myParaCursorNode))) {
				// we found the bookmarkEnd as a sibling of a paragraph
				break;
			}
			else if (myParaCursorNode instanceof CTP) {
				CTP ctp = (CTP) myParaCursorNode;
				CTMarkup ctBookmarkEnd = BookmarkTagUtil.findBookmarkEndUsingPath(ctp, this._bookmarkId);
				if (ctBookmarkEnd != null) {
					// delete all runs before bookmarkEnd
					deleteSiblingsBeforeBookmarkEnd(ctBookmarkEnd);

					// move bookmarkEnd to the end of the first paragraph
					firstParagraph.getCTP().getDomNode().appendChild(ctBookmarkEnd.getDomNode());

					// Falls nach Verschieben des BookmarkEnd-Elements ein leerer Paragraph zur�ckbleibt, diesen entfernen
					deleteIfEmpty(document, ctp);

					break;
				} else {
//					XWPFParagraph paragraphToRemove = document.getParagraph(ctp);
					XWPFParagraph paragraphToRemove = ParagraphFinder.findParagraph(document, ctp);
//					int posOfParagraph = document.getPosOfParagraph(paragraphToRemove);
//					document.removeBodyElement(posOfParagraph);
					// do not remove now, so that cursor doesn't get confused
					paragraphsToRemove.add(paragraphToRemove);
				}
			}
			// Table
			else if (myParaCursorNode instanceof CTTbl) {
				CTTbl ctTbl = (CTTbl) myParaCursorNode;
				XWPFTable table = document.getTable(ctTbl);
				tablesToRemove.add(table);
			}
		}
		cursor.dispose();

		for (XWPFParagraph paragraphToRemove : paragraphsToRemove) {
			int posOfParagraph = document.getPosOfParagraph(paragraphToRemove);
			document.removeBodyElement(posOfParagraph);
		}

		for (XWPFTable tableToRemove : tablesToRemove) {
			int posOfTable = document.getPosOfTable(tableToRemove);
			document.removeBodyElement(posOfTable);
		}
	}

	private void deleteIfEmpty(XWPFDocument document, CTP ctp) {
		if (ctp.getRList().isEmpty() && ctp.getBookmarkStartList().isEmpty() && ctp.getBookmarkEndList().isEmpty()) {
//			ctp.getDomNode().getParentNode().removeChild(ctp.getDomNode());
			XWPFParagraph paragraph = document.getParagraph(ctp);
			int posOfParagraph = document.getPosOfParagraph(paragraph);
			document.removeBodyElement(posOfParagraph);
		}
	}

	/**
	 * Copy every run, including its run properties
	 */
	private void copyRunsInto(List<XWPFRun> runs, XWPFParagraph paragraph) {
		for (XWPFRun srcRun : runs) {

			XWPFRun destRun = paragraph.createRun();
			
			// deal with images
			boolean hasImage = false;
			for (XWPFPicture sourcePic : srcRun.getEmbeddedPictures()) {
				hasImage = true;

				XWPFPictureData pictureData = sourcePic.getPictureData();
				byte[] img = pictureData.getData();

				// zu den Werten in <wp:extent cx="2153345" cy="1472897"/> (Kind von <wp:inline>) kommt man offenbar nicht, weil kein Zugang zum Drawing Element 
				long cx = sourcePic.getCTPicture().getSpPr().getXfrm().getExt().getCx();
				long cy = sourcePic.getCTPicture().getSpPr().getXfrm().getExt().getCy();

//                    String blipId = dstPr.getDocument().addPictureData(new ByteArrayInputStream(img),
//                            Document.PICTURE_TYPE_PNG);
//                    destDoc.createPictureCxCy(blipId, destDoc.getNextPicNameNumber(Document.PICTURE_TYPE_PNG),
//                            cx, cy);
				ByteArrayInputStream is = new ByteArrayInputStream(img);
				try {
					XWPFPicture destPic = destRun.addPicture(is, pictureData.getPictureType(), pictureData.getFileName(), (int) cx, (int) cy);
					copyImageProperties(sourcePic, destPic);

					//lock aspect ratio
					destRun.getCTR().getDrawingArray(0).getInlineArray(0).addNewCNvGraphicFramePr().addNewGraphicFrameLocks().setNoChangeAspect(true);

				} catch (InvalidFormatException | IOException e) {
					log.warn("Bookmark {}: Error adding image. Skipping", _bookmarkName, e);
				}
			}

			if (hasImage) {
				// copy <w:rPr> run properties
				destRun.getCTR().setRPr(srcRun.getCTR().getRPr());
			} else {
				// Wenn keine Bilder vorhanden sind, einfach den gesamten Paragraph kopieren
				XmlObject rCopy = srcRun.getCTR().copy();
				destRun.getCTR().set(rCopy);
			}
		}
	}
	
	private void copyImageProperties(XWPFPicture sourcePic, XWPFPicture destPic) {
		
		CTNonVisualDrawingProps nvPrSource = sourcePic.getCTPicture().getNvPicPr().getCNvPr();
		CTNonVisualDrawingProps nvPrDest = destPic.getCTPicture().getNvPicPr().getCNvPr();
		if (nvPrSource.isSetDescr()) {
			nvPrDest.setDescr(nvPrSource.getDescr());
		}
		if (nvPrSource.isSetHidden()) {
			nvPrDest.setHidden(nvPrSource.getHidden());
		}
		if (nvPrSource.isSetHlinkClick()) {
			nvPrDest.setHlinkClick(nvPrSource.getHlinkClick());
		}
		if (nvPrSource.isSetHlinkHover()) {
			nvPrDest.setHlinkHover(nvPrSource.getHlinkHover());
		}

		CTNonVisualPictureProperties nvPicPrSource = sourcePic.getCTPicture().getNvPicPr().getCNvPicPr();
		CTNonVisualPictureProperties nvPicPrDest = destPic.getCTPicture().getNvPicPr().getCNvPicPr();
		if (nvPicPrSource.isSetPicLocks()) {
			nvPicPrDest.setPicLocks(nvPicPrSource.getPicLocks());
		}
		if (nvPicPrSource.isSetPreferRelativeResize()) {
			nvPicPrDest.setPreferRelativeResize(nvPicPrSource.getPreferRelativeResize());
		}

		CTBlipFillProperties blipFillSource = sourcePic.getCTPicture().getBlipFill();
		CTBlipFillProperties blipFillDest = destPic.getCTPicture().getBlipFill();
		if (blipFillSource.isSetSrcRect()) {
			blipFillDest.setSrcRect(blipFillSource.getSrcRect());
		}
		if (blipFillSource.isSetStretch()) {
			blipFillDest.setStretch(blipFillSource.getStretch());
		}
		if (blipFillSource.isSetDpi()) {
			blipFillDest.setDpi(blipFillSource.getDpi());
		}
		if (blipFillSource.isSetRotWithShape()) {
			blipFillDest.setRotWithShape(blipFillSource.getRotWithShape());
		}
		if (blipFillSource.isSetTile()) {
			blipFillDest.setTile(blipFillSource.getTile());
		}

		CTShapeProperties spPrSource = sourcePic.getCTPicture().getSpPr();
		CTShapeProperties spPrDest = destPic.getCTPicture().getSpPr();
		if (spPrSource.isSetBwMode()) {
			spPrDest.setBwMode(spPrSource.getBwMode());
		}
		if (spPrSource.isSetCustGeom()) {
			spPrDest.setCustGeom(spPrSource.getCustGeom());
		}
		if (spPrSource.isSetEffectDag()) {
			spPrDest.setEffectDag(spPrSource.getEffectDag());
		}
		if (spPrSource.isSetEffectLst()) {
			spPrDest.setEffectLst(spPrSource.getEffectLst());
		}
		if (spPrSource.isSetGradFill()) {
			spPrDest.setGradFill(spPrSource.getGradFill());
		}
		if (spPrSource.isSetGrpFill()) {
			spPrDest.setGrpFill(spPrSource.getGrpFill());
		}
		if (spPrSource.isSetLn()) {
			spPrDest.setLn(spPrSource.getLn());
		}
		if (spPrSource.isSetNoFill()) {
			spPrDest.setNoFill(spPrSource.getNoFill());
		}
		if (spPrSource.isSetPattFill()) {
			spPrDest.setPattFill(spPrSource.getPattFill());
		}
		if (spPrSource.isSetPrstGeom()) {
			spPrDest.setPrstGeom(spPrSource.getPrstGeom());
		}
		if (spPrSource.isSetScene3D()) {
			spPrDest.setScene3D(spPrSource.getScene3D());
		}
		if (spPrSource.isSetSolidFill()) {
			spPrDest.setSolidFill(spPrSource.getSolidFill());
		}
		if (spPrSource.isSetSp3D()) {
			spPrDest.setSp3D(spPrSource.getSp3D());
		}
		if (spPrSource.isSetXfrm()) {
			spPrDest.setXfrm(spPrSource.getXfrm());
		}

	}

	private List<XWPFRun> copyCTRsInto(List<CTR> ctrs, XWPFParagraph myParagraph) {
		List<XWPFRun> createdRuns = new ArrayList<>();
		for (CTR ctr : ctrs) {
			XWPFRun run = myParagraph.createRun();
			XmlObject rCopy = ctr.copy();
			run.getCTR().set(rCopy);
			createdRuns.add(run);
		}
		return createdRuns;
	}

	// Problem mit dieser Methode ist, dass
	// 1. andere Elemente als CTR nicht erkannt werden und sofort abgebrochen wird,
	//    sobald anderes Element (z.B. <w:proofErr> erscheint
	// 2. keine Paragraph-Properties kopiert werden inkl. Nummerierung
	private void copyAllNodesBeforeBookmarkEnd(CTMarkup ctBookmarkEnd, XWPFParagraph toParagraph) {

//		XmlCursor otherPCursor = otherPara.getCTP().newCursor();
//		if (!otherPCursor.toFirstChild()) {
//			return;
//		}
//
		List<CTR> runs = new ArrayList<>();
//		do {
//			XmlObject object = otherPCursor.getObject();
//			// TODO object instance of CTPPr
//			if (object instanceof CTBookmark) {
//				break;
//			} else if (object instanceof CTR) {
//				runs.add((CTR) object);
//			}
//		} while (otherPCursor.toNextSibling());

		XmlCursor cursor = ctBookmarkEnd.newCursor();
		while (cursor.toPrevSibling()) {
			XmlObject o = cursor.getObject();
			if (o instanceof CTR) {
				runs.add(0, (CTR) o);
			} else {
				break;
			}
		}
		cursor.dispose();

		copyCTRsInto(runs, toParagraph);
	}
	
	private List<XWPFRun> filterRunsBeforeBookmarkEnd(XWPFParagraph fromPara, CTMarkup ctBookmarkEnd) {
		
		// find the last run before the bookmarkEnd (it could of course be none)
		CTR lastCtrBeforeBookmarkEnd = null;
		XmlCursor cursor = ctBookmarkEnd.newCursor();
		while (cursor.toPrevSibling()) {
			XmlObject o = cursor.getObject();
			if (o instanceof CTR) {
				lastCtrBeforeBookmarkEnd = (CTR) o;
				break;
			}
		}
		cursor.dispose();
		
		// if a run was found before the bookmarkEnd, add all runs up to that one to the filtered list
		List<XWPFRun> filteredRuns = new ArrayList<>();
		if (lastCtrBeforeBookmarkEnd != null) {
//			XWPFRun xwpfRun = toParagraph.getRun(lastCtrBeforeBookmarkEnd);
			
			for (XWPFRun run : fromPara.getRuns()) {
				filteredRuns.add(run);
				// bis zum letzten vor dem bookmarkEnd alle hinzufuegen
				if (run.getCTR() == lastCtrBeforeBookmarkEnd) {
					break;
				}
			}
		}

		return filteredRuns;
	}

	private void copyAllRunsFromBookmarkStart(Bookmark fromBookmark) {
		List<CTR> runs = getAllRunsFromBookmarkStart(fromBookmark);
		if (runs.isEmpty()) {
			return;
		}
		
		XWPFParagraph toParagraph = this._para;
		List<XWPFRun> createdRuns = copyCTRsInto(runs, toParagraph);

		CTMarkup toBookmarkEnd = BookmarkTagUtil.findBookmarkEndUsingPath(toParagraph.getCTP(), this.getBookmarkId());
		if (toBookmarkEnd != null) {
			// If bookmarkEnd is in the toBookmark's paragraph, then the runs were inserted
			// after the bookmarkEnd - there is no way to prevent that, apparently
			// Therefore, it is necessary to move the bookmarkEnd to after the inserted runs
			for (XWPFRun run : createdRuns) {
				// move all runs before the bookmarkEnd (no deletion necessary, deletion is done
				// automatically, see JavaDoc of insertBefore()
				this._para.getCTP().getDomNode().insertBefore(run.getCTR().getDomNode(), toBookmarkEnd.getDomNode());
			}
		}
	}
	
	private List<CTR> getAllRunsFromBookmarkStart(Bookmark fromBookmark) {
		List<CTR> runs = new ArrayList<>();

		XmlCursor cursor = fromBookmark._ctBookmark.newCursor();
		while (cursor.toNextSibling()) {
			XmlObject o = cursor.getObject();
			if (o instanceof CTR) {
				runs.add((CTR) o);
			} else if (isBookmarkEnd(o)) {
				break;
			}
		}
		cursor.dispose();
		
		return runs;
	}
	
	private boolean isBookmarkEnd(XmlObject o) {
		return this.isBookmarkEnd(o.getDomNode());
	}
	
	private boolean isBookmarkEnd(Node node) {
		if (node.getNodeName().contains(Bookmark.BOOKMARK_END_TAG)) {
			int bookmarkEndID = Integer.parseInt(
					node.getAttributes().getNamedItem(Bookmark.BOOKMARK_ID_ATTR_NAME).getNodeValue());
			if (this._bookmarkId.intValue() == bookmarkEndID) {
				return true;
			}
		}
		return false;
	}

	public void deleteBookmark() {
		OverlappingBookmarks overlappingBookmarks = BookmarkTagUtil.findOverlappingBookmarks(this);
		log.debug("Bookmark '{}': overlappingBookmarks: {}", _bookmarkName, overlappingBookmarks);

		// Die �berlappenden Bookmarks vor das zu l�schende bookmarkStart bzw. nach bookmarkEnd schieben, damit diese Bookmarks nicht zerst�rt werden
		for (XmlObject xmlObject : overlappingBookmarks.getOnFirstParagraph()) {
//			this._para.getCTP().getDomNode().insertBefore(xmlObject.getDomNode(), _ctBookmark.getDomNode());
			MoveContentUtil.moveXInFrontOfY(this._para.getCTP().getDomNode(), xmlObject.getDomNode(), _ctBookmark.getDomNode());
		}

		for (int i = overlappingBookmarks.getOnOtherParagraphs().size() - 1; i >= 0; i--) {
			XmlObject xmlObject = overlappingBookmarks.getOnOtherParagraphs().get(i);
			XmlObject ctBookmarkEnd = overlappingBookmarks.getBookmarkEnd();

			MoveContentUtil.moveXAfterY(xmlObject, ctBookmarkEnd);
		}

		this.clearMyBookmarContent(_para);

		// remove bookmark start element
		_para.getCTP().getDomNode().removeChild(this._ctBookmark.getDomNode());

		// remove bookmark end element
		BookmarkTagUtil.deleteBookmarkEnd(_para.getDocument(), _bookmarkId);

		log.debug("Bookmark '{}': deleted", _bookmarkName);
	}
}
