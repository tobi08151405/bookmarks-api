package bookmarks.copy1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.xmlbeans.XmlException;

import bookmarks.Bookmark;
import bookmarks.Bookmarks;

/**
 * Konkretes Problem im Betrieb, das hier nachgestellt wird.
 * 
 * <p>
 * Original-Request:
 * <pre>
 * {"sourceFile":"/var/www/samba/qms/Templates/STC-T-052_ORDER CONFIRMATION_A.docx",
 *  "targetFile":"/var/www/samba/doa/Customer/C001_din/C001-0001_din1/Commercial/OC-767C001-0010_A.docx",
 *  "texts":{},"images":{},"tables":{},
 *  "copy":[
 *   {"fromFile":"/var/www/samba/doa/Customer/C001_din/C001-0001_din1/Commercial/P-767C001-0009_A.docx",
 *    "contents":[
 *     {"fromBookmark":"QuotationDataInput","toBookmark":"QuotationDataInput"},
 *     {"fromBookmark":"QuotationDeliverables","toBookmark":"QuotationDeliverables"},
 *     {"fromBookmark":"QuotationSOW","toBookmark":"QuotationSOW"},
 *     {"fromBookmark":"QuotationTechAssumptions","toBookmark":"QuotationTechAssumptions"},
 *     {"fromBookmark":"QuotationTimeline","toBookmark":"QuotationTimeline"},
 *     {"fromBookmark":"QuotationHardware","toBookmark":"QuotationHardware"},
 *     {"fromBookmark":"AircraftMSN","toBookmark":"AircraftMSN"},
 *     {"fromBookmark":"AircraftReg","toBookmark":"AircraftReg"},
 *     {"fromBookmark":"AircraftType","toBookmark":"AircraftType"}
 *    ]
 *   }]
 * }
 * </pre>
 * 
 */
public class Copy1 {

	private static final String AIRCRAFT_MSN = "AircraftMSN";
	private static final String AIRCRAFT_REG = "AircraftReg";
	private static final String AIRCRAFT_TYPE = "AircraftType";
	private static final String QUOTATION_DATA_INPUT = "QuotationDataInput";
	private static final String QUOTATION_DELIVERABLES = "QuotationDeliverables";
	private static final String QUOTATION_HARDWARE = "QuotationHardware";
	private static final String QUOTATION_SOW = "QuotationSOW";
	private static final String QUOTATION_TECH_ASSUMPTIONS = "QuotationTechAssumptions";
	private static final String QUOTATION_TIMELINE = "QuotationTimeline";

	private static final String INPUTFILENAME = "STC-T-052_ORDER CONFIRMATION_A.docx";
	private static final String test_resources_path = "src/test/resources/bookmarks/copy1/";
	private static final String source = test_resources_path+"/"+INPUTFILENAME;
	private static final String from = test_resources_path+"/P-767C001-0009_A.docx";
//	private static final String target = "C:\\temp\\doc\\poi\\copyBookmarks\\STC-T-052_ORDER CONFIRMATION_A.docx";
//	private static final String target = "C:\\temp\\doc\\poi\\copyBookmarks\\STC-T-055_ADDITIONAL WORK CLAIM_A.docx";
//	private static final String target = "C:\\temp\\doc\\poi\\copyBookmarks\\Dok1.docx";
//	private static final String targetUpdated = "C:\\temp\\doc\\poi\\copyBookmarks\\STC-T-052_ORDER CONFIRMATION_A_Updated.docx";

	public static void main(String[] args) throws IOException, XmlException, InvalidFormatException {

		File file = new File(test_resources_path);
		String absolutePath = file.getAbsolutePath();
		 
		System.out.println(absolutePath);
		
		XWPFDocument fromDoc = loadDoc(from);
		XWPFDocument sourceDoc = loadDoc(source);
		
		Bookmarks fromBookmarks = new Bookmarks(fromDoc, from);
		Bookmarks sourceBookmarks = new Bookmarks(sourceDoc, source);

		copyText(fromBookmarks, AIRCRAFT_MSN, sourceBookmarks, AIRCRAFT_MSN);
		copyText(fromBookmarks, AIRCRAFT_REG, sourceBookmarks, AIRCRAFT_REG);
		copyText(fromBookmarks, AIRCRAFT_TYPE, sourceBookmarks, AIRCRAFT_TYPE);
		copyFullBookmarkContent(fromBookmarks, QUOTATION_DATA_INPUT, sourceBookmarks, QUOTATION_DATA_INPUT);
		copyFullBookmarkContent(fromBookmarks, QUOTATION_DELIVERABLES, sourceBookmarks, QUOTATION_DELIVERABLES);
		copyFullBookmarkContent(fromBookmarks, QUOTATION_SOW, sourceBookmarks, QUOTATION_SOW);
		copyFullBookmarkContent(fromBookmarks, QUOTATION_TECH_ASSUMPTIONS, sourceBookmarks, QUOTATION_TECH_ASSUMPTIONS);
		copyFullBookmarkContent(fromBookmarks, QUOTATION_HARDWARE, sourceBookmarks, QUOTATION_HARDWARE);
		copyExcelWorkbook(fromBookmarks, QUOTATION_TIMELINE, sourceBookmarks, QUOTATION_TIMELINE);

		saveDoc(test_resources_path + "updated_" + INPUTFILENAME, sourceDoc);
	}

	private static void copyFullBookmarkContent(Bookmarks fromBookmarks, String fromBookmarkName, Bookmarks toBookmarks, String toBookmarkName) throws XmlException, FileNotFoundException, IOException {

		Bookmark fromBookmark = fromBookmarks.getBookmark(fromBookmarkName);
		Bookmark toBookmark = toBookmarks.getBookmark(toBookmarkName);
		toBookmark.copyAllParagraphsFrom(fromBookmark);
	}

	private static void copyExcelWorkbook(Bookmarks fromBookmarks, String fromBookmarkName, Bookmarks toBookmarks, String toBookmarkName) throws XmlException, FileNotFoundException, IOException {
		
		Bookmark fromBookmark = fromBookmarks.getBookmark(fromBookmarkName);
		Bookmark toBookmark = toBookmarks.getBookmark(toBookmarkName);

//		targetBookmark.copyOleImageFrom(sourceBookmark);
		toBookmark.copyOleObjectFrom(fromBookmark);
	}
	
	private static void copyText(Bookmarks fromBookmarks, String fromBookmarkName, Bookmarks toBookmarks, String toBookmarkName) throws XmlException, FileNotFoundException, IOException {
		
		Bookmark sourceBookmark = fromBookmarks.getBookmark(fromBookmarkName);
		String aircraftMsnValue = sourceBookmark.getBookmarkText();

		Bookmark targetBookmark = toBookmarks.getBookmark(toBookmarkName);
		targetBookmark.insertTextAtBookmark(aircraftMsnValue, Bookmark.REPLACE);
	}

	private static void saveDoc(String filename, XWPFDocument document) throws FileNotFoundException, IOException {
		try (FileOutputStream out = new FileOutputStream(new File(filename))) {
			document.write(out);
			System.out.println(filename + " written successully");
		}
	}

	private static XWPFDocument loadDoc(String sourceFile) {
		try (FileInputStream fis = new FileInputStream(sourceFile)) {
			return new XWPFDocument(fis);
		} catch (Exception e) {
			throw new RuntimeException("File cannot be parsed: " + sourceFile, e);
		}
	}

}
