package bookmarks.copy4;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.xmlbeans.XmlException;

import bookmarks.Bookmark;
import bookmarks.Bookmarks;

/**
 * Konkretes Problem im Betrieb, das hier nachgestellt wird.
 * 
 * <p>
 * Hier geht es darum, auch solche Textmarken zu kopieren, die eigentlich ueber
 * normale "texts" gesetzt werden sollten. D.h. es sind Textmarken, die keinen
 * Absatz beinhalten, sondern bookmarkStart und bookmarkEnd befinden sich im
 * selben Absatz.
 * 
 * <p>
 * Es geht also darum, nur die Runs zu kopieren, die zwischen BookmarkStart und
 * BookmarkEnd liegen.
 * 
 * <p>
 * Z.B.: C_Company, C_Postcode, etc.
 */
public class Copy4 {

	private static final String QUOTATION_DATA_INPUT = "QuotationDataInput";
	private static final String QUOTATION_DELIVERABLES = "QuotationDeliverables";
	private static final String QUOTATION_HARDWARE = "QuotationHardware";
	private static final String QUOTATION_SOW = "QuotationSOW";
	private static final String QUOTATION_TECH_ASSUMPTIONS = "QuotationTechAssumptions";
	private static final String QUOTATION_TIMELINE = "QuotationTimeline";
	
	private static final String C_COMPANY = "C_Company";
	private static final String C_STREET = "C_Street";
	private static final String C_POSTCODE = "C_Postcode";
	private static final String C_CITY = "C_City";
	private static final String C_COUNTRY = "C_Country";
	private static final String C_FIRSTNAME = "C_FirstName";
	private static final String C_FAMILYNAME = "C_FamilyName";
	private static final String C_EMAIL = "C_email";
	private static final String C_TEL = "C_Tel";
	private static final String E_FIRSTNAME = "E_FirstName";
	private static final String E_FAMILYNAME = "E_FamilyName";
	private static final String E_EMAIL = "E_email";
	private static final String E_TEL = "E_Tel";
	private static final String PM_FIRSTNAME = "PM_FirstName";
	private static final String PM_FAMILYNAME = "PM_FamilyName";
	private static final String PM_EMAIL = "PM_email";
	private static final String PM_TEL = "PM_tel";

//	private static final String INPUTFILENAME = "STC-T-052_ORDER CONFIRMATION_A.docx";
	private static final String INPUTFILENAME = "STC-T-052_ORDER CONFIRMATION_A-ein-gesamtlogo.docx";
	private static final String test_resources_path = "src/test/resources/bookmarks/copy4/";
	private static final String source = test_resources_path + INPUTFILENAME;
	private static final String from = test_resources_path + "P-767C001-0010_A.docx";

	public static void main(String[] args) throws IOException, XmlException, InvalidFormatException {

		File file = new File(test_resources_path);
		String absolutePath = file.getAbsolutePath();

		System.out.println(absolutePath);

		XWPFDocument fromDoc = loadDoc(from);
		XWPFDocument sourceDoc = loadDoc(source);

		Bookmarks fromBookmarks = new Bookmarks(fromDoc, from);
		Bookmarks sourceBookmarks = new Bookmarks(sourceDoc, source);

		copyFullBookmarkContent(fromBookmarks, C_COMPANY, sourceBookmarks, C_COMPANY);
		copyFullBookmarkContent(fromBookmarks, C_STREET, sourceBookmarks, C_STREET);
		copyFullBookmarkContent(fromBookmarks, C_POSTCODE, sourceBookmarks, C_POSTCODE);
		copyFullBookmarkContent(fromBookmarks, C_CITY, sourceBookmarks, C_CITY);
		copyFullBookmarkContent(fromBookmarks, C_COUNTRY, sourceBookmarks, C_COUNTRY);
		copyFullBookmarkContent(fromBookmarks, C_FIRSTNAME, sourceBookmarks, C_FIRSTNAME);
		copyFullBookmarkContent(fromBookmarks, C_FAMILYNAME, sourceBookmarks, C_FAMILYNAME);
		copyFullBookmarkContent(fromBookmarks, C_EMAIL, sourceBookmarks, C_EMAIL);
		copyFullBookmarkContent(fromBookmarks, C_TEL, sourceBookmarks, C_TEL);

		copyFullBookmarkContent(fromBookmarks, E_FIRSTNAME, sourceBookmarks, PM_FIRSTNAME);
		copyFullBookmarkContent(fromBookmarks, E_FAMILYNAME, sourceBookmarks, PM_FAMILYNAME);
		copyFullBookmarkContent(fromBookmarks, E_EMAIL, sourceBookmarks, PM_EMAIL);
		copyFullBookmarkContent(fromBookmarks, E_TEL, sourceBookmarks, PM_TEL);

		copyExcelWorkbook(fromBookmarks, QUOTATION_TIMELINE, sourceBookmarks, QUOTATION_TIMELINE);
		copyFullBookmarkContent(fromBookmarks, QUOTATION_DATA_INPUT, sourceBookmarks, QUOTATION_DATA_INPUT);
		copyFullBookmarkContent(fromBookmarks, QUOTATION_DELIVERABLES, sourceBookmarks, QUOTATION_DELIVERABLES);
		copyFullBookmarkContent(fromBookmarks, QUOTATION_SOW, sourceBookmarks, QUOTATION_SOW);
		copyFullBookmarkContent(fromBookmarks, QUOTATION_TECH_ASSUMPTIONS, sourceBookmarks, QUOTATION_TECH_ASSUMPTIONS);
		copyFullBookmarkContent(fromBookmarks, QUOTATION_HARDWARE, sourceBookmarks, QUOTATION_HARDWARE);

		saveDoc(test_resources_path + "updated_" + INPUTFILENAME, sourceDoc);
	}

	private static void copyFullBookmarkContent(Bookmarks fromBookmarks, String fromBookmarkName, Bookmarks toBookmarks, String toBookmarkName) throws XmlException, FileNotFoundException, IOException {

		Bookmark fromBookmark = fromBookmarks.getBookmark(fromBookmarkName);
		Bookmark toBookmark = toBookmarks.getBookmark(toBookmarkName);
		toBookmark.copyAllParagraphsFrom(fromBookmark);
	}

	private static void copyExcelWorkbook(Bookmarks fromBookmarks, String fromBookmarkName, Bookmarks toBookmarks, String toBookmarkName) throws XmlException, FileNotFoundException, IOException {
		
		Bookmark fromBookmark = fromBookmarks.getBookmark(fromBookmarkName);
		Bookmark toBookmark = toBookmarks.getBookmark(toBookmarkName);

//		targetBookmark.copyOleImageFrom(sourceBookmark);
		toBookmark.copyOleObjectFrom(fromBookmark);
	}
	
//	private static void copyText(XWPFDocument sourceDoc, String sourceBookmarkName, XWPFDocument targetDoc, String targetBookmarkName) throws XmlException, FileNotFoundException, IOException {
//		
//		Bookmarks sourceBookmarks = new Bookmarks(sourceDoc);
//		Bookmark sourceBookmark = sourceBookmarks.getBookmark(sourceBookmarkName);
//		String aircraftMsnValue = sourceBookmark.getBookmarkText();
//		
//		Bookmarks targetBookmarks = new Bookmarks(targetDoc);
//		Bookmark targetBookmark = targetBookmarks.getBookmark(targetBookmarkName);
//		targetBookmark.insertTextAtBookmark(aircraftMsnValue, Bookmark.REPLACE);
//	}

	private static void saveDoc(String filename, XWPFDocument document) throws FileNotFoundException, IOException {
		try (FileOutputStream out = new FileOutputStream(new File(filename))) {
			document.write(out);
			System.out.println(filename + " written successully");
		}
	}

	private static XWPFDocument loadDoc(String sourceFile) {
		try (FileInputStream fis = new FileInputStream(sourceFile)) {
			return new XWPFDocument(fis);
		} catch (Exception e) {
			throw new RuntimeException("File cannot be parsed: " + sourceFile, e);
		}
	}

}
