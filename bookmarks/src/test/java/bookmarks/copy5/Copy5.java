package bookmarks.copy5;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.xmlbeans.XmlException;

import bookmarks.Bookmark;
import bookmarks.Bookmarks;

/**
 * Konkretes Problem im Betrieb, das hier nachgestellt wird: Beim Kopieren von Absaetzen tritt eine NPE auf, die mit Styles zu tun hat.
 * 
 */
public class Copy5 {

	private static final String QUOTATION_DATA_INPUT = "QuotationDataInput";
	private static final String QUOTATION_DELIVERABLES = "QuotationDeliverables";
	private static final String QUOTATION_HARDWARE = "QuotationHardware";
	private static final String QUOTATION_SOW = "QuotationSOW";
	private static final String QUOTATION_TIMELINE = "QuotationTimeline";
	
	private static final String C_COMPANY = "C_Company";
	private static final String C_STREET = "C_Street";
	private static final String C_POSTCODE = "C_Postcode";
	private static final String C_CITY = "C_City";
	private static final String C_COUNTRY = "C_Country";
	private static final String C_FIRSTNAME = "C_FirstName";
	private static final String C_FAMILYNAME = "C_FamilyName";
	private static final String C_EMAIL = "C_email";
	private static final String C_TEL = "C_Tel";
	private static final String DOC_NUMBER = "DocNumber";
	private static final String DOC_NUMBER_P = "DocNumberP";
	private static final String DOC_REVISION = "DocRevision";
	private static final String DOC_REVISION_P = "DocRevisionP";
	private static final String PROJECT = "Project";
	private static final String PROJECT_NUMBER = "ProjectNumber";
	private static final String AIRCRAFT_TYPE = "AircraftType";
	private static final String AIRCRAFT_MSN = "AircraftMSN";
	private static final String AIRCRAFT_REG = "AircraftReg";
	private static final String ENGINEERING_CERTIFICATION = "EngineeringCertification";

//	private static final String INPUTFILENAME = "STC-T-052_ORDER CONFIRMATION_A.docx";
//	private static final String INPUTFILENAME = "STC-min.docx";
//	private static final String INPUTFILENAME = "STC-ohne-Logo.docx";
	private static final String INPUTFILENAME = "STC-T-052_ORDER CONFIRMATION_A-ein-gesamtlogo.docx";
//	private static final String INPUTFILENAME = "updated_STC-ohne-Logo.docx";
	private static final String test_resources_path = "src/test/resources/bookmarks/copy5/";
	private static final String source = test_resources_path + INPUTFILENAME;
	private static final String from = test_resources_path + "P-B7567C005-0002_A.docx";
//	private static final String from = test_resources_path + "P-modified.docx"; //"P-B7567C005-0002_A.docx";
//	private static final String from = test_resources_path + "P-min.docx"; //"P-B7567C005-0002_A.docx";

	public static void main(String[] args) throws IOException, XmlException, InvalidFormatException {

		File file = new File(test_resources_path);
		String absolutePath = file.getAbsolutePath();

		System.out.println(absolutePath);


		// mehrmals hintereinander die Datei bearbeiten, jeweils mit den selben Daten bef�llen
		int counter = 0;
 		String lastSaveFile = doit(source, counter++);
		lastSaveFile = doit(lastSaveFile, counter++);
		lastSaveFile = doit(lastSaveFile, counter++);
	}

	private static String doit(String sourceFilename, int counter)
			throws XmlException, FileNotFoundException, IOException {
		XWPFDocument fromDoc = loadDoc(from);
		XWPFDocument sourceDoc = loadDoc(sourceFilename);

		Bookmarks fromBookmarks = new Bookmarks(fromDoc, from);
		Bookmarks sourceBookmarks = new Bookmarks(sourceDoc, source);

		copyFullBookmarkContent(fromBookmarks, C_COMPANY, sourceBookmarks, C_COMPANY);
		copyFullBookmarkContent(fromBookmarks, C_STREET, sourceBookmarks, C_STREET);
		copyFullBookmarkContent(fromBookmarks, C_POSTCODE, sourceBookmarks, C_POSTCODE);
		copyFullBookmarkContent(fromBookmarks, C_CITY, sourceBookmarks, C_CITY);
		copyFullBookmarkContent(fromBookmarks, C_COUNTRY, sourceBookmarks, C_COUNTRY);
		copyFullBookmarkContent(fromBookmarks, C_FIRSTNAME, sourceBookmarks, C_FIRSTNAME);
		copyFullBookmarkContent(fromBookmarks, C_FAMILYNAME, sourceBookmarks, C_FAMILYNAME);
		copyFullBookmarkContent(fromBookmarks, C_EMAIL, sourceBookmarks, C_EMAIL);
		copyFullBookmarkContent(fromBookmarks, C_TEL, sourceBookmarks, C_TEL);

		copyFullBookmarkContent(fromBookmarks, DOC_NUMBER, sourceBookmarks, DOC_NUMBER_P);
		copyFullBookmarkContent(fromBookmarks, DOC_REVISION, sourceBookmarks, DOC_REVISION_P);
		copyFullBookmarkContent(fromBookmarks, PROJECT, sourceBookmarks, PROJECT);
		copyFullBookmarkContent(fromBookmarks, PROJECT_NUMBER, sourceBookmarks, PROJECT_NUMBER);
		copyFullBookmarkContent(fromBookmarks, AIRCRAFT_TYPE, sourceBookmarks, AIRCRAFT_TYPE);
		copyFullBookmarkContent(fromBookmarks, AIRCRAFT_MSN, sourceBookmarks, AIRCRAFT_MSN);
		copyFullBookmarkContent(fromBookmarks, AIRCRAFT_REG, sourceBookmarks, AIRCRAFT_REG);
		copyFullBookmarkContent(fromBookmarks, ENGINEERING_CERTIFICATION, sourceBookmarks, ENGINEERING_CERTIFICATION);

		copyExcelWorkbook(fromBookmarks, QUOTATION_TIMELINE, sourceBookmarks, QUOTATION_TIMELINE);
		copyFullBookmarkContent(fromBookmarks, QUOTATION_DATA_INPUT, sourceBookmarks, QUOTATION_DATA_INPUT);
		copyFullBookmarkContent(fromBookmarks, QUOTATION_DELIVERABLES, sourceBookmarks, QUOTATION_DELIVERABLES);
		copyFullBookmarkContent(fromBookmarks, QUOTATION_SOW, sourceBookmarks, QUOTATION_SOW);
		copyFullBookmarkContent(fromBookmarks, QUOTATION_HARDWARE, sourceBookmarks, QUOTATION_HARDWARE);

		String saveFilename = test_resources_path + "updated_" + counter + "_" + INPUTFILENAME;
		saveDoc(saveFilename, sourceDoc);
		return saveFilename;
	}

	private static void copyFullBookmarkContent(Bookmarks fromBookmarks, String fromBookmarkName, Bookmarks toBookmarks, String toBookmarkName) throws XmlException, FileNotFoundException, IOException {

		Bookmark fromBookmark = fromBookmarks.getBookmark(fromBookmarkName);
		Bookmark toBookmark = toBookmarks.getBookmark(toBookmarkName);
		toBookmark.copyAllParagraphsFrom(fromBookmark);
	}

	private static void copyExcelWorkbook(Bookmarks fromBookmarks, String fromBookmarkName, Bookmarks toBookmarks, String toBookmarkName) throws XmlException, FileNotFoundException, IOException {
		
		Bookmark fromBookmark = fromBookmarks.getBookmark(fromBookmarkName);
		Bookmark toBookmark = toBookmarks.getBookmark(toBookmarkName);

//		targetBookmark.copyOleImageFrom(sourceBookmark);
		toBookmark.copyOleObjectFrom(fromBookmark);
	}
	
//	private static void copyText(XWPFDocument sourceDoc, String sourceBookmarkName, XWPFDocument targetDoc, String targetBookmarkName) throws XmlException, FileNotFoundException, IOException {
//		
//		Bookmarks sourceBookmarks = new Bookmarks(sourceDoc);
//		Bookmark sourceBookmark = sourceBookmarks.getBookmark(sourceBookmarkName);
//		String aircraftMsnValue = sourceBookmark.getBookmarkText();
//		
//		Bookmarks targetBookmarks = new Bookmarks(targetDoc);
//		Bookmark targetBookmark = targetBookmarks.getBookmark(targetBookmarkName);
//		targetBookmark.insertTextAtBookmark(aircraftMsnValue, Bookmark.REPLACE);
//	}

	private static void saveDoc(String filename, XWPFDocument document) throws FileNotFoundException, IOException {
		try (FileOutputStream out = new FileOutputStream(new File(filename))) {
			document.write(out);
			System.out.println(filename + " written successully");
		}
	}

	private static XWPFDocument loadDoc(String sourceFile) {
		try (FileInputStream fis = new FileInputStream(sourceFile)) {
			return new XWPFDocument(fis);
		} catch (Exception e) {
			throw new RuntimeException("File cannot be parsed: " + sourceFile, e);
		}
	}

}
