package bookmarks.table1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.xwpf.usermodel.XWPFDocument;

import bookmarks.BookmarkException;
import bookmarks.Bookmarks;
import bookmarks.api.TableData;

/**
 * Beim 2. mal setzen der Tabelle-Bookmarks erscheinen die Werte doppelt.
 */
public class Table1 {

	private static final String ENGINEERING_ITEM_TABLE = "EngineeringItemTable";

	private static final String INPUTFILENAME = "P-B767C005-0002_A.docx";
	private static final String test_resources_path = "src/test/resources/bookmarks/table1/";
	private static final String source = test_resources_path + INPUTFILENAME;
//	private static final String target = test_resources_path + "created.docx";
//	private static final String from = test_resources_path + "P-767C002-0001_A.docx";

	public static void main(String[] args) throws IOException, BookmarkException {

		File file = new File(test_resources_path);
		String absolutePath = file.getAbsolutePath();

		System.out.println(absolutePath);

		XWPFDocument doc = loadDoc(source);
		Bookmarks bookmarks = new Bookmarks(doc, source);
		
		bookmarks.getBookmark(ENGINEERING_ITEM_TABLE).insertTableAtBookmark(new TableData(new String[][] { 
            {"A", "zu\nueberschreiben", "A320\nZeile 2\r\nZeile 3", "4 weeks", "� 1.00"},
           }));

		saveDoc(test_resources_path + INPUTFILENAME + "_updated.docx", doc);
	}

	

	private static void saveDoc(String filename, XWPFDocument document) throws FileNotFoundException, IOException {
		try (FileOutputStream out = new FileOutputStream(new File(filename))) {
			document.write(out);
			System.out.println(filename + " written successully");
		}
	}

	private static XWPFDocument loadDoc(String sourceFile) {
		try (FileInputStream fis = new FileInputStream(sourceFile)) {
			return new XWPFDocument(fis);
		} catch (Exception e) {
			throw new RuntimeException("File cannot be parsed: " + sourceFile, e);
		}
	}

}
