package bookmarks.image0;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.Document;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.xmlbeans.XmlException;

import bookmarks.Bookmark;
import bookmarks.Bookmarks;

/**
 * Bilder kopieren fuehrt manchmal (wenn viele Bilder?) zu fehlerhaftem Dokument.
 */
public class Image0 {

	private static final String QUOTATION_DATA_INPUT = "QuotationDataInput";
	private static final String QUOTATION_DELIVERABLES = "QuotationDeliverables";
	private static final String QUOTATION_HARDWARE = "QuotationHardware";
	private static final String QUOTATION_SOW = "QuotationSOW";
	private static final String QUOTATION_TECH_ASSUMPTIONS = "QuotationTechAssumptions";
	private static final String QUOTATION_TIMELINE = "QuotationTimeline";

	private static final String INPUTFILENAME = "STC-T-052_ORDER CONFIRMATION_A.docx";
	private static final String test_resources_path = "src/test/resources/bookmarks/image0/";
	private static final String source = test_resources_path + INPUTFILENAME;
//	private static final String target = test_resources_path + "created.docx";
//	private static final String from = test_resources_path + "P-767C002-0001_A.docx";

	public static void main(String[] args) throws IOException, XmlException, InvalidFormatException {

		File file = new File(test_resources_path);
		String absolutePath = file.getAbsolutePath();

		System.out.println(absolutePath);

//		XWPFDocument doc = new XWPFDocument();
		XWPFDocument doc = loadDoc(source);

		Bookmarks sourceBookmarks = new Bookmarks(doc, source);
//        XWPFParagraph p = doc.createParagraph();
		
		Bookmark bookmarkHardware = sourceBookmarks.getBookmark(QUOTATION_HARDWARE);
		Bookmark bookmarkSow = sourceBookmarks.getBookmark(QUOTATION_SOW);
		

        insertImage(bookmarkHardware.getParagraph(), test_resources_path + "CoverPlate1-Custom.jpg");
        insertImage(bookmarkHardware.getParagraph(), test_resources_path + "CoverPlate2-Custom.png");
        insertImage(bookmarkHardware.getParagraph(), test_resources_path + "signature2.bmp");
        insertImage(bookmarkHardware.getParagraph(), test_resources_path + "Signature-cindy.png");
//        insertImage(bookmarkHardware.getParagraph(), test_resources_path + "signature-stamp-small.png");
        insertImage(bookmarkHardware.getParagraph(), test_resources_path + "signature-toby.png");
//        insertImage(bookmarkHardware.getParagraph(), test_resources_path + "skytec.jpg");
		
		saveDoc(test_resources_path + "created.docx", doc);
	}

	private static void insertImage(XWPFParagraph p, String imgFile) throws FileNotFoundException, IOException, InvalidFormatException {

            XWPFRun r = p.createRun();

                int format;

                if (imgFile.endsWith(".emf")) {
                    format = Document.PICTURE_TYPE_EMF;
                } else if (imgFile.endsWith(".wmf")) {
                    format = Document.PICTURE_TYPE_WMF;
                } else if (imgFile.endsWith(".pict")) {
                    format = Document.PICTURE_TYPE_PICT;
                } else if (imgFile.endsWith(".jpeg") || imgFile.endsWith(".jpg")) {
                    format = Document.PICTURE_TYPE_JPEG;
                } else if (imgFile.endsWith(".png")) {
                    format = Document.PICTURE_TYPE_PNG;
                } else if (imgFile.endsWith(".dib")) {
                    format = Document.PICTURE_TYPE_DIB;
                } else if (imgFile.endsWith(".gif")) {
                    format = Document.PICTURE_TYPE_GIF;
                } else if (imgFile.endsWith(".tiff")) {
                    format = Document.PICTURE_TYPE_TIFF;
                } else if (imgFile.endsWith(".eps")) {
                    format = Document.PICTURE_TYPE_EPS;
                } else if (imgFile.endsWith(".bmp")) {
                    format = Document.PICTURE_TYPE_BMP;
                } else if (imgFile.endsWith(".wpg")) {
                    format = Document.PICTURE_TYPE_WPG;
                } else {
                    System.err.println("Unsupported picture: " + imgFile +
                            ". Expected emf|wmf|pict|jpeg|png|dib|gif|tiff|eps|bmp|wpg");
                    return;
                }

                r.setText(imgFile);
                r.addBreak();
                try (FileInputStream is = new FileInputStream(imgFile)) {
                    r.addPicture(is, format, imgFile, Units.toEMU(200), Units.toEMU(200)); // 200x200 pixels
                }
//                r.addBreak(BreakType.PAGE);
	}
	

	private static void saveDoc(String filename, XWPFDocument document) throws FileNotFoundException, IOException {
		try (FileOutputStream out = new FileOutputStream(new File(filename))) {
			document.write(out);
			System.out.println(filename + " written successully");
		}
	}

	private static XWPFDocument loadDoc(String sourceFile) {
		try (FileInputStream fis = new FileInputStream(sourceFile)) {
			return new XWPFDocument(fis);
		} catch (Exception e) {
			throw new RuntimeException("File cannot be parsed: " + sourceFile, e);
		}
	}

}
