package bookmarks;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.xmlbeans.XmlException;
import org.junit.jupiter.api.Disabled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Diese Test-Klasse ist als normale Java Application auszufuehren, nicht als Unit Test
 * 
 * @author Markus Pscheidt
 *
 */
@Disabled
public class STCT050Test {
	
	public static final String INPUTFILENAME = "STC-T-050_DOCUMENT CHANGE NOTE_A.docx";
	private static final String test_resources_path = "src/test/resources/bookmarks/";
	private static final String source = test_resources_path+"/"+INPUTFILENAME;

	private static Logger log = LoggerFactory.getLogger(Bookmark.class);

	public static void main(String[] args) throws IOException, XmlException, BookmarkException {

//		String template = "Y:\\templates\\" + INPUTFILENAME;
		XWPFDocument document = loadDoc(source);

//		FileInputStream fis = new FileInputStream(new File(template));
//		XWPFDocument document = new XWPFDocument(fis);

		Bookmarks bookmarks = new Bookmarks(document, source);

		bookmarks.getBookmark("ChangeSummary").insertTextAtBookmark("Eine Change Summary\nmitZeilenumbruch", Bookmark.REPLACE);

		// in local dir
		File outFile = new File("updated_" + INPUTFILENAME);
		FileOutputStream out = new FileOutputStream(outFile);
		document.write(out);
		out.close();
		log.info("{} written successully", outFile.getAbsolutePath());
	}

	private static XWPFDocument loadDoc(String sourceFile) {
		try (FileInputStream fis = new FileInputStream(sourceFile)) {
			return new XWPFDocument(fis);
		} catch (Exception e) {
			throw new RuntimeException("File cannot be parsed: " + sourceFile, e);
		}
	}
}
