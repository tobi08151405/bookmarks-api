package bookmarks.copy6;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.xmlbeans.XmlException;

import bookmarks.Bookmark;
import bookmarks.Bookmarks;

/**
 * Konkretes Problem im Betrieb, das hier nachgestellt wird. Es entsteht ein "ung�ltiges" Dokument, das mit einer Warnung ge�ffnet werden kann.
 * 
 * Wie sich heraus stellte, ist das Problem im (SVG?)-Logo zu suchen, und zwar genau darin, dass das "y" mit zwei �bereinander liegenden (�berlappenden) Grafiken dargestellt wird.
 * <p>
 * Ideen zur L�sung:
 * 1. Das y oder gleich den ganzen Skytec-Schriftzug in ein einziges SVG packen (Die Grafik im Word d�rfte kein SVG mehr sein, daher f�llt diese Variante flach, au�er Skytec stellt ein SVG bereit)
 * 2. Das Logo als Grafik (statt SVG) einbinden (diese Variante funktioniert fehlerlos)
 * 
 * <p>
 * Funktionierendes STC-T-052: STC-T-052_ORDER CONFIRMATION_A-logo-png.docx
 * 
 * <p>
 * <pre>
 * {@code
 * Apply BookmarkData [sourceFile=/var/www/samba/qms/Templates/STC-T-052_ORDER CONFIRMATION_A.docx, targetFile=/var/www/samba/doa/Customer/C002_Condor/C002-0001_B767 and B757 Autoland Indicator/Commercial/OC-B7567C002-0001_A.docx, texts={OrderReference=SO12345, DeliveryTerms=Ex Works, acc. to Incoterms 2020, MaterialsCertification=EASA Form 1, DocNumber=OC-B7567C002-0001, DocRevision=A, PM_FirstName=, PM_FamilyName=, PM_email=, PM_tel=}, images={}, tables={EngineeringItemTable=TableData [data=[[1, EASA Minor Change for the design, certification and installation of a new Autoland Status Indicator and Cover Plate on the affected aircraft.
 * For details refer to section 3 above., 767-300ER & B757-300
 * 
 * QTY 29
 * (1), 08.02.2021
 * (2), � 4350.00]]], ItemTable=TableData [data=[[1, 6 , Autoland Status Indicator and Cover Plate
 * For details refer to section 3.1 above, KT-B756734A-00010, 15.02.2021
 * (3), � 350, � 2100.00], [2, 25 , Autoland Status Indicator Placard
 * Note: Includes placard only, S-B756734A-00011, 15.02.2021
 * (3), � 7.9, � 197.50]]], ItemNotesEngineering=TableData [data=[[1, Assumed all aircraft require same placard.], [2, Lead time dependent on EASA response times.]]], ItemNotesMaterial=TableData [data=[[1, Lead Time dependent on POA approval of Skytec.]]], PaymentMilestonesEngineering=TableData [data=[[5e985b0fe0be7027444ff1aa, 100 %, 5e247974a8f1d83f8c692dec, As requested by the customer.]]], PaymentMilestonesMaterial=TableData [data=[[5e985b0fe0be7027444ff1a4, 100 %, 5e247974a8f1d83f8c692dec, As requested by the customer.]]]}, copy=[CopyData [fromFile=/var/www/samba/doa/Customer/C002_Condor/C002-0001_B767 and B757 Autoland Indicator/Commercial/P-B7567C002-0001_A.docx, contents=[CopyFromTo [fromBookmark=QuotationDataInput, toBookmark=QuotationDataInput], CopyFromTo [fromBookmark=QuotationDeliverables, toBookmark=QuotationDeliverables], CopyFromTo [fromBookmark=QuotationHardware, toBookmark=QuotationHardware], CopyFromTo [fromBookmark=QuotationSOW, toBookmark=QuotationSOW], CopyFromTo [fromBookmark=C_Company, toBookmark=C_Company], CopyFromTo [fromBookmark=C_Street, toBookmark=C_Street], CopyFromTo [fromBookmark=C_Postcode, toBookmark=C_Postcode], CopyFromTo [fromBookmark=C_City, toBookmark=C_City], CopyFromTo [fromBookmark=C_Country, toBookmark=C_Country], CopyFromTo [fromBookmark=C_FirstName, toBookmark=C_FirstName], CopyFromTo [fromBookmark=C_FamilyName, toBookmark=C_FamilyName], CopyFromTo [fromBookmark=C_email, toBookmark=C_email], CopyFromTo [fromBookmark=C_Tel, toBookmark=C_Tel], CopyFromTo [fromBookmark=DocNumber, toBookmark=DocNumberP], CopyFromTo [fromBookmark=DocRevision, toBookmark=DocRevisionP], CopyFromTo [fromBookmark=Project, toBookmark=Project], CopyFromTo [fromBookmark=ProjectNumber, toBookmark=ProjectNumber], CopyFromTo [fromBookmark=AircraftType, toBookmark=AircraftType], CopyFromTo [fromBookmark=AircraftMSN, toBookmark=AircraftMSN], CopyFromTo [fromBookmark=AircraftReg, toBookmark=AircraftReg], CopyFromTo [fromBookmark=EngineeringCertification, toBookmark=EngineeringCertification]], oleObjects=[CopyFromTo [fromBookmark=QuotationTimeline, toBookmark=QuotationTimeline]]]]]
 * }
 * </pre>
 */
public class Copy6 {

	private static final String QUOTATION_DATA_INPUT = "QuotationDataInput";
	private static final String QUOTATION_DELIVERABLES = "QuotationDeliverables";
	private static final String QUOTATION_HARDWARE = "QuotationHardware";
	private static final String QUOTATION_SOW = "QuotationSOW";
	private static final String QUOTATION_TIMELINE = "QuotationTimeline";
	
	private static final String C_COMPANY = "C_Company";
	private static final String C_STREET = "C_Street";
	private static final String C_POSTCODE = "C_Postcode";
	private static final String C_CITY = "C_City";
	private static final String C_COUNTRY = "C_Country";
	private static final String C_FIRSTNAME = "C_FirstName";
	private static final String C_FAMILYNAME = "C_FamilyName";
	private static final String C_EMAIL = "C_email";
	private static final String C_TEL = "C_Tel";
	private static final String DOC_NUMBER = "DocNumber";
	private static final String DOC_NUMBER_P = "DocNumberP";
	private static final String DOC_REVISION = "DocRevision";
	private static final String DOC_REVISION_P = "DocRevisionP";
	private static final String PROJECT = "Project";
	private static final String PROJECT_NUMBER = "ProjectNumber";
	private static final String AIRCRAFT_TYPE = "AircraftType";
	private static final String AIRCRAFT_MSN = "AircraftMSN";
	private static final String AIRCRAFT_REG = "AircraftReg";
	private static final String ENGINEERING_CERTIFICATION = "EngineeringCertification";

//	private static final String INPUTFILENAME = "STC-T-052_ORDER CONFIRMATION_A.docx";
//	private static final String INPUTFILENAME = "STC-T-052_ORDER CONFIRMATION_A-logo-png.docx";
	private static final String INPUTFILENAME = "STC-T-052_ORDER CONFIRMATION_A-ein-gesamtlogo.docx";
//	private static final String INPUTFILENAME = "updated_STC-ohne-Logo.docx";
	private static final String test_resources_path = "src/test/resources/bookmarks/copy6/";
	private static final String source = test_resources_path + INPUTFILENAME;
	private static final String from = test_resources_path + "P-B7567C002-0001_A.docx";
//	private static final String from = test_resources_path + "P-B7567C002-0001_A-ohne-logo.docx";
//	private static final String from = test_resources_path + "P-modified.docx"; //"P-B7567C005-0002_A.docx";
//	private static final String from = test_resources_path + "P-min.docx"; //"P-B7567C005-0002_A.docx";

	public static void main(String[] args) throws IOException, XmlException, InvalidFormatException {

		File file = new File(test_resources_path);
		String absolutePath = file.getAbsolutePath();

		System.out.println(absolutePath);

		// mehrmals hintereinander die Datei bearbeiten, jeweils mit den selben Daten bef�llen
		int counter = 0;
 		String lastSaveFile = doit(source, counter++);
		lastSaveFile = doit(lastSaveFile, counter++);
		lastSaveFile = doit(lastSaveFile, counter++);
	}

	private static String doit(String sourceFilename, int counter)
			throws XmlException, FileNotFoundException, IOException, InvalidFormatException {
		XWPFDocument fromDoc = loadDoc(from);
		XWPFDocument sourceDoc = loadDoc(sourceFilename);

		Bookmarks fromBookmarks = new Bookmarks(fromDoc, from);
		Bookmarks sourceBookmarks = new Bookmarks(sourceDoc, source);

		copyFullBookmarkContent(fromBookmarks, C_COMPANY, sourceBookmarks, C_COMPANY);
		copyFullBookmarkContent(fromBookmarks, C_STREET, sourceBookmarks, C_STREET);
		copyFullBookmarkContent(fromBookmarks, C_POSTCODE, sourceBookmarks, C_POSTCODE);
		copyFullBookmarkContent(fromBookmarks, C_CITY, sourceBookmarks, C_CITY);
		copyFullBookmarkContent(fromBookmarks, C_COUNTRY, sourceBookmarks, C_COUNTRY);
		copyFullBookmarkContent(fromBookmarks, C_FIRSTNAME, sourceBookmarks, C_FIRSTNAME);
		copyFullBookmarkContent(fromBookmarks, C_FAMILYNAME, sourceBookmarks, C_FAMILYNAME);
		copyFullBookmarkContent(fromBookmarks, C_EMAIL, sourceBookmarks, C_EMAIL);
		copyFullBookmarkContent(fromBookmarks, C_TEL, sourceBookmarks, C_TEL);

		copyFullBookmarkContent(fromBookmarks, DOC_NUMBER, sourceBookmarks, DOC_NUMBER_P);
		copyFullBookmarkContent(fromBookmarks, DOC_REVISION, sourceBookmarks, DOC_REVISION_P);
		copyFullBookmarkContent(fromBookmarks, PROJECT, sourceBookmarks, PROJECT);
		copyFullBookmarkContent(fromBookmarks, PROJECT_NUMBER, sourceBookmarks, PROJECT_NUMBER);
		copyFullBookmarkContent(fromBookmarks, AIRCRAFT_TYPE, sourceBookmarks, AIRCRAFT_TYPE);
		copyFullBookmarkContent(fromBookmarks, AIRCRAFT_MSN, sourceBookmarks, AIRCRAFT_MSN);
		copyFullBookmarkContent(fromBookmarks, AIRCRAFT_REG, sourceBookmarks, AIRCRAFT_REG);
		copyFullBookmarkContent(fromBookmarks, ENGINEERING_CERTIFICATION, sourceBookmarks, ENGINEERING_CERTIFICATION);

		// ---
		// Quotation SOW und Hardware verursachen ung�ltiges File
		// ---
		copyExcelWorkbook(fromBookmarks, QUOTATION_TIMELINE, sourceBookmarks, QUOTATION_TIMELINE);
		copyFullBookmarkContent(fromBookmarks, QUOTATION_DATA_INPUT, sourceBookmarks, QUOTATION_DATA_INPUT);
		copyFullBookmarkContent(fromBookmarks, QUOTATION_DELIVERABLES, sourceBookmarks, QUOTATION_DELIVERABLES);
		copyFullBookmarkContent(fromBookmarks, QUOTATION_SOW, sourceBookmarks, QUOTATION_SOW);
		copyFullBookmarkContent(fromBookmarks, QUOTATION_HARDWARE, sourceBookmarks, QUOTATION_HARDWARE);
		
		// Flei�aufgabe: Bild einf�gen
		sourceBookmarks.getBookmark("SignatureCreatedBy_Signature").insertImageAtBookmark(test_resources_path + "/signature-stamp-small.png");

		String saveFilename = test_resources_path + "updated_" + counter + "_" + INPUTFILENAME;
		saveDoc(saveFilename, sourceDoc);
		return saveFilename;
	}

	private static void copyFullBookmarkContent(Bookmarks fromBookmarks, String fromBookmarkName, Bookmarks toBookmarks, String toBookmarkName) throws XmlException, FileNotFoundException, IOException {

		Bookmark fromBookmark = fromBookmarks.getBookmark(fromBookmarkName);
		Bookmark toBookmark = toBookmarks.getBookmark(toBookmarkName);
		toBookmark.copyAllParagraphsFrom(fromBookmark);
	}

	private static void copyExcelWorkbook(Bookmarks fromBookmarks, String fromBookmarkName, Bookmarks toBookmarks, String toBookmarkName) throws XmlException, FileNotFoundException, IOException {
		
		Bookmark fromBookmark = fromBookmarks.getBookmark(fromBookmarkName);
		Bookmark toBookmark = toBookmarks.getBookmark(toBookmarkName);

//		targetBookmark.copyOleImageFrom(sourceBookmark);
		toBookmark.copyOleObjectFrom(fromBookmark);
	}
	
//	private static void copyText(XWPFDocument sourceDoc, String sourceBookmarkName, XWPFDocument targetDoc, String targetBookmarkName) throws XmlException, FileNotFoundException, IOException {
//		
//		Bookmarks sourceBookmarks = new Bookmarks(sourceDoc);
//		Bookmark sourceBookmark = sourceBookmarks.getBookmark(sourceBookmarkName);
//		String aircraftMsnValue = sourceBookmark.getBookmarkText();
//		
//		Bookmarks targetBookmarks = new Bookmarks(targetDoc);
//		Bookmark targetBookmark = targetBookmarks.getBookmark(targetBookmarkName);
//		targetBookmark.insertTextAtBookmark(aircraftMsnValue, Bookmark.REPLACE);
//	}

	private static void saveDoc(String filename, XWPFDocument document) throws FileNotFoundException, IOException {
		try (FileOutputStream out = new FileOutputStream(new File(filename))) {
			document.write(out);
			System.out.println(filename + " written successully");
		}
	}

	private static XWPFDocument loadDoc(String sourceFile) {
		try (FileInputStream fis = new FileInputStream(sourceFile)) {
			return new XWPFDocument(fis);
		} catch (Exception e) {
			throw new RuntimeException("File cannot be parsed: " + sourceFile, e);
		}
	}

}
