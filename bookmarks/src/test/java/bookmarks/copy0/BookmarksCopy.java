package bookmarks.copy0;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.xmlbeans.XmlException;

import bookmarks.Bookmark;
import bookmarks.Bookmarks;

public class BookmarksCopy {

	private static final String AIRCRAFT_MSN = "AircraftMSN";
	private static final String QUOTATION_DATA_INPUT = "QuotationDataInput";
	private static final String QUOTATION_HARDWARE = "QuotationHardware";
	private static final String QUOTATION_SOW = "QuotationSOW";
	private static final String QUOTATION_TECH_ASSUMPTIONS = "QuotationTechAssumptions";
	private static final String QUOTATION_TIMELINE = "QuotationTimeline";

	private static final String test_resources_path = "src/test/resources/bookmarks/copy0/";
	private static final String template = test_resources_path + "STC-T-051_PROPOSAL_A.docx";
	private static final String templateUpdated = test_resources_path + "STC-T-051_PROPOSAL_A_Updated.docx";
//	private static final String target = test_resources_path + "STC-T-052_ORDER CONFIRMATION_A.docx";
	private static final String target = test_resources_path + "STC-T-052_ORDER CONFIRMATION_A-ohne-logo.docx";
	private static final String targetUpdated = test_resources_path + "STC-T-052_ORDER CONFIRMATION_A_Updated.docx";

	public static void main(String[] args) throws IOException, XmlException, InvalidFormatException {
		initT51();

		XWPFDocument sourceDoc = loadDoc(templateUpdated);
		XWPFDocument targetDoc = loadDoc(target);

//		copyText(templateUpdated, AIRCRAFT_MSN, target, AIRCRAFT_MSN);
		copyExcelWorkbook(sourceDoc, QUOTATION_TIMELINE, targetDoc, QUOTATION_TIMELINE);
		copyFullBookmarkContent(sourceDoc, QUOTATION_DATA_INPUT, targetDoc, QUOTATION_DATA_INPUT);
		copyFullBookmarkContent(sourceDoc, QUOTATION_SOW, targetDoc, QUOTATION_SOW);
		copyFullBookmarkContent(sourceDoc, QUOTATION_TECH_ASSUMPTIONS, targetDoc, QUOTATION_TECH_ASSUMPTIONS);
		copyFullBookmarkContent(sourceDoc, QUOTATION_HARDWARE, targetDoc, QUOTATION_HARDWARE);
		
		saveDoc(targetUpdated, targetDoc);
	}
	
	private static void copyFullBookmarkContent(XWPFDocument sourceDoc, String sourceBookmarkName, XWPFDocument targetDoc, String targetBookmarkName) throws XmlException, FileNotFoundException, IOException {

		Bookmarks sourceBookmarks = new Bookmarks(sourceDoc, templateUpdated);
		Bookmark sourceBookmark = sourceBookmarks.getBookmark(sourceBookmarkName);

		Bookmarks targetBookmarks = new Bookmarks(targetDoc, target);
		Bookmark targetBookmark = targetBookmarks.getBookmark(targetBookmarkName);

		targetBookmark.copyAllParagraphsFrom(sourceBookmark);
	}

	private static void copyExcelWorkbook(XWPFDocument sourceDoc, String sourceBookmarkName, XWPFDocument targetDoc, String targetBookmarkName) throws XmlException, FileNotFoundException, IOException {
		
		Bookmarks sourceBookmarks = new Bookmarks(sourceDoc, templateUpdated);
		Bookmark sourceBookmark = sourceBookmarks.getBookmark(sourceBookmarkName);

		Bookmarks targetBookmarks = new Bookmarks(targetDoc, target);
		Bookmark targetBookmark = targetBookmarks.getBookmark(targetBookmarkName);

//		targetBookmark.copyOleImageFrom(sourceBookmark);
		targetBookmark.copyOleObjectFrom(sourceBookmark);
	}
	
	private static void copyText(XWPFDocument sourceDoc, String sourceBookmarkName, XWPFDocument targetDoc, String targetBookmarkName) throws XmlException, FileNotFoundException, IOException {
		
		Bookmarks sourceBookmarks = new Bookmarks(sourceDoc, templateUpdated);
		Bookmark sourceBookmark = sourceBookmarks.getBookmark(sourceBookmarkName);
		String aircraftMsnValue = sourceBookmark.getBookmarkText();
		
		Bookmarks targetBookmarks = new Bookmarks(targetDoc, target);
		Bookmark targetBookmark = targetBookmarks.getBookmark(targetBookmarkName);
		targetBookmark.insertTextAtBookmark(aircraftMsnValue, Bookmark.REPLACE);
	}

	private static void initT51() throws FileNotFoundException, IOException {
		
		XWPFDocument document = loadDoc(template);
		Bookmarks bookmarks = new Bookmarks(document, template);

		Bookmark aircraftMsn = bookmarks.getBookmark(AIRCRAFT_MSN);
		aircraftMsn.insertTextAtBookmark("Referenz X", Bookmark.REPLACE);

		Bookmark deliverables = bookmarks.getBookmark("QuotationDeliverables");
		deliverables.insertBulletsAtBookmark(Arrays.asList("TA", "TB", "TC", "TD"));

		saveDoc(templateUpdated, document);
	}

	private static void saveDoc(String filename, XWPFDocument document) throws FileNotFoundException, IOException {
		try (FileOutputStream out = new FileOutputStream(new File(filename))) {
			document.write(out);
			System.out.println(filename + " written successully");
		}
	}

	private static XWPFDocument loadDoc(String sourceFile) {
		try (FileInputStream fis = new FileInputStream(sourceFile)) {
			return new XWPFDocument(fis);
		} catch (Exception e) {
			throw new RuntimeException("File cannot be parsed: " + sourceFile, e);
		}
	}

}
