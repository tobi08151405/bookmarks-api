package bookmarks.delete1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.xmlbeans.XmlException;

import bookmarks.Bookmarks;

/**
 * Bookmark l�schen, d.h. komplett entfernen, und zwar sowohl Bookmarks in Abs�tzen als auch in Zellen.
 */
public class Delete1 {

	private static final String SIGNATURE_CREATED_BY = "SignatureCreatedBy";
	private static final String SIGNATURE_CREATED_BY_SIGNATURE = "SignatureCreatedBy_Signature";
	private static final String SIGNATURE_APPROVAL_1 = "SignatureApproval1";
	private static final String SIGNATURE_APPROVAL_2 = "SignatureApproval2";
	private static final String SIGNATURE_APPROVAL_3 = "SignatureApproval3";
	private static final String SIGNATURE_APPROVAL_4 = "SignatureApproval4";
	private static final String SIGNATURE_RELEASED_BY1 = "SignatureReleasedBy1";

	// Achtung: Beim L�schen der Textmarke SIGNATURE_CREATED_BY geht Formatierung der folgenden �berschrift ("Approved by") verloren.
	// Abhilfe: ein Leerzeichen vor der �berschrift ("Approved by") einf�gen
	private static final String INPUTFILENAME = "STC-T-001_PROJECT DESCRIPTION_A.docx";
	private static final String test_resources_path = "src/test/resources/bookmarks/delete1/";
	private static final String source = test_resources_path + INPUTFILENAME;

	public static void main(String[] args) throws IOException, XmlException, InvalidFormatException {

		File file = new File(test_resources_path);
		String absolutePath = file.getAbsolutePath();

		System.out.println(absolutePath);

		XWPFDocument sourceDoc = loadDoc(source);

		Bookmarks sourceBookmarks = new Bookmarks(sourceDoc, source);

		// Folgendes funktioniert nicht, da unbekanntes Bookmark
		sourceBookmarks.deleteBookmark("bla bla bla");

		// Folgendes funktioniert nicht, da Zellen-Bookmark
		sourceBookmarks.deleteBookmark(SIGNATURE_CREATED_BY_SIGNATURE);

		sourceBookmarks.deleteBookmark(SIGNATURE_APPROVAL_1);
		sourceBookmarks.deleteBookmark(SIGNATURE_APPROVAL_3);
		sourceBookmarks.deleteBookmark(SIGNATURE_CREATED_BY);
		sourceBookmarks.deleteBookmark(SIGNATURE_APPROVAL_2);
		sourceBookmarks.deleteBookmark("aouter");
		sourceBookmarks.deleteBookmark(SIGNATURE_APPROVAL_4);
		sourceBookmarks.deleteBookmark(SIGNATURE_RELEASED_BY1);

		saveDoc(test_resources_path + "updated_" + INPUTFILENAME, sourceDoc);
	}

	private static void saveDoc(String filename, XWPFDocument document) throws FileNotFoundException, IOException {
		try (FileOutputStream out = new FileOutputStream(new File(filename))) {
			document.write(out);
			System.out.println(filename + " written successully");
		}
	}

	private static XWPFDocument loadDoc(String sourceFile) {
		try (FileInputStream fis = new FileInputStream(sourceFile)) {
			return new XWPFDocument(fis);
		} catch (Exception e) {
			throw new RuntimeException("File cannot be parsed: " + sourceFile, e);
		}
	}

}
