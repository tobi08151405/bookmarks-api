package bookmarks;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.junit.jupiter.api.Disabled;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBookmark;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import bookmarks.api.TableData;

/**
 * Diese Test-Klasse ist als normale Java Application auszufuehren, nicht als Unit Test
 * 
 * @author Markus Pscheidt
 *
 */
@Disabled
public class STCT055Test {
	
	public static final String INPUTFILENAME = "STC-T-055_ADDITIONAL WORK CLAIM_A.docx";

	private static Logger log = LoggerFactory.getLogger(Bookmark.class);

	public static void main(String[] args) throws IOException, XmlException, BookmarkException {
		
		String template = "Y:\\templates\\" + INPUTFILENAME;

		FileInputStream fis = new FileInputStream(new File(template));
		XWPFDocument document = new XWPFDocument(fis);

		Bookmarks bookmarks = new Bookmarks(document, template);
		
		bookmarks.getBookmark("DeliveryTerms").insertTextAtBookmark("Ein Deliv. Term", Bookmark.REPLACE);

		bookmarks.getBookmark("EngineeringItemTable").insertTableAtBookmark(new TableData(new String[][] { 
            {"1", "desc1", "A320", "4 weeks", "� 1.00"},
            {"2", "desc2", "A322", "2 weeks", "� 2.00"},
           }));

		bookmarks.getBookmark("ItemNotesEngineering").insertTableAtBookmark(new TableData(new String[][] { 
            {"Sp 11", "Spalte 12"},
            {"Sp 21", "Sp 22"},
           }));

		bookmarks.getBookmark("ItemTable").insertTableAtBookmark(new TableData(new String[][] { 
            {"1", "1ea", "desc1", "Pn1", "4 weeks", "� 1.00", "� 1.10"},
            {"2", "2ea", "desc2", "Pn2", "2 weeks", "� 2.00", "� 2.20"},
           }));

		bookmarks.getBookmark("ItemNotesMaterial").insertTableAtBookmark(new TableData(new String[][] { 
            {"M 11", "Item Notes material 12"},
            {"M 21", "M 22"},
           }));
		
		bookmarks.getBookmark("PaymentMilestonesEngineering").insertTableAtBookmark(new TableData(new String[][] { 
            {"M1", "25 %", "Terms A", "Remarks 1"},
            {"M2", "55 %", "Terms B", "Remarks 2"},
           }));

		bookmarks.getBookmark("PaymentMilestonesMaterial").insertTableAtBookmark(new TableData(new String[][] { 
            {"M7", "28 %", "Terms A", "Rem 1"},
            {"M8", "14 %", "Terms B", "Rem 2"},
           }));
		
		// Test Excel spreadsheet
		if (!bookmarks.getBookmark("QuotationTimeline").existsOleObject()) {
			throw new RuntimeException("Kein Excel Objekt gefunden");
		}
		
		// in local dir
		File outFile = new File("updated_" + INPUTFILENAME);
		FileOutputStream out = new FileOutputStream(outFile);
		document.write(out);
		out.close();
		log.info("{} written successully", outFile.getAbsolutePath());
	}

	private static void setTextAt(XWPFTable table, int rowNum, int colNum, String text) {
		XWPFTableRow row = table.getRow(rowNum);
		if (row == null) {
			row = table.createRow();
		}
		row.getTableCells().get(colNum).setText(text);
	}
	
	
	private static void procParaList(XWPFDocument document, List<XWPFParagraph> paraList, String bookmarkName, String bookmarkValue) {
	    Iterator<XWPFParagraph> paraIter = null;
	    List<CTBookmark> bookmarkList = null;
	    Iterator<CTBookmark> bookmarkIter = null;
	    CTBookmark bookmark = null;
//	    XWPFRun run = null;

        XmlCursor cursor = null;
        CTP theCtp = null;

	    paraIter = paraList.iterator();
	    while (paraIter.hasNext()) {
	    	XWPFParagraph para = paraIter.next();
	        CTP ctp = para.getCTP();
			bookmarkList = ctp.getBookmarkStartList();
	        System.out.println(bookmarkList.size() + " bookmarks");
	        bookmarkIter = bookmarkList.iterator();

	        while (bookmarkIter.hasNext()) {
	            bookmark = bookmarkIter.next();
	            System.out.println(bookmark.getName());
	            if (bookmark.getName().equals(bookmarkName)) {
	            	
	            	cursor = ctp.newCursor();
	            	theCtp = ctp;
	            	
//	            	XWPFRun run = para.createRun();
//	            	run.getCTR().newCursor();
//	                run.setText(bookmarkValue);
//	            	CTTbl inserted = document.getDocument().getBody().insertNewTbl(5);
//	            	XWPFTable table = new XWPFTable(inserted, document);
//	            	XWPFParagraph paragraph = document.createParagraph();
//	            	XWPFTable table = run.in
//	            	XWPFTable table = document.createTable(2, 2);
//	        		XWPFTableRow tableRowOne = table.getRow(0);
//	        		tableRowOne.getCell(0).setText("col one, row one");
//	        		tableRowOne.getCell(1).setText("col two, row one");

//	        		XWPFTableRow tableRowTwo = table.getRow(1);
//	        		tableRowTwo.getCell(0).setText("col one, row two");
//	        		tableRowTwo.getCell(1).setText("col two, row two");
	            	
//	            	document.getDocument().getBody().addNewBookmarkEnd();
	                
	            	XmlObject o = cursor.getObject();
	            	cursor.toFirstChild();
	            	o = cursor.getObject();
	                while (!(o instanceof CTBookmark) && (cursor.toNextSibling())) {
	                    o = cursor.getObject();
	                }
	                
//	                String uri = CTTbl.type.getName().getNamespaceURI();
//	                String localPart = "tbl";
//	                cursor.beginElement(localPart, uri);
//	                cursor.toParent();
//	                CTTbl t = (CTTbl) cursor.getObject();


	            	
	        	    XWPFTable table = insertTableAtCursor(document, cursor);

//	                Node nextNode = bookmark.getDomNode().getNextSibling();
//	                while (!(nextNode.getNodeName().contains("bookmarkEnd"))) {
//	                    ctp.getDomNode().removeChild(nextNode);
//	                    nextNode = bookmark.getDomNode().getNextSibling();
//	                }
//	                ctp.getDomNode().insertBefore(table.getCTTbl().getDomNode(), nextNode);
	            }
	        }
	        
//	        for (XWPFRun run : para.getRuns()) {
//				run.get
//			}
	    }

//	    insertTableAtCursor(document, cursor);
        
	}

	private static XWPFTable insertTableAtCursor(XWPFDocument document, XmlCursor cursor) {
		if (cursor != null) {
        	XWPFTable table = document.insertNewTbl(cursor);
        	XWPFTableRow tableRowOne = table.getRow(0);
    		tableRowOne.getCell(0).setText("col one, row one");
    		cursor = null;
    		return table;
        }
		return null;
	}
	

//	private void replaceBookmark(XWPFRun run) {
//		Node nextNode = null;
//		Node styleNode = null;
//		Node lastRunNode = null;
//		Node toDelete = null;
//		NodeList childNodes = null;
//		Stack<Node> nodeStack = null;
//		boolean textNodeFound = false;
//		boolean foundNested = true;
//		int bookmarkStartID = 0;
//		int bookmarkEndID = -1;
//		int numChildNodes = 0;
//
//		nodeStack = new Stack<Node>();
//		bookmarkStartID = this._ctBookmark.getId().intValue();
//		nextNode = this._ctBookmark.getDomNode();
//		nodeStack.push(nextNode);
//
//		// Loop through the nodes looking for a matching bookmarkEnd tag
//		while (bookmarkStartID != bookmarkEndID) {
//			nextNode = nextNode.getNextSibling();
//			nodeStack.push(nextNode);
//
//			// If an end tag is found, does it match the start tag? If so, end
//			// the while loop.
//			if (nextNode.getNodeName().contains(Bookmark.BOOKMARK_END_TAG)) {
//				try {
//					bookmarkEndID = Integer.parseInt(
//							nextNode.getAttributes().getNamedItem(Bookmark.BOOKMARK_ID_ATTR_NAME).getNodeValue());
//				} catch (NumberFormatException nfe) {
//					bookmarkEndID = bookmarkStartID;
//				}
//			}
//			// else {
//			// Place a reference to the node on the nodeStack
//			// nodeStack.push(nextNode);
//			// }
//		}
//
//		// If the stack of nodes found between the bookmark tags is not empty
//		// then they have to be removed.
//		if (!nodeStack.isEmpty()) {
//
//			// Check the node at the top of the stack. If it is a run, get it's
//			// style - if any - and apply to the run that will be replacing it.
//			// lastRunNode = nodeStack.pop();
//			lastRunNode = nodeStack.peek();
//			if ((lastRunNode.getNodeName().equals(Bookmark.RUN_NODE_NAME))) {
//				styleNode = this.getStyleNode(lastRunNode);
//				if (styleNode != null) {
//					run.getCTR().getDomNode().insertBefore(styleNode.cloneNode(true),
//							run.getCTR().getDomNode().getFirstChild());
//				}
//			}
//		}
//	}
}
