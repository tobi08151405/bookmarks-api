package bookmarks.copy3;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.xmlbeans.XmlException;

import bookmarks.Bookmark;
import bookmarks.Bookmarks;

/**
 * Konkretes Problem im Betrieb, das hier nachgestellt wird.
 * 
 * <p>
 * Das Problem, das hier nachgestellt wird, ist Kopieren von Textmarken aus
 * P-767C002-0001_A.docx in STC-T-052_ORDER CONFIRMATION_A.docx. Dabei wird
 * alles nach Kapitel 3.4 abgeschnitten, d.h. Kapitel 3.5 und folgende sind
 * nicht mehr im geschriebenen Dokument.
 * 
 * <p>
 * Die Ursache des Problems liegt darin, dass Bookmark-Start und Bookmark-End innerhalb des selben
 * Paragraphen liegen. Dadurch wird alles, was eingefügt wird, erst nach dem Bookmark eingefügt.
 * 
 * <p>
 * <pre>
 * {@code
 * <w:p>...
 *  <w:bookmarkStart w:id="39" w:name="QuotationDataInput"/>
 *   <w:r>...</w:r>
 *  <w:bookmarkEnd w:id="39"/>
 * </w:p>}
 * </pre>
 * 
 * <p>
 * Original-Request derzeit nicht verfuegbar.
 */
public class Copy3 {

	private static final String QUOTATION_DATA_INPUT = "QuotationDataInput";
	private static final String QUOTATION_DELIVERABLES = "QuotationDeliverables";
	private static final String QUOTATION_HARDWARE = "QuotationHardware";
	private static final String QUOTATION_SOW = "QuotationSOW";
	private static final String QUOTATION_TECH_ASSUMPTIONS = "QuotationTechAssumptions";
	private static final String QUOTATION_TIMELINE = "QuotationTimeline";

//	private static final String INPUTFILENAME = "STC-T-052_ORDER CONFIRMATION_A.docx";
	private static final String INPUTFILENAME = "STC-T-052_ORDER CONFIRMATION_A-ein-gesamtlogo.docx";
	private static final String test_resources_path = "src/test/resources/bookmarks/copy3/";
	private static final String source = test_resources_path + INPUTFILENAME;
	private static final String from = test_resources_path + "P-767C002-0001_A.docx";

	public static void main(String[] args) throws IOException, XmlException, InvalidFormatException {

		File file = new File(test_resources_path);
		String absolutePath = file.getAbsolutePath();

		System.out.println(absolutePath);

		XWPFDocument fromDoc = loadDoc(from);
		XWPFDocument sourceDoc = loadDoc(source);

		Bookmarks fromBookmarks = new Bookmarks(fromDoc, from);
		Bookmarks sourceBookmarks = new Bookmarks(sourceDoc, source);
		
		copyExcelWorkbook(fromBookmarks, QUOTATION_TIMELINE, sourceBookmarks, QUOTATION_TIMELINE);
		copyFullBookmarkContent(fromBookmarks, QUOTATION_DATA_INPUT, sourceBookmarks, QUOTATION_DATA_INPUT);
		copyFullBookmarkContent(fromBookmarks, QUOTATION_DELIVERABLES, sourceBookmarks, QUOTATION_DELIVERABLES);
		copyFullBookmarkContent(fromBookmarks, QUOTATION_SOW, sourceBookmarks, QUOTATION_SOW);
		copyFullBookmarkContent(fromBookmarks, QUOTATION_TECH_ASSUMPTIONS, sourceBookmarks, QUOTATION_TECH_ASSUMPTIONS);
		copyFullBookmarkContent(fromBookmarks, QUOTATION_HARDWARE, sourceBookmarks, QUOTATION_HARDWARE);

		saveDoc(test_resources_path + "updated_" + INPUTFILENAME, sourceDoc);
	}

	private static void copyFullBookmarkContent(Bookmarks fromBookmarks, String fromBookmarkName, Bookmarks toBookmarks, String toBookmarkName) throws XmlException, FileNotFoundException, IOException {

		Bookmark fromBookmark = fromBookmarks.getBookmark(fromBookmarkName);
		Bookmark toBookmark = toBookmarks.getBookmark(toBookmarkName);
		toBookmark.copyAllParagraphsFrom(fromBookmark);
	}

	private static void copyExcelWorkbook(Bookmarks fromBookmarks, String fromBookmarkName, Bookmarks toBookmarks, String toBookmarkName) throws XmlException, FileNotFoundException, IOException {
		
		Bookmark fromBookmark = fromBookmarks.getBookmark(fromBookmarkName);
		Bookmark toBookmark = toBookmarks.getBookmark(toBookmarkName);

//		targetBookmark.copyOleImageFrom(sourceBookmark);
		toBookmark.copyOleObjectFrom(fromBookmark);
	}
	
//	private static void copyText(XWPFDocument sourceDoc, String sourceBookmarkName, XWPFDocument targetDoc, String targetBookmarkName) throws XmlException, FileNotFoundException, IOException {
//		
//		Bookmarks sourceBookmarks = new Bookmarks(sourceDoc);
//		Bookmark sourceBookmark = sourceBookmarks.getBookmark(sourceBookmarkName);
//		String aircraftMsnValue = sourceBookmark.getBookmarkText();
//		
//		Bookmarks targetBookmarks = new Bookmarks(targetDoc);
//		Bookmark targetBookmark = targetBookmarks.getBookmark(targetBookmarkName);
//		targetBookmark.insertTextAtBookmark(aircraftMsnValue, Bookmark.REPLACE);
//	}

	private static void saveDoc(String filename, XWPFDocument document) throws FileNotFoundException, IOException {
		try (FileOutputStream out = new FileOutputStream(new File(filename))) {
			document.write(out);
			System.out.println(filename + " written successully");
		}
	}

	private static XWPFDocument loadDoc(String sourceFile) {
		try (FileInputStream fis = new FileInputStream(sourceFile)) {
			return new XWPFDocument(fis);
		} catch (Exception e) {
			throw new RuntimeException("File cannot be parsed: " + sourceFile, e);
		}
	}

}
