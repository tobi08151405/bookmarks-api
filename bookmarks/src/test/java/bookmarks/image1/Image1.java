package bookmarks.image1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.Document;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.xmlbeans.XmlException;

import bookmarks.Bookmark;
import bookmarks.Bookmarks;

/**
 * Am Test-Server f�hrte das Setzen der Unterschrift zum Verlust der
 * entsprechenden Textmarke und vermutlich dadurch in weiterer Folge zu einem
 * ung�ltigen Dokument.
 * 
 * <p>
 * <pre>
 * {@code
 * Zustand nach Erzeugen der Order Confirmation (OC-...docx, hier ersetzt durch STC-T-052..., aus dem die OC hervorgeht). Auff�llig ist:
 * 1. dass gleich zwei Aufrufe direkt hintereinander passieren 
 * 2. dass offenbar das Bookmark SignatureCreatedBy_Signature "verloren" bzw. gel�scht wurde, was nicht passieren d�rfte
 * 
 *   (1. Aufruf)
 *   
 * 2021-01-04 12:52:35.491  INFO 283701 --- [http-nio-19095-exec-8] bookmarks.api.BookmarkApplier            : Apply BookmarkData [sourceFile=/var/www/samba/doa/Customer/C002_Condor/C002-0001_B767 and B757 Autoland Indicator/Commercial/OC-B7567C002-0002_A.docx, targetFile=/var/www/samba/doa/Customer/C002_Condor/C002-0001_B767 and B757 Autoland Indicator/Commercial/OC-B7567C002-0002_A.docx, 
 * texts={DocDate=04.01.2021, SignatureCreatedBy_Name=Administrator, SignatureCreatedBy_Function=Administrator}, 
 * images={SignatureCreatedBy_Signature=ImageData [filename=/var/www/html/skytec-backend/src/uploads/users/admin/Isaac_Newton_signature.svg.png, source=null, width=200, height=80]}, 
 * tables={}, copy=[]]
 * 
 * 2021-01-04 12:52:35.522  INFO 283701 --- [http-nio-19095-exec-8] bookmarks.api.ImageBookmarks             : Filling bookmark SignatureCreatedBy_Signature with ImageData [filename=/var/www/html/skytec-backend/src/uploads/users/admin/Isaac_Newton_signature.svg.png, source=null, width=200, height=80] in document
 * 
 *   (2. Aufruf)
 * 
 * 2021-01-04 12:52:48.015  INFO 283701 --- [http-nio-19095-exec-1] bookmarks.api.BookmarkApplier            : Apply BookmarkData [sourceFile=/var/www/samba/doa/Customer/C002_Condor/C002-0001_B767 and B757 Autoland Indicator/Commercial/OC-B7567C002-0002_A.docx, targetFile=/var/www/samba/doa/Customer/C002_Condor/C002-0001_B767 and B757 Autoland Indicator/Commercial/OC-B7567C002-0002_A.docx, 
 * texts={DocDate=04.01.2021, SignatureCreatedBy_Name=Administrator, SignatureCreatedBy_Function=Administrator}, 
 * images={SignatureCreatedBy_Signature=ImageData [filename=/var/www/html/skytec-backend/src/uploads/users/admin/Isaac_Newton_signature.svg.png, source=null, width=200, height=80]},
 * tables={}, copy=[]]
 * 
 * 2021-01-04 12:52:48.051  INFO 283701 --- [http-nio-19095-exec-1] bookmarks.api.ImageBookmarks             : Filling bookmark SignatureCreatedBy_Signature with ImageData [filename=/var/www/html/skytec-backend/src/uploads/users/admin/Isaac_Newton_signature.svg.png, source=null, width=200, height=80] in document
 * 2021-01-04 12:52:48.051  WARN 283701 --- [http-nio-19095-exec-1] bookmarks.api.ImageBookmarks             : Bookmark 'SignatureCreatedBy_Signature' is not defined in document. Skipping.
 * }
 * </pre>
 * 
 */
public class Image1 {

	private static final String SIGNATURE_CREATED_BY_SIGNATURE = "SignatureCreatedBy_Signature";

//	private static final String INPUTFILENAME = "OC-B7567C002-0003_A.docx";
//	private static final String INPUTFILENAME = "STC-T-052_ORDER CONFIRMATION_A-logo-png.docx";
	private static final String INPUTFILENAME = "STC-T-052_ORDER CONFIRMATION_A-ein-gesamtlogo.docx";
//	private static final String INPUTFILENAME = "STC-T-052_ORDER CONFIRMATION_A-logo-png-ohne-svg.docx";
	private static final String test_resources_path = "src/test/resources/bookmarks/image1/";
	private static final String source = test_resources_path + INPUTFILENAME;
//	private static final String target = test_resources_path + "created.docx";
//	private static final String from = test_resources_path + "P-767C002-0001_A.docx";

	public static void main(String[] args) throws IOException, XmlException, InvalidFormatException {

		File file = new File(test_resources_path);
		String absolutePath = file.getAbsolutePath();

		System.out.println(absolutePath);

//		XWPFDocument doc = new XWPFDocument();
		XWPFDocument doc = loadDoc(source);

		Bookmarks sourceBookmarks = new Bookmarks(doc, source);
//        XWPFParagraph p = doc.createParagraph();

		Bookmark bookmarkCreatedBySignature = sourceBookmarks.getBookmark(SIGNATURE_CREATED_BY_SIGNATURE);

//        insertImage(bookmarkCreatedBySignature.getParagraph(), test_resources_path + "signature2.bmp");
//        insertImage(bookmarkCreatedBySignature.getParagraph(), test_resources_path + "Signature-cindy.png");
//        insertImage(bookmarkCreatedBySignature.getParagraph(), test_resources_path + "signature-stamp-small.png");
//        insertImage(bookmarkCreatedBySignature.getParagraph(), test_resources_path + "signature-toby.png");
//        insertImage(bookmarkCreatedBySignature.getParagraph(), test_resources_path + "skytec.jpg");

		bookmarkCreatedBySignature.insertImageAtBookmark(test_resources_path + "signature-stamp-small.png");

		saveDoc(test_resources_path + "updated_" + INPUTFILENAME, doc);
	}

	private static void insertImage(XWPFParagraph p, String imgFile) throws FileNotFoundException, IOException, InvalidFormatException {

            XWPFRun r = p.createRun();

                int format;

                if (imgFile.endsWith(".emf")) {
                    format = Document.PICTURE_TYPE_EMF;
                } else if (imgFile.endsWith(".wmf")) {
                    format = Document.PICTURE_TYPE_WMF;
                } else if (imgFile.endsWith(".pict")) {
                    format = Document.PICTURE_TYPE_PICT;
                } else if (imgFile.endsWith(".jpeg") || imgFile.endsWith(".jpg")) {
                    format = Document.PICTURE_TYPE_JPEG;
                } else if (imgFile.endsWith(".png")) {
                    format = Document.PICTURE_TYPE_PNG;
                } else if (imgFile.endsWith(".dib")) {
                    format = Document.PICTURE_TYPE_DIB;
                } else if (imgFile.endsWith(".gif")) {
                    format = Document.PICTURE_TYPE_GIF;
                } else if (imgFile.endsWith(".tiff")) {
                    format = Document.PICTURE_TYPE_TIFF;
                } else if (imgFile.endsWith(".eps")) {
                    format = Document.PICTURE_TYPE_EPS;
                } else if (imgFile.endsWith(".bmp")) {
                    format = Document.PICTURE_TYPE_BMP;
                } else if (imgFile.endsWith(".wpg")) {
                    format = Document.PICTURE_TYPE_WPG;
                } else {
                    System.err.println("Unsupported picture: " + imgFile +
                            ". Expected emf|wmf|pict|jpeg|png|dib|gif|tiff|eps|bmp|wpg");
                    return;
                }

                r.setText(imgFile);
                r.addBreak();
                try (FileInputStream is = new FileInputStream(imgFile)) {
                    r.addPicture(is, format, imgFile, Units.toEMU(200), Units.toEMU(200)); // 200x200 pixels
                }
//                r.addBreak(BreakType.PAGE);
	}
	

	private static void saveDoc(String filename, XWPFDocument document) throws FileNotFoundException, IOException {
		try (FileOutputStream out = new FileOutputStream(new File(filename))) {
			document.write(out);
			System.out.println(filename + " written successully");
		}
	}

	private static XWPFDocument loadDoc(String sourceFile) {
		try (FileInputStream fis = new FileInputStream(sourceFile)) {
			return new XWPFDocument(fis);
		} catch (Exception e) {
			throw new RuntimeException("File cannot be parsed: " + sourceFile, e);
		}
	}

}
