package bookmarks.text0;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.xmlbeans.XmlException;

import bookmarks.Bookmark;
import bookmarks.Bookmarks;

/**
 * Problem, das hier nachgestellt wird: es werden Bookmarks gel�scht, z.B. DocDate, DocRevision.
 * 
 * <p>
 * Original-Request: (Achtung: viele der Bookmarks im Request existieren gar nicht im Dokument, z.B. C_Country)
 * <pre>
 * Apply BookmarkData [
 *  sourceFile=/var/www/samba/qms/Templates/STC-T-066_WORK ORDER_A.docx, 
 *  targetFile=/var/www/samba/doa/Customer/C005_Condor/C005-0003_B767 and B757 Autoland Indicator/Project Data/Production/WO-S-B756734A-00011_A-0003_A.docx,
 *  texts={DocDate=DocDate, DocNumber=WO-S-B756734A-00011_A-0003, DocRevision=A, C_City=Frankfurt am Main / Flughafen, 
 *   C_Country=Germany, C_Postcode=60549, C_Street=Cargo City S�d, Geb�ude 507, S_City=Frankfurt am Main / Flughafen, 
 *   S_Country=Germany, S_Postcode=60549, S_Street=Cargo City S�d, Geb�ude 507, C_FamilyName=Conradi, C_FirstName=Stanislaw, 
 *   C_Tel=+49 (0) 69 697132 448 , C_tel=+49 (0) 69 697132 448 , C_email=Stanislaw.Conradi@Condor.com, S_FamilyName=Conradi, 
 *   S_FirstName=Stanislaw, S_Tel=+49 (0) 69 697132 448 , S_tel=+49 (0) 69 697132 448 , S_email=Stanislaw.Conradi@Condor.com, 
 *   C_Company=Condor Technik GmbH, C_VatNo=C_VatNo, C_QMS_EN9100=No, C_QMS_ISO9001=No, C_QMS_P21G=No, C_QMS_P21J=No, 
 *   S_Company=Condor Technik GmbH, S_VatNo=S_VatNo, S_QMS_EN9100=No, S_QMS_ISO9001=No, S_QMS_P21G=No, 
 *   S_QMS_P21J=No, Project=B767 and B757 Autoland Indicator, ProjectNumber=C005-0003, EngineeringCertification=Minor Change, PM_Name=PM_Name, 
 *   AircraftType=B7567, PD_IAC_TypeCertificateHolder=The Boeing Company P.O. Box 3707 Seattle, WA 98124-2207 USA,
 *   PD_IAC_Model=Boeing 757-300 and 767-300l, AC_ApplicapleTypeCertificate=EASA.IM.A.205 & EASA.IM.A.035, AD_TCDS_Revision=Issue 03 and Issue 08 , 
 *   Applicable_Agency=EASA, AD_ForeignApprovalReference=EASA.IM.A.205 & EASA.IM.A.035], Applicable_CS=JAR 25, CS_Amendment=Change 14
 *  },
 *  images={}, tables={}, copy=[]]
 * </pre>
 * 
 */
public class Text0 {

	private static final String DOC_DATE = "DocDate";
	private static final String DOC_NUMBER = "DocNumber";
	private static final String DOC_REVISION = "DocRevision";

	private static final String PROJECT_NUMBER = "ProjectNumber";

	private static final String INPUTFILENAME = "STC-T-066_WORK ORDER_A.docx";
//	private static final String INPUTFILENAME = "STC-ohne-Logo.docx";
	private static final String test_resources_path = "src/test/resources/bookmarks/text0/";
	private static final String source = test_resources_path + INPUTFILENAME;

	public static void main(String[] args) throws IOException, XmlException, InvalidFormatException {

		File file = new File(test_resources_path);
		String absolutePath = file.getAbsolutePath();

		System.out.println(absolutePath);

		// mehrmals hintereinander die Datei bearbeiten, jeweils mit den selben Daten bef�llen
		int counter = 0;
 		String lastSaveFile = doit(source, counter++);
		lastSaveFile = doit(lastSaveFile, counter++);
		lastSaveFile = doit(lastSaveFile, counter++);
	}

	private static String doit(String sourceFilename, int counter)
			throws XmlException, FileNotFoundException, IOException {
		XWPFDocument sourceDoc = loadDoc(sourceFilename);

		Bookmarks sourceBookmarks = new Bookmarks(sourceDoc, source);

		sourceBookmarks.getBookmark(DOC_DATE).insertTextAtBookmark("DocDate", Bookmark.REPLACE);
		sourceBookmarks.getBookmark(DOC_NUMBER).insertTextAtBookmark("WO-S-B756734A-00011_A-0003", Bookmark.REPLACE);
		sourceBookmarks.getBookmark(DOC_REVISION).insertTextAtBookmark("A", Bookmark.REPLACE);
		sourceBookmarks.getBookmark(PROJECT_NUMBER).insertTextAtBookmark("C005-0003", Bookmark.REPLACE);

		String saveFilename = test_resources_path + "updated_" + counter + "_" + INPUTFILENAME;
		saveDoc(saveFilename, sourceDoc);
		return saveFilename;
	}

	private static void saveDoc(String filename, XWPFDocument document) throws FileNotFoundException, IOException {
		try (FileOutputStream out = new FileOutputStream(new File(filename))) {
			document.write(out);
			System.out.println(filename + " written successully");
		}
	}

	private static XWPFDocument loadDoc(String sourceFile) {
		try (FileInputStream fis = new FileInputStream(sourceFile)) {
			return new XWPFDocument(fis);
		} catch (Exception e) {
			throw new RuntimeException("File cannot be parsed: " + sourceFile, e);
		}
	}

}
