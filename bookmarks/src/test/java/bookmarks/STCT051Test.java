package bookmarks;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.xmlbeans.XmlException;
import org.junit.jupiter.api.Disabled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import bookmarks.api.TableData;

/**
 * Diese Test-Klasse ist als normale Java Application auszufuehren, nicht als Unit Test
 * 
 * @author Markus Pscheidt
 *
 */
@Disabled
public class STCT051Test {
	
	public static final String INPUTFILENAME = "STC-T-051_PROPOSAL_A.docx";

	private static Logger log = LoggerFactory.getLogger(Bookmark.class);

	public static void main(String[] args) throws IOException, XmlException, BookmarkException {
		
		String template = "Y:\\templates\\" + INPUTFILENAME;

		FileInputStream fis = new FileInputStream(new File(template));
		XWPFDocument document = new XWPFDocument(fis);

		Bookmarks bookmarks = new Bookmarks(document, template);

		bookmarks.getBookmark("DeliveryTerms").insertTextAtBookmark("Ein Deliv. Term\nmitZeilenumbruch", Bookmark.REPLACE);
		bookmarks.getBookmark("EngineeringCertification").insertTextAtBookmark("Eng Zertifizierung", Bookmark.REPLACE);
		bookmarks.getBookmark("MaterialsCertification").insertTextAtBookmark("Mat Zertifizierung", Bookmark.REPLACE);

		bookmarks.getBookmark("EngineeringItemTable").insertTableAtBookmark(new TableData(new String[][] { 
            {"1", "desc1", "A320\nZeile 2\r\nZeile 3", "4 weeks", "� 1.00"},
            {"2", "desc2", "A322", "2 weeks", "� 2.00"},
           }));

		bookmarks.getBookmark("ItemNotesEngineering").insertTableAtBookmark(new TableData(new String[][] { 
            {"Sp 11", "Spalte 12"},
            {"Sp 21", "Sp 22"},
           }));

		bookmarks.getBookmark("ItemTable").insertTableAtBookmark(new TableData(new String[][] { 
            {"1", "1ea", "desc1", "Pn1", "4 weeks", "� 1.00", "� 1.10"},
            {"2", "2ea", "desc2", "Pn2", "2 weeks", "� 2.00", "� 2.20"},
           }));

		bookmarks.getBookmark("ItemNotesMaterial").insertTableAtBookmark(new TableData(new String[][] { 
            {"M 11", "Item Notes material 12"},
            {"M 21", "M 22"},
           }));
		
		bookmarks.getBookmark("PaymentMilestonesEngineering").insertTableAtBookmark(new TableData(new String[][] { 
            {"M1", "25 %", "Terms A", "Remarks 1"},
            {"M2", "55 %", "Terms B", "Remarks 2"},
           }));

		bookmarks.getBookmark("PaymentMilestonesMaterial").insertTableAtBookmark(new TableData(new String[][] { 
            {"M7", "28 %", "Terms A", "Rem 1"},
            {"M8", "14 %", "Terms B", "Rem 2"},
           }));
		
		bookmarks.getBookmark("QuotationDeliverables").insertBulletsAtBookmark(Arrays.asList("TA", "TB1\nTB2", "TC", "TD"));

		// Test Excel spreadsheet
		if (!bookmarks.getBookmark("QuotationTimeline").existsOleObject()) {
			throw new RuntimeException("Kein Excel Objekt gefunden");
		}
		
		// in local dir
		File outFile = new File("updated_" + INPUTFILENAME);
		FileOutputStream out = new FileOutputStream(outFile);
		document.write(out);
		out.close();
		log.info("{} written successully", outFile.getAbsolutePath());
	}

	private static void setTextAt(XWPFTable table, int rowNum, int colNum, String text) {
		XWPFTableRow row = table.getRow(rowNum);
		if (row == null) {
			row = table.createRow();
		}
		row.getTableCells().get(colNum).setText(text);
	}
	
}
