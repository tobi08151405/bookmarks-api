package bookmarks.delete;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.xmlbeans.XmlException;

import bookmarks.Bookmark;
import bookmarks.Bookmarks;

/**
 * Bookmark leeren mit {@code null}-Wert (oder Leerstring).
 */
public class Delete {

	private static final String DOCNUMBER = "DocNumber";

	private static final String INPUTFILENAME = "STC-T-001_PROJECT DESCRIPTION_A.docx";
	private static final String test_resources_path = "src/test/resources/bookmarks/delete/";
	private static final String source = test_resources_path + INPUTFILENAME;

	public static void main(String[] args) throws IOException, XmlException, InvalidFormatException {

		File file = new File(test_resources_path);
		String absolutePath = file.getAbsolutePath();

		System.out.println(absolutePath);

		XWPFDocument sourceDoc = loadDoc(source);

		Bookmarks sourceBookmarks = new Bookmarks(sourceDoc, source);
		sourceBookmarks.getBookmark(DOCNUMBER).insertTextAtBookmark(null, Bookmark.REPLACE);

		saveDoc(test_resources_path + "updated_" + INPUTFILENAME, sourceDoc);
	}

	private static void saveDoc(String filename, XWPFDocument document) throws FileNotFoundException, IOException {
		try (FileOutputStream out = new FileOutputStream(new File(filename))) {
			document.write(out);
			System.out.println(filename + " written successully");
		}
	}

	private static XWPFDocument loadDoc(String sourceFile) {
		try (FileInputStream fis = new FileInputStream(sourceFile)) {
			return new XWPFDocument(fis);
		} catch (Exception e) {
			throw new RuntimeException("File cannot be parsed: " + sourceFile, e);
		}
	}

}
