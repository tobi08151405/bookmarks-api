# bookmarks-api
Bookmarks API for the Skytec project

## Prerequisites

- Java 8 (or newer). Get it from https://adoptopenjdk.net/?variant=openjdk8&jvmVariant=hotspot

## Download the bookmarks jar file

Get it from https://drive.google.com/drive/folders/1n6VSVaR_ZwIGtFnbiDKfOC-QCkVT2EZb

## Run the Bookmarks API

To run the Bookmarks API, simply run the jar file:

    java -jar bookmarks-x.y.z.jar

This starts the tool listening on TCP port 19095.

## Use the Bookmarks API

The tool is accessible via HTTP POST at the URL "127.0.0.1:19095/apply".

Here is an example:

    POST 127.0.0.1:19095/apply
    Content-Type: application/json
    
    Request-Body:
	{
		"sourceFile": "C:\\temp\\doc\\poi\\copyBookmarks\\STC-T-052_ORDER CONFIRMATION_A.docx",
		"targetFile": "C:\\temp\\doc\\poi\\copyBookmarks\\STC-T-052_ORDER CONFIRMATION_A_Updated.docx",
		"texts": { "AircraftMSN": "Reference X", "DocNumber": "589", "TemplateRev": "Y"},
		"images": { "SignatureCreatedBy_Signature": { "filename": "C:\\temp\\doc\\signature2.png" } },
		"tables": { "EngineeringItemTable": { "data": [["1", "Desc1", "Aircraft1", "1 week", "€ 1.00"],
													   ["2", "Desc2", "Aircraft2", "2 weeks", "€ 2.00"]] } },
		"copy": [{"fromFile": "C:\\temp\\doc\\poi\\copyBookmarks\\STC-T-051_PROPOSAL_A.docx",
							"contents": [{"fromBookmark": "QuotationDataInput", "toBookmark": "QuotationDataInput"},
										 {"fromBookmark": "QuotationHardware", "toBookmark": "QuotationHardware"}],
							"oleObjects": [{"fromBookmark": "QuotationTimeline", "toBookmark": "QuotationTimeline"}]
		}],
		"delete": ["SignatureApproval3", "SignatureApproval4"]
	}

### Texts

Text bookmarks must not include "end-of-line". So be sure to select only text, but no line break, when defining a new bookmark.

### Images

Note that for "images" the "width" and "height" values are optional. It is recommended to omit these values in order to avoid distortions. If these values are omitted, the width and height are determined automatically.

### Tables

- Define table bookmarks by spanning the first entire cell (not only the text in the cell), where bookmark content is to be inserted, for example the first cell after the header row.
- There could be problems with replacement of text-in-cells if there are multiple runs within a single cell. What may happen is that not all of the text is removed before text is placed into table cells. To avoid such problems, keep cells empty or paste text so that only a single run is created.

### Copy bookmarks

The "copy" parameter object contains a list of parameter objects
- "fromFile": The path to another .docx file that contains the data that needs to be copied into the "sourceFile"'s bookmarks.
- For each "fromFile" there are two options (both are lists):
    - "contents" refers to copying paragraph contents, including bullet lists, texts and images. It is not able to copy Excel workbooks or other OLE objects.
    - "oleObjects" refers to Excel workbooks, for example. Note that there must exist an Excel workbook in the target document bookmark, because it is not possible for the bookmark tool to create an Excel workbook, only to copy its contents.

### Delete bookmarks

Given is a list of bookmark names.

This clears the bookmarks' content and removes the bookmarks.

This is applicable for bookmarks in paragraphs. Table cell bookmarks cannot be removed.

## Usage in skytec-backend

There is a convenience function in src/middlewares/bookmarks.api.js, which calls the Bookmarks API:

    bookmarksApi.applyBookmarks = function (sourceFile, targetFile, texts, images, tables) {
        ...
    }

## Limitations

Word files must not use section in any form because Apache POI does not support sections. This means that no section breaks must be used.

## Version info

### 1.0.14

- New feature: Delete bookmarks in paragraphs.

### 1.0.13

- Fix for setting image bookmarks in table cells: Bookmark was incorrectly removed. Cell bookmarks are now treated the same as bookmarks in paragraphs.

### 1.0.12

- Fix for setting the text value of bookmarks in cell, which was caused by identifying all bookmarks inside table cells as table cell bookmarks.
  When setting text, table cell bookmarks are now treated the same way as non-table cell bookmarks.

### 1.0.11

- New feature: Copy paragraphs within table cells
- Pictures as bullet points: technically not possible with Apache Poi. Get rid of picture bullet IDs in order to avoid invalid .docx files.
- Bullet lists: Fix paragraph style issues
- Copy paragraphs: fix positioning of the bookmarkEnd tag.
- Images: Cropping (show a portion of an image)

### 1.0.10

- Table bookmarks: fix so that all runs of table paragraphs are removed.

### 1.0.9

- Copy bookmarks: Do not skip all remaining bookmark copy operations when a particular bookmark copy operation failed.
- Copy bookmarks: Provide a warning message instead of throwing a NullPointException in an attempt to copy a table bookmark.

### 1.0.8

- Improved error message to correctly state the bookmark name when bookmark-to-copy do not exist.
- Copy bookmarks: support bookmarks that do not contain any paragraphs

### 1.0.7

- Copy OLE objects: Search not only the bookmark's paragraph, but also all following paragraphs until the bookmark end element is found.

### 1.0.6

- Support bookmarks that are defined outside of a paragraph, as described in https://stackoverflow.com/q/64143049/606662. This is often the case when defining a bookmark for Excel workbooks.
- Include "copy" section in log.
- For copyAllParagraphs: Move bookmarkEnd to after the paragraph if on same paragraph as bookmarkStart.

### 1.0.5

- Bullets fix: Prevent empty style node <w:pStyle w:val=""/>, which affects the display of bullet points in Libre Office.

### 1.0.4

- Fix for the numbering of bullet lists and other numbered elements: Deep comparison of abstract numberings and proper creation of concrete numbering IDs.

### 1.0.3

- Table bookmarks and bullet lists: Support for line breaks in texts (\r\n or \n) and convert them into Word breaks (corresponds to Shift+Enter).

### 1.0.2

- Table bookmarks: Layout fix for the paragraph properties (alignment, etc.) of the inserted cells; each cell paragraph is applied the same style as the top cell in the same column.

### 1.0.0

- Base version with support for inserting data into text bookmarks, image bookarmks and table bookmarks, plus support for copying bookmark content.
- Text bookmarks: Must not span multiple paragraphs
- Table bookmarks: A table bookmark must be defined in a cell of the first row, spanning the entire cell
- Copy bookmarks: The tool is able to copy multiple paragraphs containing texts, images and bullet lists. Not supported are Excel workbooks and tables with the exception Excel workbook data can be copied if an Excel workbook exists in the target document.

## Credits

This tool is based on the work done by Mark Beardley: http://apache-poi.1045710.n5.nabble.com/How-to-read-the-value-of-bookmarks-docx-td5710184i20.html
